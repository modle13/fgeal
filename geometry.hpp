/*
 * geometry.hpp
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2017  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef FGEAL_GEOMETRY_HPP_
#define FGEAL_GEOMETRY_HPP_
#include <ciso646>

#ifndef USE_MATH_DEFINES
	#define USE_MATH_DEFINES
#endif  /* USE_MATH_DEFINES */

#ifndef _USE_MATH_DEFINES
	#define _USE_MATH_DEFINES
#endif  /* _USE_MATH_DEFINES */

#include <cmath>
#include <string>

#ifndef M_PI
	/// pi
	#define M_PI 3.14159265358979323846
#endif  /* M_PI */

#ifndef M_PI_2
	/// pi/2
	#define M_PI_2 1.57079632679489661923
#endif  /* M_PI_2 */

#ifndef M_3_PI_2
	/// 3*pi/2
	#define M_3_PI_2 4.71238898038468985769
#endif  /* M_3_PI_2 */

namespace fgeal
{
	/** A structure that contains parameters for a point. This POD is used mostly as a way to specify coordinates. */
	struct Point { float x, y; };

	/** A structure that contains parameters for a rectangle. This POD is used mostly as a way to specify a region. */
	struct Rectangle
	{
		float x, y, w, h;

		/** Returns true if this rectangle intersect with given one */
		inline bool intersects(const Rectangle& r)
		{
			return !(x + w <= r.x or x >= r.x + r.w or y + h <= r.y or y >= r.y + r.h);
		}

		inline bool contains(const Point& p)
		{
			return p.x > x and p.x < x + w and p.y > y and p.y < y + h;
		}
	};

	/** A structure that contains parameters for a 2D vector.
	 *  This is a POD to be mostly used as a way to specify coordinates.
	 *  There is also a set of vector operations methods available. */
	struct Vector2D
	{
		float x, y;

		/** Creates a vector with the given magnitude and random direction. */
		static Vector2D random(float magnitude);

		// ------ equality

		bool operator ==(const Vector2D& v) const;
		bool operator !=(const Vector2D& v) const;

		/** Returns true if this vector coordinates equals the ones from the given vector */
		bool equals(const Vector2D& v) const;

		/** Creates and returns a copy of this vector via copy constructor */
		Vector2D clone() const;

		// ------- utils

		/** Creates a string with this vector coordinates (x, y) */
		std::string toString() const;

		/** Creates and returns an array with this Vectors coordinates, in correct order.  */
		float* getCoordinates() const;

		// ------- magnitude/length

		float operator ~() const;

		/** Returns the length/magnitude of this vector. */
		float magnitude() const;

		/** \see magnitude() */
		inline float length() const { return magnitude(); }

		/** Return true if x = 0 and y = 0; */
		inline bool isZero() const
		{
			return x==0 and y==0;
		}

		// ------- normalization

		Vector2D operator !() const;

		/** Creates a vector with length 1 and same direction as this vector. In other words, a new vector that is a normalized version of this vector. Note that <b>the original vector remains unchanged</b>. */
		Vector2D unit() const;

		/** Divides this vector's coordinates by its length/magnitude, normalizing it.
		The returned object is the vector instance itself after normalization. */
		Vector2D& normalize();

		// ------- reflection

		Vector2D operator -() const;

		/** Creates and returns the opposite of this vector. In other words, returns a vector with same coordinates as this, but with changed signal. Note that <b>the original vector remains unchanged</b>. */
		Vector2D opposite() const;

		Vector2D& operator --();

		/** Changes the signal of this vector coordinates, effectively reflecting it.
		<br> The returned object is <b>the vector instance itself</b> after reflection. */
		Vector2D& reflect();

		/** Changes the signal of this vector x coordinate.
		<br> The returned object is <b>the vector instance itself</b> after reflection. */
		Vector2D& reflectX();

		/** Changes the signal of this vector y coordinate.
		<br> The returned object is <b>the vector instance itself</b> after reflection. */
		Vector2D& reflectY();

		// ------- basic arithmetic

		Vector2D operator +(const Vector2D& v) const;

		/** Creates and returns a vector that represents the sum of this vector and the given vector. Note that <b>the original vector remains unchanged</b>.*/
		Vector2D sum(const Vector2D& v) const;

		Vector2D& operator +=(const Vector2D& v);

		/** Adds to this vector the given vector. In other words, it performs an addition to this vector coordinates.
		<br> The returned object is <b>the vector instance itself</b> after summation. */
		Vector2D& add(const Vector2D& v);

		Vector2D operator -(const Vector2D& v) const;

		/** Creates a vector that represents the difference/displacement of this vector and the given vector, in this order. It's useful to remember that vector subtraction is <b>not commutative</b>: a-b != b-a.
		<br> Note that <b>the original vector remains unchanged</b>. */
		Vector2D difference(const Vector2D& v) const;

		Vector2D& operator -=(const Vector2D& v);

		/** Subtracts from this vector the given vector. In other words, it performs an subtraction to this vector coordinates.
		It's useful to remember that vector subtraction is <b>not commutative</b>: a-b != b-a.
		<br> The returned object is the <b>the vector instance itself</b> after subtraction. */
		Vector2D& subtract(const Vector2D& v);

		Vector2D operator *(const float& factor) const;

		/** Creates a vector that represents the scalar multiplication of this vector by the given factor. Note that <b>the original vector remains unchanged</b>.*/
		Vector2D times(const float factor) const;

		Vector2D& operator *=(const float& factor);

		/** Multiply this vectors coordinates by the given factor. The returned object is <b>the vector instance itself</b> after multiplication.*/
		Vector2D& scale(float factor);

		// ------- miscellaneous operations

		float operator %(const Vector2D& v) const;

		/** Compute the distance between this vector and the given vector. In other words, returns the length/magnitude of the displacement between this and the given vector. */
		float distance(const Vector2D& v) const;

		float operator ^(const Vector2D& v) const;

		/** Compute the inner/dot product between this and the given vector. */
		float innerProduct(const Vector2D& v) const;

		// ------- projection operations (XXX not tested)

		Vector2D operator ||(const Vector2D& v) const;

		/* Creates a vector that represents the projection of this vector on the given vector v. */
		Vector2D projection(const Vector2D& v) const;

		/* Creates a vector that represents the rejection of this vector on the given vector v. The rejection is defined as rej(u, v) = u - proj(u, v) */
		Vector2D rejection(const Vector2D& v) const;

		Vector2D operator |(const Vector2D& v) const;

		/* Creates a vector that represents the reflection of this vector in the axis represented by the given vector v. */
		Vector2D reflection(const Vector2D& v) const;

		// ------- rotation operations (XXX not tested)

		Vector2D operator <(const float& radians) const;

		Vector2D rotation(const float& radians) const;

		Vector2D& operator <<(const float& radians);

		Vector2D& rotate(const float& radians);

		Vector2D perpendicular() const;

		//-----------------------------------------------------------------

		/** Represents the null/zero vector. It has coordinates (0, 0). */
		const static Vector2D NULL_VECTOR;

		/** A vector codirectional with the X axis, with length 1. */
		const static Vector2D X_VERSOR;

		/** A vector codirectional with the Y axis, with length 1. */
		const static Vector2D Y_VERSOR;
	};
}

#endif /* FGEAL_GEOMETRY_HPP_ */
