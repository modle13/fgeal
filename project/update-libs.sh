#!/bin/bash

# 
#  This file is part of fgeal.
# 
#  fgeal - Faruolo's game engine/library abstraction layer
#  Copyright (C) 2015  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
# 
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License, or (at your option) any later version.
# 
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
# 
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#

echo "Updating libs with current build configuration ""$1""..."
if [ ! -d "libs" ]; then
	echo "Creating directory /libs..."
	mkdir libs
else
	echo "Deleting contents of /libs..."
	rm -r libs/*
fi

echo "Browsing build folder""$1""..."
cd "$1"
echo "copying .so .a files..."
find . -name '*.so' | cpio -pdm ../libs/
find . -name '*.a' | cpio -pdm ../libs/
echo "done."


# Configuring eclipse external tool configurations:
#
# Location: ${workspace_loc:/fgeal/update-libs.sh}
# Working Directoty: ${workspace_loc:/fgeal}
# Arguments: "${workspace_loc:/fgeal}/${config_name:fgeal}"
# 
# On the "Refresh" tab, uncheck "Refresh resources upon completion"
