################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../adapters/allegro4.4/core.cpp \
../adapters/allegro4.4/display.cpp \
../adapters/allegro4.4/event.cpp \
../adapters/allegro4.4/font.cpp \
../adapters/allegro4.4/graphics.cpp \
../adapters/allegro4.4/image.cpp \
../adapters/allegro4.4/input.cpp \
../adapters/allegro4.4/sound.cpp 

OBJS += \
./adapters/allegro4.4/core.o \
./adapters/allegro4.4/display.o \
./adapters/allegro4.4/event.o \
./adapters/allegro4.4/font.o \
./adapters/allegro4.4/graphics.o \
./adapters/allegro4.4/image.o \
./adapters/allegro4.4/input.o \
./adapters/allegro4.4/sound.o 

CPP_DEPS += \
./adapters/allegro4.4/core.d \
./adapters/allegro4.4/display.d \
./adapters/allegro4.4/event.d \
./adapters/allegro4.4/font.d \
./adapters/allegro4.4/graphics.d \
./adapters/allegro4.4/image.d \
./adapters/allegro4.4/input.d \
./adapters/allegro4.4/sound.d 


# Each subdirectory must supply rules for building sources it contributes
adapters/allegro4.4/%.o: ../adapters/allegro4.4/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I"BUILD_PATH" -I"BUILD_PATH/.." -O3 -Wall -c -fmessage-length=0 -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


