################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../adapters/sdl2.0/core.cpp \
../adapters/sdl2.0/display.cpp \
../adapters/sdl2.0/event.cpp \
../adapters/sdl2.0/font.cpp \
../adapters/sdl2.0/graphics.cpp \
../adapters/sdl2.0/image.cpp \
../adapters/sdl2.0/input.cpp \
../adapters/sdl2.0/sound.cpp 

OBJS += \
./adapters/sdl2.0/core.o \
./adapters/sdl2.0/display.o \
./adapters/sdl2.0/event.o \
./adapters/sdl2.0/font.o \
./adapters/sdl2.0/graphics.o \
./adapters/sdl2.0/image.o \
./adapters/sdl2.0/input.o \
./adapters/sdl2.0/sound.o 

CPP_DEPS += \
./adapters/sdl2.0/core.d \
./adapters/sdl2.0/display.d \
./adapters/sdl2.0/event.d \
./adapters/sdl2.0/font.d \
./adapters/sdl2.0/graphics.d \
./adapters/sdl2.0/image.d \
./adapters/sdl2.0/input.d \
./adapters/sdl2.0/sound.d 


# Each subdirectory must supply rules for building sources it contributes
adapters/sdl2.0/%.o: ../adapters/sdl2.0/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I"BUILD_PATH" -I"BUILD_PATH/.." -O3 -Wall -c -fmessage-length=0 -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


