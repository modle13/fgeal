################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../adapters/sfml1.6/core.cpp \
../adapters/sfml1.6/display.cpp \
../adapters/sfml1.6/event.cpp \
../adapters/sfml1.6/font.cpp \
../adapters/sfml1.6/graphics.cpp \
../adapters/sfml1.6/image.cpp \
../adapters/sfml1.6/input.cpp \
../adapters/sfml1.6/sound.cpp 

OBJS += \
./adapters/sfml1.6/core.o \
./adapters/sfml1.6/display.o \
./adapters/sfml1.6/event.o \
./adapters/sfml1.6/font.o \
./adapters/sfml1.6/graphics.o \
./adapters/sfml1.6/image.o \
./adapters/sfml1.6/input.o \
./adapters/sfml1.6/sound.o 

CPP_DEPS += \
./adapters/sfml1.6/core.d \
./adapters/sfml1.6/display.d \
./adapters/sfml1.6/event.d \
./adapters/sfml1.6/font.d \
./adapters/sfml1.6/graphics.d \
./adapters/sfml1.6/image.d \
./adapters/sfml1.6/input.d \
./adapters/sfml1.6/sound.d 


# Each subdirectory must supply rules for building sources it contributes
adapters/sfml1.6/%.o: ../adapters/sfml1.6/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I"BUILD_PATH" -I"BUILD_PATH/.." -O3 -Wall -c -fmessage-length=0 -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


