################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../adapters/allegro5.0/core.cpp \
../adapters/allegro5.0/display.cpp \
../adapters/allegro5.0/event.cpp \
../adapters/allegro5.0/font.cpp \
../adapters/allegro5.0/graphics.cpp \
../adapters/allegro5.0/image.cpp \
../adapters/allegro5.0/input.cpp \
../adapters/allegro5.0/sound.cpp 

OBJS += \
./adapters/allegro5.0/core.o \
./adapters/allegro5.0/display.o \
./adapters/allegro5.0/event.o \
./adapters/allegro5.0/font.o \
./adapters/allegro5.0/graphics.o \
./adapters/allegro5.0/image.o \
./adapters/allegro5.0/input.o \
./adapters/allegro5.0/sound.o 

CPP_DEPS += \
./adapters/allegro5.0/core.d \
./adapters/allegro5.0/display.d \
./adapters/allegro5.0/event.d \
./adapters/allegro5.0/font.d \
./adapters/allegro5.0/graphics.d \
./adapters/allegro5.0/image.d \
./adapters/allegro5.0/input.d \
./adapters/allegro5.0/sound.d 


# Each subdirectory must supply rules for building sources it contributes
adapters/allegro5.0/%.o: ../adapters/allegro5.0/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I"BUILD_PATH" -I"BUILD_PATH/.." -O3 -Wall -c -fmessage-length=0 -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


