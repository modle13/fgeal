################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../adapters/sdl1.2/core.cpp \
../adapters/sdl1.2/display.cpp \
../adapters/sdl1.2/event.cpp \
../adapters/sdl1.2/font.cpp \
../adapters/sdl1.2/graphics.cpp \
../adapters/sdl1.2/image.cpp \
../adapters/sdl1.2/input.cpp \
../adapters/sdl1.2/sound.cpp 

OBJS += \
./adapters/sdl1.2/core.o \
./adapters/sdl1.2/display.o \
./adapters/sdl1.2/event.o \
./adapters/sdl1.2/font.o \
./adapters/sdl1.2/graphics.o \
./adapters/sdl1.2/image.o \
./adapters/sdl1.2/input.o \
./adapters/sdl1.2/sound.o 

CPP_DEPS += \
./adapters/sdl1.2/core.d \
./adapters/sdl1.2/display.d \
./adapters/sdl1.2/event.d \
./adapters/sdl1.2/font.d \
./adapters/sdl1.2/graphics.d \
./adapters/sdl1.2/image.d \
./adapters/sdl1.2/input.d \
./adapters/sdl1.2/sound.d 


# Each subdirectory must supply rules for building sources it contributes
adapters/sdl1.2/%.o: ../adapters/sdl1.2/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I"BUILD_PATH" -I"BUILD_PATH/.." -O3 -Wall -c -fmessage-length=0 -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


