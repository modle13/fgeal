#!/bin/bash
# 
#  This file is part of fgeal.
# 
#  fgeal - Faruolo's game engine/library abstraction layer
#  Copyright (C) 2015  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
# 
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License, or (at your option) any later version.
# 
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
# 
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

# This script will copy (overwriting if needed) the eclipse build configuration 
# folder (passed by the first argument) and its content to a folder with the same 
# name, but inside the /project/build folder.
#
# It will also replace all ocurrences of ${pwd} (the current folder) string 
# inside the copied files with the token BUILD_CONFIG.
#
# These tokens can be later replaced by the ./configure script when building the 
# library. 
#  
# How to use: ./extract_makefiles.sh <build-configuration-name> 
#
# Note that this requires that the build configutation folder have the same name 
# as the build configuration itself.
#

echo "### Preparing makefiles for prime time ###"
echo "- Erasing content of $1 build folder (except configure.sh)..."
mv project/build/$1/configure.sh .
rm project/build/$1/* -rf
mv configure.sh project/build/$1/
echo "- Performing make clean in $1 folder..."
cd $1
make clean
cd ..
echo "- Copying makefiles from $1 folder to build folder..."
cp $1/* -r project/build/$1/
echo "- Replacing current folder with generic BUILD_PATH token..."
./project/touch_makefiles.sh ./project/build/$1/ ${PWD} BUILD_PATH

echo "Done."
