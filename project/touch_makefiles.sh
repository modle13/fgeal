#!/bin/bash
# 
#  This file is part of fgeal.
# 
#  fgeal - Faruolo's game engine/library abstraction layer
#  Copyright (C) 2015  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
# 
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License, or (at your option) any later version.
# 
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
# 
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

# This script will patch all .mk files inside the folder given by the first 
# argument. The patch replaces all text ocurrences of the second argument with 
# the token given by third argument.
#
# Usage ./touch_makefiles.sh <folder> <occurrence_to_replace> <replacement_token>
#

DPATH=$1
OLD=$2
NEW=$3

echo "Will patch .mk files within the $DPATH folder."
echo "The patch will replace $OLD with $NEW" 

find $DPATH -name *.mk -type f -exec echo "Patched " {} \;
find $DPATH -name *.mk -type f -exec sed -i "s,$OLD,$NEW,g" {} \;
