/*
 * graphics.hpp
 *
 *  Created on: 21/06/2018
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2017  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef FGEAL_GRAPHICS_HPP_
#define FGEAL_GRAPHICS_HPP_
#include <ciso646>

#include "core.hpp"
#include "geometry.hpp"
#include "colors.hpp"

namespace fgeal
{
	class Graphics
	{
		Graphics();  // non-instantiable

		public:

		/** Sets 'image' as the target of any subsequent drawing calls (including primitives and font drawing).
		 *  If 'image' is null, sets the current display as target. */
		static void setDrawTarget(const Image* image);

		/** Sets 'display' as the target of any subsequent drawing calls (including primitives and font drawing). */
		static void setDrawTarget(const Display& display);

		/** Sets the current (default) display as the target of any subsequent drawing calls (including primitives and font drawing). */
		static void setDefaultDrawTarget();

		// Primitives =================================================================================================

		/** Draws a line segment in the current display. The line goes from point (x1, y1) to point (x2, y2). */
		static void drawLine(float x1, float y1, float x2, float y2, const Color& color=Color::WHITE);

		/** Draws a rectangle (outline) in the current display. The rectangle's top-left point will be drawn at (x, y)
		 *  and it's dimensions are given by 'width' and 'height' parameters. */
		static void drawRectangle(float x, float y, float width, float height, const Color& color=Color::WHITE);

		/** Draws a rectangle (filled) in the current display. The rectangle's top-left point will be drawn at (x, y)
		 *  and it's dimensions are given by 'width' and 'height' parameters. */
		static void drawFilledRectangle(float x, float y, float width, float height, const Color& color=Color::WHITE);

		/** Draws a circle (outline) in the current display. The circle's centre will be draw at (cx, cy),
		 *  with radius given by the 'radius' parameter. */
		static void drawCircle(float cx, float cy, float radius, const Color& color=Color::WHITE);

		/** Draws a circle (filled) in the current display. The circle's centre will be draw at (cx, cy),
		 *  with radius given by the 'radius' parameter. */
		static void drawFilledCircle(float cx, float cy, float radius, const Color& color=Color::WHITE);

		/** Draws a ellipse (outline) in the current display. The ellipse's centre will be draw at (cx, cy),
		 *  with horizontal and vertical radii given by the 'radiusX' and 'radiusY' parameters, respectively. */
		static void drawEllipse(float cx, float cy, float radiusX, float radiusY, const Color& color=Color::WHITE);

		/** Draws a ellipse (filled) in the current display. The ellipse's centre will be draw at (cx, cy),
		 *  with horizontal and vertical radii given by the 'radiusX' and 'radiusY' parameters, respectively. */
		static void drawFilledEllipse(float cx, float cy, float radiusX, float radiusY, const Color& color=Color::WHITE);

		/** Draws a triangle (outline) in the current display. The triangle will be drawn as if it's points were (x1, y1),
		 * (x2, y2) and (x3, y3). */
		static void drawTriangle(float x1, float y1, float x2, float y2, float x3, float y3, const Color& color=Color::WHITE);

		/** Draws a triangle (filled) in the current display. The triangle will be drawn as if it's points were (x1, y1),
		 * (x2, y2) and (x3, y3). */
		static void drawFilledTriangle(float x1, float y1, float x2, float y2, float x3, float y3, const Color& color=Color::WHITE);

		/** Draws a quadrangle (quadrilateral) (outline) in the current display. The quadrangle will be drawn as if it's
		 * points were (x1, y1), (x2, y2), (x3, y3) and (x4, y4). */
		static void drawQuadrangle(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4, const Color& color=Color::WHITE);

		/** Draws a quadrangle (quadrilateral) (filled) in the current display. The quadrangle will be drawn as if it's
		 * points were (x1, y1), (x2, y2), (x3, y3) and (x4, y4). */
		static void drawFilledQuadrangle(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4, const Color& color=Color::WHITE);

		/** Draws a circular arc in the current display. The arc will be draw with center at ('cx', 'cy') and radius 'r',
		 * starting from angle 'a1' and ending upon reaching 'a2', counter-clockwise. */
		static void drawArc(float cx, float cy, float r, float a1, float a2, const Color& color=Color::WHITE);

		/** Draws a circle sector (aka circular sector or "pie slice") (outline) in the current display. The sector
		 * will be drawn with center at ('cx', 'cy') and radius 'r', starting from angle 'a1' and ending upon reaching
		 * 'a2', counter-clockwise. */
		static void drawCircleSector(float cx, float cy, float r, float a1, float a2, const Color& color=Color::WHITE);

		/** Draws a circle sector (aka circular sector or "pie slice") (filled) in the current display. The sector will
		 *  be drawn with center at ('cx', 'cy') and radius 'r', starting from angle 'a1' and ending upon reaching
		 *  'a2', counter-clockwise. */
		static void drawFilledCircleSector(float cx, float cy, float r, float a1, float a2, const Color& color=Color::WHITE);

		/** Draws a rounded-corner rectangle (outline) in the current display. The rectangle's top-left point will be
		 *  drawn at (x, y) and it's dimensions are given by 'width' and 'height' parameters. Its corners will be
		 *  rounded by an arc of radius 'rd'. */
		static void drawRoundedRectangle(float x, float y, float width, float height, float rd, const Color& color=Color::WHITE);

		/** Draws a rounded-corner rectangle (filled) in the current display. The rectangle's top-left point will be
		 *  drawn at (x, y) and it's dimensions are given by 'width' and 'height' parameters. Its corners will be
		 *  rounded by an arc of radius 'rd'. */
		static void drawFilledRoundedRectangle(float x, float y, float width, float height, float rd, const Color& color=Color::WHITE);

		private:  // for internal use
		inline static void genericDrawRoundedRectangle(float x, float y, float w, float h, float rd, const Color& c) {
			Graphics::drawArc(x+rd, y+rd, rd, M_PI_2, M_PI, c);
			Graphics::drawArc(x+w-rd, y+rd, rd, 0, M_PI_2, c);
			Graphics::drawArc(x+rd, y+h-rd, rd, M_PI, M_3_PI_2, c);
			Graphics::drawArc(x+w-rd, y+h-rd, rd, M_3_PI_2, 2*M_PI, c);
			Graphics::drawLine(x+rd, y, x+w-rd, y, c);
			Graphics::drawLine(x, y+rd, x, y+h-rd, c);
			Graphics::drawLine(x+w, y+rd, x+w, y+h-rd, c);
			Graphics::drawLine(x+rd, y+h, x+w-rd, y+h, c);
		}

		inline static void genericDrawFilledRoundedRectangle(float x, float y, float w, float h, float rd, const Color& c) {
			Graphics::drawFilledCircleSector(x+rd, y+rd, rd, M_PI_2, M_PI, c);
			Graphics::drawFilledCircleSector(x+w-rd, y+rd, rd, 0, M_PI_2, c);
			Graphics::drawFilledCircleSector(x+rd, y+h-rd, rd, M_PI, M_3_PI_2, c);
			Graphics::drawFilledCircleSector(x+w-rd, y+h-rd, rd, M_3_PI_2, 2*M_PI, c);
			Graphics::drawFilledRectangle(x+rd, y, w-2*rd, rd, c);
			Graphics::drawFilledRectangle(x, y+rd, w, h-2*rd, c);
			Graphics::drawFilledRectangle(x+rd, y+h-rd, w-2*rd, rd, c);
		}

		// Point/Rectangle versions. ==================================================================================
		public:

		/** Draws a line segment in the current display. The line goes from point p1 to point p2. */
		inline static void drawLine(const Point& p1, const Point& p2, const Color& color=Color::WHITE) {
			Graphics::drawLine(p1.x, p1.y, p2.x, p2.y, color);
		}

		/** Draws a rectangle (outline) in the current display. */
		inline static void drawRectangle(const Rectangle& rectangle, const Color& color=Color::WHITE) {
			Graphics::drawRectangle(rectangle.x, rectangle.y, rectangle.w, rectangle.h, color);
		}

		/** Draws a rectangle (filled) in the current display. */
		inline static void drawFilledRectangle(const Rectangle& rectangle, const Color& color=Color::WHITE) {
			Graphics::drawFilledRectangle(rectangle.x, rectangle.y, rectangle.w, rectangle.h, color);
		}

		/** Draws a circle (outline) in the current display. The circle's centre will be drawn in position the given
		 * by the 'center' parameter, with radius given by the 'radius' parameter. */
		inline static void drawCircle(const Point& center, float radius, const Color& color=Color::WHITE) {
			Graphics::drawCircle(center.x, center.y, radius, color);
		}

		/** Draws a circle (filled) in the current display. The circle's centre will be drawn in position the given
		 * by the 'center' parameter, with radius given by the 'radius' parameter. */
		inline static void drawFilledCircle(const Point& center, float radius, const Color& color=Color::WHITE) {
			Graphics::drawFilledCircle(center.x, center.y, radius, color);
		}

		/** Draws an ellipse (outline) in the current display. The circle's centre will be drawn in position the given
		 * by the 'center' parameter, with horizontal and vertical radii given by the 'radii' parameter (with horizontal
		 * radius on the 'x' field and the vertical radius in the 'y' field). */
		inline static void drawEllipse(const Point& center, const Point radii, const Color& color=Color::WHITE) {
			Graphics::drawEllipse(center.x, center.y, radii.x, radii.y, color);
		}

		/** Draws an ellipse (filled) in the current display. The circle's centre will be drawn in position the given
		 * by the 'center' parameter, with horizontal and vertical radii given by the 'radii' parameter (with horizontal
		 * radius on the 'x' field and the vertical radius in the 'y' field).  */
		inline static void drawFilledEllipse(const Point& center, const Point radius, const Color& color=Color::WHITE) {
			Graphics::drawFilledEllipse(center.x, center.y, radius.x, radius.y, color);
		}

		/** Draws a triangle (outline) in the current display. The triangle will be drawn as if it's points were 'p1', 'p2' and 'p3'. */
		inline static void drawTriangle(const Point& p1, const Point& p2, const Point& p3, const Color& color=Color::WHITE) {
			Graphics::drawTriangle(p1.x, p1.y, p2.x, p2.y, p3.x, p3.y, color);
		}

		/** Draws a triangle (filled) in the current display. The triangle will be drawn as if it's points were 'p1', 'p2' and 'p3'. */
		inline static void drawFilledTriangle(const Point& p1, const Point& p2, const Point& p3, const Color& color=Color::WHITE) {
			Graphics::drawFilledTriangle(p1.x, p1.y, p2.x, p2.y, p3.x, p3.y, color);
		}

		/** Draws a quadrangle (quadrilateral) (outline) in the current display. The quadrangle will be drawn as if it's points were 'p1', 'p2', 'p3' and 'p4'. */
		inline static void drawQuadrangle(const Point& p1, const Point& p2, const Point& p3, const Point& p4, const Color& color=Color::WHITE) {
			Graphics::drawQuadrangle(p1.x, p1.y, p2.x, p2.y, p3.x, p3.y, p4.x, p4.y, color);
		}

		/** Draws a quadrangle (quadrilateral) (filled) in the current display. The quadrangle will be drawn as if it's points were 'p1', 'p2', 'p3' and 'p4'. */
		inline static void drawFilledQuadrangle(const Point& p1, const Point& p2, const Point& p3, const Point& p4, const Color& color=Color::WHITE) {
			Graphics::drawFilledQuadrangle(p1.x, p1.y, p2.x, p2.y, p3.x, p3.y, p4.x, p4.y, color);
		}

		/** Draws a circular arc in the current display. The arc will be draw with center in the position given by the
		 *  'center' parameter, with radius 'r', starting from angle 'a1' and ending upon reaching 'a2', counter-clockwise. */
		inline static void drawArc(const Point& center, float r, float a1, float a2, const Color& color=Color::WHITE) {
			Graphics::drawArc(center.x, center.y, r, a1, a2, color);
		}

		/** Draws a circle sector (aka circular sector or "pie slice") (outline) in the current display. The sector
		 *  will be drawn with center at ('cx', 'cy') and radius 'r', starting from angle 'a1' and ending upon reaching
		 * 'a2', counter-clockwise. */
		inline static void drawCircleSector(const Point& center, float r, float a1, float a2, const Color& color=Color::WHITE) {
			Graphics::drawArc(center.x, center.y, r, a1, a2, color);
		}

		/** Draws a circle sector (aka circular sector or "pie slice") (filled) in the current display. The sector will
		 *  be drawn with center at ('cx', 'cy') and radius 'r', starting from angle 'a1' and ending upon reaching
		 *  'a2', counter-clockwise. */
		static void drawFilledCircleSector(const Point& center, float r, float a1, float a2, const Color& color=Color::WHITE) {
			Graphics::drawArc(center.x, center.y, r, a1, a2, color);
		}

		/** Draws a rounded-corner rectangle (outline) in the current display. It's corners will be rounded by an arc
		 *  of radius 'rd'. */
		static void drawRoundedRectangle(const Rectangle& rectangle, float rd, const Color& color=Color::WHITE) {
			Graphics::drawRoundedRectangle(rectangle.x, rectangle.y, rectangle.w, rectangle.h, rd, color);
		}

		/** Draws a rounded-corner rectangle (filled) in the current display. It's corners will be rounded by an arc
		 *  of radius 'rd'. */
		static void drawFilledRoundedRectangle(const Rectangle& rectangle, float rd, const Color& color=Color::WHITE) {
			Graphics::drawFilledRoundedRectangle(rectangle.x, rectangle.y, rectangle.w, rectangle.h, rd, color);
		}

		// Polygon primitives. ========================================================================================

		/** Draws a polygon (outline) defined by the points given by the 'x' and 'y' vectors. */
		static void drawPolygon(const std::vector<float>& x, const std::vector<float>& y, const Color& color=Color::WHITE);

		/** Draws a polygon (filled) defined by the points given by the 'x' and 'y' vectors. */
		static void drawFilledPolygon(const std::vector<float>& x, const std::vector<float>& y, const Color& color=Color::WHITE);

		/** Draws a 'n'-sided polygon (outline) defined by the points given by the 'x' and 'y' arrays.
		 *  The 'x' and 'y' arrays must have x and y coordinates respectively and both should have size of at least 'n'. */
		inline static void drawPolygon(float x[], float y[], unsigned n, const Color& color=Color::WHITE) {
			const std::vector<float> vx(x, x+n), vy(y, y+n);
			Graphics::drawPolygon(vx, vy, color);
		}

		/** Draws a 'n'-sided polygon (filled) defined by the points given by the 'x' and 'y' arrays.
		 *  The 'x' and 'y' arrays must have x and y coordinates respectively and both should have size of at least 'n'. */
		static void drawFilledPolygon(float x[], float y[], unsigned n, const Color& color=Color::WHITE) {
			const std::vector<float> vx(x, x+n), vy(y, y+n);
			Graphics::drawFilledPolygon(vx, vy, color);
		}

		/** Draws a 'n'-sided polygon (outline) defined by the points given by the 'pts' array.
		 *  'pts' must contain a series of x, y points (pairs), and have size of at least '2n'. */
		static void drawPolygon(float pts[], unsigned n, const Color& color=Color::WHITE) {
			std::vector<float> vx(n), vy(n);
			for(unsigned i = 0; i < n; i++) { vx[i] = pts[2*i]; vy[i] = pts[2*i+1]; }
			Graphics::drawPolygon(vx, vy, color);
		}

		/** Draws a 'n'-sided polygon (filled) defined by the points given by the 'pts' array.
		 *  'pts' must contain a series of x, y points (pairs), and have size of at least '2n'. */
		static void drawFilledPolygon(float pts[], unsigned n, const Color& color=Color::WHITE) {
			std::vector<float> vx(n), vy(n);
			for(unsigned i = 0; i < n; i++) { vx[i] = pts[2*i]; vy[i] = pts[2*i+1]; }
			Graphics::drawFilledPolygon(vx, vy, color);
		}

		/** Draws a polygon (outline) defined by the points given by the 'pts' vector.
		 *  'pts' should contain a series of x, y points (pairs). */
		static void drawPolygon(const std::vector<float>& pts, const Color& color=Color::WHITE) {
			const unsigned n = pts.size()/2;
			std::vector<float> x(n), y(n);
			for(unsigned i = 0; i < n; i++) { x[i] = pts[2*i]; y[i] = pts[2*i+1]; }
			Graphics::drawPolygon(x, y, color);
		}

		/** Draws a polygon (filled) defined by the points given by the 'pts' vector.
		 *  'pts' should contain a series of x, y points (pairs). */
		static void drawFilledPolygon(const std::vector<float>& pts, const Color& color=Color::WHITE) {
			const unsigned n = pts.size()/2;
			std::vector<float> x(n), y(n);
			for(unsigned i = 0; i < n; i++) { x[i] = pts[2*i]; y[i] = pts[2*i+1]; }
			Graphics::drawFilledPolygon(x, y, color);
		}

		/** Draws a 'n'-sided polygon (outline) defined by the points given by the 'pts' array.
		 *  'pts' should have size of at least 'n'. */
		inline static void drawPolygon(const Point pts[], unsigned n, const Color& color=Color::WHITE) {
			std::vector<float> x(n), y(n);
			for(unsigned i = 0; i < n; i++) { x[i] = pts[i].x; y[i] = pts[i].y; }
			Graphics::drawPolygon(x, y, color);
		}

		/** Draws a polygon (filled) defined by the points given by the 'pts' array. */
		inline static void drawFilledPolygon(const Point pts[], unsigned n, const Color& color=Color::WHITE) {
			std::vector<float> x(n), y(n);
			for(unsigned i = 0; i < n; i++) { x[i] = pts[i].x; y[i] = pts[i].y; }
			Graphics::drawFilledPolygon(x, y, color);
		}

		/** Draws a polygon (outline) defined by the points given by the 'pts' vector. */
		inline static void drawPolygon(const std::vector<Point>& pts, const Color& color=Color::WHITE) {
			const unsigned n = pts.size()/2;
			std::vector<float> x(n), y(n);
			for(unsigned i = 0; i < n; i++) { x[i] = pts[i].x; y[i] = pts[i].y; }
			Graphics::drawPolygon(x, y, color);
		}

		/** Draws a polygon (filled) defined by the points given by the 'pts' vector. */
		inline static void drawFilledPolygon(const std::vector<Point>& pts, const Color& color=Color::WHITE) {
			const unsigned n = pts.size()/2;
			std::vector<float> x(n), y(n);
			for(unsigned i = 0; i < n; i++) { x[i] = pts[i].x; y[i] = pts[i].y; }
			Graphics::drawFilledPolygon(x, y, color);
		}
	};
}

#endif /* FGEAL_GRAPHICS_HPP_ */
