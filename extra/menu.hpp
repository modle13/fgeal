/*
 * menu.hpp
 *
 *  Created on: 2/06/2017
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2017  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef FGEAL_EXTRA_MENU_HPP_
#define FGEAL_EXTRA_MENU_HPP_
#include <ciso646>

#include "fgeal/fgeal.hpp"

#include <vector>
#include <string>

namespace fgeal
{
	/** Very simple menu class to draw very simple menus. */
	class Menu
	{
		struct Entry
		{
			std::string label;
			bool enabled;
			void* userData;

			Entry(std::string str)
			: label(str),
			  enabled(true),
			  userData(null)
			{}
		};

		std::vector<Entry> entries;
		std::string title;

		unsigned selectedIndex;

		// The font used to draw the entries.
		Font* font;

		// Set to false to delete the passed font when deleting this menu
		bool isFontShared;

		// The font used to draw the title (default=font)
		Font* titleFont;

		// Set to false to delete the passed title font when deleting this menu
		bool isTitleFontShared;

		// the amount of vertical scrolling
		float scrollVerticalAmount;

		// Cached values to cap entry size when drawing.
		unsigned cachedWidth, cachedFieldLength;

		void updateScroll();

		public:

		Rectangle bounds;

		// The menu background color (default=black)
		Color bgColor;

		// The menu border color (default=entryColor)
		Color borderColor;

		// The color used to draw the entries' text.
		Color entryColor;

		// The colors used to draw the focused entry text (default=white)
		Color focusedEntryFontColor;

		// The color used to draw the focused entry's background (default=entryColor)
		Color focusedEntryBgColor;

		// The color used to draw the title (default=entryColor)
		Color titleColor;

		enum LayoutMode { PACK_ENTRIES, STRETCH_SPACING } layoutMode;
		float entrySpacing; // the spacing between entries
		bool entrySpacingIsRelative; // if true, 'entrySpacing' is a fraction relative to the font's size

		// if true, the menu will be vertically scrolled when the number of entries is big enough to exceed the menu's height. (default=true)
		bool scrollVerticalEnabled;

		// If true, entries' texts are capped to fit the menu bounds when drawn. No change is done on the entries actual labels. (default=true)
		bool entryClippingEnabled;

		// if true, when the cursor is moved up at the zero-index, it will be moved to last index, and vice-versa. (default=false)
		bool cursorWrapAroundEnabled;

		/** Creates a menu with the given bounds, font, color and (optionally) title. The passed font is assumed to be
		 *  shared and will never be deleted (even when this menu is destroyed). */
		Menu(const Rectangle bounds=Rectangle(), Font* font=null, const Color fontColor=Color::WHITE, const std::string title="");
		~Menu();

		/** Adds an entry to this menu, at the given index.  If 'index' is negative, the entry is created at the end of
		 *  the menu. */
		void addEntry(std::string label, int index=-1);

		/** Remove from this menu the entry at the given index. if the given index is out of bounds, nothing is done and
		 *  the menu is left unchanged. */
		void removeEntry(unsigned index);

		/** Returns the number of entries on this menu. */
		unsigned getEntryCount();

		/** Alias to getEntryCount() */
		unsigned getNumberOfEntries();

		/** Returns a reference to the entry at the given index. */
		Entry& at(int index);

		/** Same as at(), but no bounds check is done. */
		Entry& operator [] (int index);

		/** Sets the current entry. If the given entry is not on this menu, nothing is done and the menu is left unchanged. */
		void setSelectedEntry(const Entry& entry);

		/** Returns a reference to the current entry. */
		Entry& getSelectedEntry();

		/** Safe way to set the selected index */
		void setSelectedIndex(unsigned index);

		/** Returns the index of the current entry. */
		unsigned getSelectedIndex();

		/** Decrement the selected index in a safe way, moving the selected entry up. */
		void moveCursorUp();

		/** Increment the selected index in a safe way, moving the selected entry down. */
		void moveCursorDown();

		/** Alias to moveCursorUp(), but returning a reference for the menu as well. */
		Menu& operator --();

		/** Alias to moveCursorDown(), but returning a reference for the menu as well. */
		Menu& operator ++();

		/** Sets this menu's main font, used to draw the entries' text, replacing any previous font.
		 *  If the "isFontShared" argument is true (default), then the font is assumed to be shared and won't be deleted when
		 *  this menu is destroyed. Otherwise, it will be deleted automatically either when this menu is destroyed or when the
		 *  font is replaced again by this method. */
		void setFont(Font* font, bool isFontShared=true);

		/** Returns a reference to the menu's main font. */
		Font& getFont();

		/** Same as setFont(), but replaces the title font instead. If no title font is set, then the main font is used to
		 *  draw the entries' text. */
		void setTitleFont(Font* font, bool isFontShared=true);

		/** Returns a reference to the menu's title font. */
		Font& getTitleFont();

		/** Sets the color of this menu. The meaning of this is somewhat vague but it's the same as the color argument in the
		 *  Menu class constructor: an overall color theme.
		 *  For fine-tuning colors, modify the public fields of this class. */
		void setColor(const Color& color);

		/** (Optional) Sets this menu's title. */
		void setTitle(const std::string& txt);

		/** Returns a reference to this menu's title. */
		const std::string& getTitle() const;

		/** Draw the menu according the menu bounds and number of entries */
		void draw();
	};
}

#endif /* FGEAL_EXTRA_MENU_HPP_ */
