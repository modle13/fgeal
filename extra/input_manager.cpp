/*
 * input_manager.cpp
 *
 *  Created on: 23/05/2018
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2016  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "input_manager.hpp"
#include "game.hpp"

#include <algorithm>

namespace
{
	/// Returns true if the given container contains the specified element.
	template<typename CollectionType, typename ElementType>
	inline static bool collectionContains(const CollectionType& collection, const ElementType& element)
	{
		return std::find(collection.begin(), collection.end(), element) != collection.end();
	}

	/// Erases all instances of the specified element from the given container, if it is present.
	template<typename CollectionType, typename ElementType>
	inline static void collectionRemoveAll(CollectionType& collection, const ElementType& element)
	{
		typename CollectionType::iterator position = std::find(collection.begin(), collection.end(), element);
		while(position != collection.end())
		{
			collection.erase(position);
			position = std::find(collection.begin(), collection.end(), element);
		}
	}
}

// --------------------------------------------------------------------------------------------------------------

namespace fgeal
{
	InputManager::InputManager()
	: closeRequestFlag(false)
	{}

	/// Updates this manager and notifies all listeners about input events.
	void InputManager::update()
	{
		// add pending key listeners
		for(unsigned i = 0; i < keyListenersToAdd.size(); i++)
			if(not collectionContains(keyListeners, keyListenersToAdd[i]))
				keyListeners.push_back(keyListenersToAdd[i]);
		keyListenersToAdd.clear();

		// add pending mouse listeners
		for(unsigned i = 0; i < mouseListenersToAdd.size(); i++)
			if(not collectionContains(mouseListeners, mouseListenersToAdd[i]))
				mouseListeners.push_back(mouseListenersToAdd[i]);
		mouseListenersToAdd.clear();

		// clear close request flag
		closeRequestFlag = false;

		Event event;
		EventQueue& eventQueue = EventQueue::getInstance();
		while(eventQueue.hasEvents())
		{
			eventQueue.getNextEvent(&event);
			switch(event.getEventType())
			{
				case Event::TYPE_KEY_PRESS:
					for(unsigned i = 0; i < keyListeners.size(); i++)
						keyListeners[i]->onKeyPressed(event.getEventKeyCode());
					break;
				case Event::TYPE_KEY_RELEASE:
					for(unsigned i = 0; i < keyListeners.size(); i++)
						keyListeners[i]->onKeyReleased(event.getEventKeyCode());
					break;
				case Event::TYPE_MOUSE_BUTTON_PRESS:
					for(unsigned i = 0; i < mouseListeners.size(); i++)
						mouseListeners[i]->onMouseButtonPressed(event.getEventMouseButton(), event.getEventMouseX(), event.getEventMouseY());
					break;
				case Event::TYPE_MOUSE_BUTTON_RELEASE:
					for(unsigned i = 0; i < mouseListeners.size(); i++)
						mouseListeners[i]->onMouseButtonReleased(event.getEventMouseButton(), event.getEventMouseX(), event.getEventMouseY());
					break;
				case Event::TYPE_MOUSE_MOTION:
					for(unsigned i = 0; i < mouseListeners.size(); i++)
						mouseListeners[i]->onMouseMoved(0, 0, event.getEventMouseX(), event.getEventMouseY());
					break;
				case Event::TYPE_MOUSE_WHEEL_MOTION:
					for(unsigned i = 0; i < mouseListeners.size(); i++)
						mouseListeners[i]->onMouseWheelMoved(event.getEventMouseWheelMotionAmount());
					break;
				case Event::TYPE_JOYSTICK_BUTTON_PRESS:
					for(unsigned i = 0; i < joystickListeners.size(); i++)
						joystickListeners[i]->onJoystickButtonPressed(event.getEventJoystickIndex(), event.getEventJoystickButtonIndex());
					break;
				case Event::TYPE_JOYSTICK_BUTTON_RELEASE:
					for(unsigned i = 0; i < joystickListeners.size(); i++)
						joystickListeners[i]->onJoystickButtonReleased(event.getEventJoystickIndex(), event.getEventJoystickButtonIndex());
					break;
				case Event::TYPE_JOYSTICK_AXIS_MOTION:
					for(unsigned i = 0; i < joystickListeners.size(); i++)
					{
						joystickListeners[i]->onJoystickAxisMoved(event.getEventJoystickIndex(), event.getEventJoystickAxisIndex(), 0, event.getEventJoystickAxisPosition());
						if(event.getEventJoystickSecondAxisIndex() != -1)
							joystickListeners[i]->onJoystickAxisMoved(event.getEventJoystickIndex(), event.getEventJoystickSecondAxisIndex(), 0, event.getEventJoystickSecondAxisPosition());
					}
					break;
				case Event::TYPE_DISPLAY_CLOSURE:
					closeRequestFlag = true;
					break;
				case Event::TYPE_UNKNOWN:
					break;
			}
		}
	}

	bool InputManager::isCloseRequested()
	{
		return closeRequestFlag;
	}

	void InputManager::addKeyListener(KeyListener* listener)
	{
		keyListenersToAdd.push_back(listener);
	}

	void InputManager::addMouseListener(MouseListener* listener)
	{
		mouseListenersToAdd.push_back(listener);
	}

	void InputManager::addJoystickListener(JoystickListener* listener)
	{
		if(not collectionContains(joystickListeners, listener))
			joystickListeners.push_back(listener);
		// fuck the allListeners list
	}

	void InputManager::addListener(InputListener* listener)
	{
		this->addKeyListener(listener);
		this->addMouseListener(listener);
		this->addJoystickListener(listener);
	}

	void InputManager::removeKeyListener(KeyListener* listener)
	{
		collectionRemoveAll(keyListeners, listener);
	}

	void InputManager::removeMouseListener(MouseListener* listener)
	{
		collectionRemoveAll(mouseListeners, listener);
	}

	void InputManager::removeJoystickListener(JoystickListener* listener)
	{
		collectionRemoveAll(joystickListeners, listener);
	}

	void InputManager::removeListener(InputListener* listener)
	{
		collectionRemoveAll(keyListeners, listener);
		collectionRemoveAll(mouseListeners, listener);
		collectionRemoveAll(joystickListeners, listener);
	}

	void InputManager::removeAllKeyListeners()
	{
		keyListeners.clear();
	}

	void InputManager::removeAllMouseListeners()
	{
		mouseListeners.clear();
	}

	void InputManager::removeAllJoystickListeners()
	{
		joystickListeners.clear();
	}

	void InputManager::removeAllListeners()
	{
		removeAllKeyListeners();
		removeAllMouseListeners();
		removeAllJoystickListeners();
	}
}
