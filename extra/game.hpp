/*
 * game.hpp
 *
 *  Created on: 27/11/2016
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2016  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef FGEAL_EXTRA_GAME_HPP_
#define FGEAL_EXTRA_GAME_HPP_
#include <ciso646>

#include "fgeal/fgeal.hpp"
#include "fgeal/extra/input_manager.hpp"

#include <string>
#include <vector>

namespace fgeal
{
	/** A class to operate as state-based game. Each distinct "state" (main-menu, options, in-game, etc) is isolated into different Game::State's. */
	class Game extends InputManager::InputListener
	{
		public:

		/** Declaration of a State class. */
		struct State;

		/** Control whether the game is running. If false, the game didn't started or is being shutdown. */
		bool running;

		/** Control whether the game is paused. */
		bool paused;

		protected:

		/** The input manager instance. */
		InputManager input;

		/** The game's current display. Currently, a game can only be tied to a single display instance. */
		Display* display;

		/** This game's state list. */
		std::vector<State*> states;

		/** The initial state. It would be strange if this changes frequently. */
		State* initialState;

		/** The current state. */
		State* currentState;

		/** The state requested to be the current state. */
		State* requestedState;

		/** The minimum delta used with update() method. Any delta smaller than this value will accumulate and be used in next calls. */
		float minimumUpdateInterval;

		/** The maximum delta used with update() method. Any delta bigger than this value will be broken down in smaller deltas to be used in separated calls, sequentially. */
		float maximumUpdateInterval;

		/** The maximum FPS intended to be achieved. If negative, the game's FPS is left unbounded/unlocked. */
		short maxFps;

		/** Flag that controls whether the input manager is enabled or not. */
		bool inputManagerEnabled;

		private:

		/** The last time that the display was rendered. */
		float lastFrameUpdateTime;

		/** The amount of accumulated delta from previous loops. */
		float accumulatedDelta;

		/** The last time that the FPS was computed. */
		float lastFpsUpdateTime;

		/** The previous second's measured FPS. */
		unsigned short recordedFps;

		/** The current frame count of the current second. */
		unsigned short currentSecondFrameCount;

		// aux var to hold display creation parameters
		void* tempDisplayInitVarsPtr;

		// dummy state
		State* dummyState;

		Game(const Game&);  // disabled copy constructor
		Game& operator=(const Game&);  // disabled value-assignent

		public:

		/** Creates a game, optionally with the given name, given initial state, display width and display height.
		 *  This constructor also adds the given initial state, if non null, to the game's list of states.
		 *  Note that, although the initial state can be set later, the game needs an initial state to work properly. */
		Game(std::string title="unnamed game", State* initialState=null, unsigned width=640, unsigned height=480);

		/// Destructor
		virtual ~Game();

		/** The method that is supposed to do all initialization of static resources. */
		void initialize();

		/** Starts the game. After ending this game, a new should created (and the previous be deleted). */
		void start();

		/** The method that is supposed to do all rendering calls. */
		void render();

		/** The method that is supposed to do all game logic calls. */
		void update(float delta);

		/** Callback method that is supposed to deal when the an attempt to close the game was made through the display's close button.
		 *  This should return true if the game is supposed to close in these cases.
		 *  By default, this does nothing and simply returns true. Override this if a more refined behavior is needed.
		 *  This function is called only by the InputManager class. @see <input_manager.hpp> */
		virtual bool closeRequested();

		/** Returns the current FPS count. (not necessarily the target FPS) */
		short getFpsCount();

		/** Returns a reference to the game's display. */
		Display& getDisplay();

		// State-related

		/** Adds the given state to this game's state list. If no initial state was set, the given state is also set as the initial state.
		 *  Note that adding 2 states with the same ID is, currently, undefined behavior. */
		void addState(State* state);

		/** Returns the state associated with the given id. If there aren't any, returns null. */
		State* getState(int id);

		/** Sets this game's initial state. */
		void setInitialState(int id);

		/** Returns this game's initial state. */
		State* getInitialState();

		/** Enters the given state. */
		void enterState(int id);

		/** Chooses whether this game should use its input manager or not.
		 *  If enabled, the input manager will catch all input events and notify input listeners about them. This eliminates the need to
		 *  handle events directly with the event queue, but also prevents from doing it, since all events are consumed by the input manager.
		 *  If disabled, the input manager will not be used and input events should be handled through the event queue methods.
		 *  By default, the input manager is disabled (this may change in future releases). */
		void setInputManagerEnabled(bool choice=true);

		/** Returns true if the input manager is enabled, and false otherwise. @see setInputManagerEnabled() */
		bool isInputManagerEnabled();

		/** An optinal method that is called right before the call to this game's initialize() method.
		 *  This method is supposed to be overriden to do things that need to be done prior to the game and it's states' initialization. By default, it does nothing. */
		virtual void preInitialize();

		/** The method that is supposed to add all states of this game. This method will be called by this game's initialize() call.
		 *  However, it is optional to do this as, despite not recommended, the states can be added outside this method (but preferably before a call to Game::start()). */
		virtual void initializeStatesList();

		/** Methods overriden from InputListener. They delegate to the current state's methods. */
		virtual void onKeyPressed(Keyboard::Key k);
		virtual void onKeyReleased(Keyboard::Key k);
		virtual void onMouseButtonPressed(Mouse::Button, int x, int y);
		virtual void onMouseButtonReleased(Mouse::Button, int x, int y);
		virtual void onMouseMoved(int oldx, int oldy, int newx, int newy);
		virtual void onMouseWheelMoved(int change);
		virtual void onJoystickButtonPressed(unsigned joystick, unsigned button);
		virtual void onJoystickButtonReleased(unsigned joystick, unsigned button);
		virtual void onJoystickAxisMoved(unsigned joystick, unsigned axis, float oldValue, float newValue);

		private:  // internals

		float computeDelta();

		void computeFps();

		void setup();

		void loop();
	};

	/** A class that works as a game state. This class is supposed to be subclassed and contain code for initializing, rendering, updating, etc, of a single state. */
	class Game::State extends InputManager::InputListener
	{
		protected:
		
		/** A reference to this state's owner game instance. */
		Game& game;
		
		public:
		
		/** Creates a Game::State meant to be used with the given Game. */
		State(Game& game) : game(game) {}
		virtual ~State() {}

		/** The method that is supposed to do all initialization of static resources of this state.
		 *  Note: this method is only called once. If instead a per-state-entering loading is needed, do it inside State::onEnter(). */
		virtual void initialize() abstract;

		/** Returns the ID of this particular state. */
		virtual int getId() abstract;

		/** Callback to be called when the game enters this state. */
		virtual void onEnter() abstract;

		/** Callback to be called when the game leaves this state. */
		virtual void onLeave() abstract;

		/** The method that is supposed to do all rendering calls of this state. */
		virtual void render() abstract;

		/** The method that is supposed to do all game logic of this state. */
		virtual void update(float delta) abstract;

		/** Methods overriden from InputListener. By default they do nothing. Override to add custom input event handling code. */
		virtual void onKeyPressed(Keyboard::Key k);
		virtual void onKeyReleased(Keyboard::Key k);
		virtual void onMouseButtonPressed(Mouse::Button button, int x, int y);
		virtual void onMouseButtonReleased(Mouse::Button button, int x, int y);
		virtual void onMouseMoved(int oldx, int oldy, int newx, int newy);
		virtual void onMouseWheelMoved(int change);
		virtual void onJoystickButtonPressed(unsigned joystick, unsigned button);
		virtual void onJoystickButtonReleased(unsigned joystick, unsigned button);
		virtual void onJoystickAxisMoved(unsigned joystick, unsigned axis, float oldValue, float newValue);
	};
}

#endif /* FGEAL_EXTRA_GAME_HPP_ */
