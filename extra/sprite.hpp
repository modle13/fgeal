/*
 * sprite.hpp
 *
 *  Created on: 13/11/2016
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2016  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef FGEAL_EXTRA_SPRITE_HPP_
#define FGEAL_EXTRA_SPRITE_HPP_
#include <ciso646>

#include "fgeal/core.hpp"
#include "fgeal/image.hpp"
#include "fgeal/geometry.hpp"

#include <vector>

namespace fgeal
{
	/** A class to represent a sprite.
	 *
	 *  A sprite can be used to draw animations stored in Image's as
	 *  many different images, organized side-by-side.
	 *
	 *  Each image should be located inside an equally-sized region (called
	 *  "frame") of the given Image argument. Also, frames are expected to be
	 *  placed side-by-side, or from top-down (or both), similar to text
	 *  orientation on a page.
	 *
	 *	                                   +-------+
	 *	                                   |       |
	 *	                                   |   0   |
	 *	                                   |       |          +-------+-------+
	 *	                                   |       |          |       |       |
	 *	+-------+-------+-------+          +-------+          |   0   |   1   |
	 *	|       |       |       |          |       |          |       |       |
	 *	|   0   |   1   |   2   |    or    |   1   |    or    |       |       |
	 *	|       |       |       |          |       |          +-------+-------+
	 *	|       |       |       |          |       |          |       |
	 *	+-------+-------+-------+          +-------+          |   2   |
	 *	                                   |       |          |       |
	 *	                                   |   2   |          |       |
	 *	                                   |       |          +-------+
	 *	                                   |       |
	 *	                                   +-------+
	 *
	 *  The given Image will be read fully, considering each rectangle with
	 *  size [width x height] an additional frame. The frames are parsed
	 *  left-to-right, top-down, until there is no remaining space left.
	 *
	 *  An offset on the given Image argument can be specified, so images will
	 *  be read starting from it.
	 *
	 *  +========================+
	 *  ||		`               ||
	 *  ||offset`               ||
	 *  ||``````+-------+-------+
	 *  ||      |   0   |   1   |
	 *  ||      |       |       |
	 *  ||      +-------+-------+
	 *  ||      |   2   |       ||
	 *  ||      |       |       ||
	 *  ||      +-------+       ||
	 *	...                     ...
	 *
	 *  Typically, the source Image for the sprite should have dimensions
	 *  (subtracting offsets) that are integer multiples of the sprite's
	 *  dimensions. However this is not required and any leftover space smaller
	 *  than the sprite's dimensions is ignored.
	 *
	 *  +===========================+
	 *  ||		`                  ||
	 *  ||offset`                  ||
	 *  ||``````+-------+-------+  ||
	 *  ||      |   0   |   1   |  ||
	 *  ||      |       |       |  ||
	 *  ||      +-------+-------+``||
	 *  ||                      `  <--- leftover space
	 *	+===========================+
	 *
	 *	Also, it is possible to specify the number of frames to read, ignoring
	 *	any remaining space which could be used as frames.
	 *
	 *  The animation sequence, by default, will be a sequence of frames, from
	 *  frame 0 to frame N-1, being N the number of frames read. This way, the
	 *  order of the images that will appear will be the same as how they were
	 *  laid out in the source Image. However, a different sequence of frames
	 *  can be specified, by properly modifying the 'frameSequence' attribute.
	 *	An arbitrary order can be specified, including repeating indexes.
	 *
	 *	With 5 frames read:
	 *	[0, 1, 2, 3, 4]      default (same order as laid out in source image)
	 *	[0, 1, 0, 1, 0]      valid   (some frames can be left unused, frames can
	 *	be repeated)
	 *	[0, 1, 2, 1]         valid   (a sequence can be smaller than the number
	 *	of frames)
	 *	[1, 4, 2, 3, 2, 1]   valid   (a sequence can be bigger than the number
	 *	of frames)
	 *	[3, 1, 6, 2]         invalid (6 is not a valid raw index - with 5
	 *	frames, the range of valid indexes is 0 to 4)
	 *
	 *	The animation is updated through 2 ways: either by manually setting the
	 *	current frame **sequence** index or by using the
	 *	Sprite::computeCurrentFrame() method. If the animation is updated
	 *	manually, the attribute Sprite::currentFrameSequenceIndex needs to be
	 *	updated each time an update is desired. If the method
	 *	Sprite::computeCurrentFrame() is going to be used, it needs to be called
	 *	before each Sprite::draw() call. This will update the current frame
	 *	sequence index according to the current time and the specified duration
	 *	of each frame (Sprite::duration attribute). If duration is negative or
	 *	zero, no update is peformed, though. If framePhaseOffset is specified,
	 *	an additional time shift is performed in the calculation.
	 *
	 *	When drawing, the sprite's current is drawn, according to the frame
	 *	sequence order. Normally the draw coordinates are defined in terms of
	 *	the Sprite's upper-left corner. However an arbitrary pixel within its
	 *	frame can be defined (other than the Sprite's upper-left corner) for
	 *	that. This works effectively by offsetting (negative) the whole frame
	 *	at the destiny position. In some cases, this is convenient, especially
	 *	if rotations are applied to the Sprite.
	 *
	 * */
	struct Sprite
	{
		/** An image to be used as a sprite sheet for this Sprite. */
		Image* image;

		/** Sprite's dimensions. */
		const unsigned width, height;

		/** An optional offset to the frame's coordinates on the raw image.
		 *  Use this when the desired sprites are located starting from this
		 *  offset. */
		const int rawOffsetX, rawOffsetY;

		/** The number of raw indices loaded in this Sprite. */
		const unsigned numberOfFrames;

		/** Point from which the sprite will be drawn. Works as an (negative)
		 *  offset on the frame when drawing. Normally (0, 0) is used
		 *  (top-left corner) but others can be used (i.e. center point
		 *  (width/2, height/2)) */
		int referencePixelX, referencePixelY;

		/** The sequence of frames (raw indices) to be used. By default the
		 *  sequence is 0, 1, 2, ... */
		std::vector<unsigned> frameSequence;

		/** The current frame sequence index. Note that this index refers to
		 *  an entry in the frame sequence, not the raw index frame on the
		 *  image/sheet. */
		unsigned currentFrameSequenceIndex;

		/** An optional sprite flipping mode. By default, a sprite is not
		 *  flipped (FLIP_NONE), but it can be set as horizontally
		 *  (FLIP_HORIZONTAL) or vertically(FLIP_VERTICAL) flipped. If you
		 *  intend to flip both horizontally and vertically, set a 180-degree
		 *  rotation instead. */
		Image::FlipMode flipmode;

		/** An optional sprite scaling factor. By default, scale is (1.0, 1.0),
		 *  which means no resizing/zoom is applied. Negative values produces
		 *  undefined behavior. */
		Vector2D scale;

		/** An optional sprite rotation angle. By default, the angle is set as 0
		 *  (zero) radians. Currently the point of rotation is the sprite's
		 *  middle point. */
		float angle;

		// Animation synchronization

		/** The duration between each frame. This can be ignored if the
		 *  animation process is managed manually.  */
		float duration;

		/** An optional time offset to be applied when computing the current
		 *  frame. This can be ignored if the animation process is managed
		 *  manually. */
		float framePhaseOffset;

		private:
		bool imageIsOwned;

		// aux variables
		const unsigned rawRowCount, rawColumnsCount;

		public:

		/** Creates a new animated Sprite using frames contained in the given
		 *  Image. The frames will be interpreted a being equally sized, with
		 *  the dimensions specified by 'width' and 'height'. They can be placed
		 *  in the image horizontally, vertically, or as a grid.
		 *
		 *  The dimensions of the source image should be an integer multiple of
		 *  the frames dimensions. If not, all remaining space is ignored.
		 *
		 *  The duration of each frame, in seconds, will be the same as the
		 *  'frameDuration' argument. If zero or a negative value is passed, no
		 *  frame passing will occur.
		 *
		 *  Up to 'maxRawFramesToLoad' raw frames will be attempted to be read
		 *  from the sheet. If 'maxRawFramesToLoad' is negative or zero, as many
		 *  as possible raw frames will be read.
		 *
		 *  Raw frames will be read starting from the given offsets (rawOffsetX,
		 *  rawOffsetY) which, by default, are (0, 0) (no offset).
		 *
		 *  If 'manageSheetDeletion' is true, the sheet image will be deleted
		 *  when this Sprite gets deleted. By default it is false, needing to be
		 *  deleted manually/externally.
		 *
		 *  The Sprite will have a default frame sequence corresponding to the
		 *  raw frame numbers, starting with frame 0. The frame sequence may be
		 *  modified afterwards.
		 *
		 *  By default, the Sprite will have its reference pixel as its
		 *  upper-left corner (0,0).  */
		Sprite(Image* image, unsigned width, unsigned height,
				float frameDuration=-1, int maxRawFramesToLoad=-1,
				int rawOffsetX=0, int rawOffsetY=0,
				bool manageSheetDeletion=false);

		/** Creates a new animated Sprite using the same values as in the given
		 *  sprite object.
		 *
		 *  If 'manageSheetDeletion' is true, the sheet image will be deleted
		 *  when this Sprite gets deleted. You really should pass this as false
		 *  here, unless you really know what you are doing. */
		Sprite(const Sprite& sprite, bool manageSheetDeletion);

		~Sprite();

		/** Returns the frame region of the given raw frame index. */
		Rectangle getFrame(unsigned rawIndex);

		/** Computes the current frame sequence index, based on time and this
		 *  Sprite's duration. This method needs to be called before each call
		 *  to Sprite::draw() to update this Sprite's current frame. Calling
		 *  this method is not needed, however, if the animation process is
		 *  managed manually (with possibly other criteria), by manipulating the
		 *  Sprite::currentFrameSequenceIndex field. */
		void computeCurrentFrame();

		/** Returns the current frame region on this' sprite's image. */
		Rectangle getCurrentFrame();

		/** Draws this sprite at the specified window position.*/
		void draw(float x=0, float y=0);
	};
}

#endif /* FGEAL_EXTRA_SPRITE_HPP_ */
