/*
 * menu.cpp
 *
 *  Created on: 2/06/2017
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2017  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "menu.hpp"

using std::string;

namespace fgeal
{
	Menu::Menu(Rectangle bounds, Font* font, Color color, string title)
	 : entries(),
	   title(title),
	   selectedIndex(0),
	   font(font), isFontShared(true), titleFont(font), isTitleFontShared(true),
	   scrollVerticalAmount(0), cachedWidth(0), cachedFieldLength(30),
	   bounds(bounds), bgColor(Color::BLACK), borderColor(color),
	   entryColor(color), focusedEntryFontColor(Color::WHITE), focusedEntryBgColor(color), titleColor(entryColor),
	   layoutMode(PACK_ENTRIES), entrySpacing(0.1), entrySpacingIsRelative(true),
	   scrollVerticalEnabled(true), entryClippingEnabled(true), cursorWrapAroundEnabled(false)
	{}

	Menu::~Menu()
	{
		if(not isFontShared and font != null)
			delete font;
		if(titleFont != font and not isTitleFontShared and titleFont != null)
			delete titleFont;

		font = titleFont = null;
	}

	void Menu::addEntry(string label, int index)
	{
		if(index < 0)
			entries.push_back(Entry(label));
		else
			entries.insert(entries.begin()+index, Entry(label));

		if(entries.size() == 1)
		{
			selectedIndex = 0;
		}
	}

	void Menu::removeEntry(unsigned index)
	{
		if(index < 0 || index > entries.size()-1)
			return;

		if(index == selectedIndex)
		{
			if(entries.size() == 1)
			{
				selectedIndex = 0;
			}

			else if(index == entries.size()-1)
			{
				selectedIndex = index-1;
			}

			else
			{
				selectedIndex = index+1;
			}
		}
		entries.erase(entries.begin()+index);
	}

	unsigned Menu::getEntryCount()
	{
		return entries.size();
	}

	unsigned Menu::getNumberOfEntries()
	{
		return getEntryCount();
	}

	Menu::Entry& Menu::at(int index)
	{
		return entries.at(index);
	}

	Menu::Entry& Menu::operator [] (int index)
	{
		return entries[index];
	}

	void Menu::setSelectedEntry(const Entry& entry)
	{
		for(unsigned i = 0; i < entries.size(); i++)
			if(&entries[i] == &entry)
			{
				selectedIndex = i;
				break;
			}

		updateScroll();
	}

	Menu::Entry& Menu::getSelectedEntry()
	{
		return entries[selectedIndex];
	}

	void Menu::setSelectedIndex(unsigned index)
	{
		if(index + 1 > entries.size())
			return;

		updateScroll();
		selectedIndex = index;
	}

	unsigned Menu::getSelectedIndex()
	{
		return selectedIndex;
	}

	void Menu::moveCursorUp()
	{
		if(selectedIndex > 0)
			selectedIndex--;
		else if(cursorWrapAroundEnabled and entries.size() > 0)
			selectedIndex = entries.size()-1;

		updateScroll();
	}

	void Menu::moveCursorDown()
	{
		if(selectedIndex + 1 < entries.size())
			selectedIndex++;
		else if(cursorWrapAroundEnabled)
			selectedIndex = 0;

		updateScroll();
	}

	Menu& Menu::operator --()
	{
		this->moveCursorUp();
		return *this;
	}

	Menu& Menu::operator ++()
	{
		this->moveCursorDown();
		return *this;
	}

	void Menu::setFont(Font* f, bool shared)
	{
		if(not isFontShared and font != null and font != titleFont)
			delete font;

		font = f;
		isFontShared = shared;

		if(titleFont == null)
		{
			titleFont = f;
			isTitleFontShared = shared;
		}
	}

	Font& Menu::getFont()
	{
		return *font;
	}

	void Menu::setTitleFont(Font* f, bool shared)
	{
		if(not isTitleFontShared and titleFont != null and titleFont != font)
			delete titleFont;

		titleFont = f;
		isTitleFontShared = true;
	}

	Font& Menu::getTitleFont()
	{
		return *titleFont;
	}

	void Menu::setColor(const Color& color)
	{
		borderColor = entryColor = focusedEntryBgColor = color;
	}

	void Menu::setTitle(const string& txt)
	{
		title = txt;
	}

	const string& Menu::getTitle() const
	{
		return title;
	}

	void Menu::updateScroll()
	{
		const float offset = title.empty()? 0 : font->getHeight(),
					distanceBetween = entrySpacingIsRelative? font->getHeight() * (1+entrySpacing) : entrySpacing;

		while(offset + selectedIndex * distanceBetween < scrollVerticalAmount)
			scrollVerticalAmount -= distanceBetween;

		while(offset + selectedIndex * distanceBetween > bounds.h + scrollVerticalAmount)
			scrollVerticalAmount += distanceBetween;
	}

	/** Draw the menu according the menu bounds and number of entries */
	void Menu::draw()
	{
//		if(bg != null)
//			bg->draw(bounds.x, bounds.y);

		Graphics::drawFilledRectangle(bounds.x, bounds.y, bounds.w, bounds.h, bgColor);
		Graphics::drawRectangle(bounds.x, bounds.y, bounds.w, bounds.h, borderColor);

		float distanceBetween = entrySpacingIsRelative? font->getHeight() * (1+entrySpacing) : entrySpacing;

		if(layoutMode == STRETCH_SPACING)
			distanceBetween = (bounds.h-font->getHeight()) / ((float) entries.size() + (title.empty()?0:1));

		float offset = (title.empty()?0:font->getHeight()) - (scrollVerticalEnabled and layoutMode != STRETCH_SPACING? scrollVerticalAmount : 0);

		if(not title.empty())
			titleFont->drawText(title, bounds.x, bounds.y, titleColor);

		if((int) cachedWidth != bounds.w)
		{
			cachedWidth = bounds.w;
			string tmp = "_";
			while(font->getTextWidth(tmp) < cachedWidth)
				tmp.append("_");

			cachedFieldLength = tmp.size()-1;
		}

		for(unsigned i = 0; i < entries.size(); i++, offset += distanceBetween)
		{
			if(offset < 0
			or offset > bounds.h)
				continue;

			const bool enabled = entries[i].enabled;
			string label = entries[i].label;
			if(entryClippingEnabled and label.length() > cachedFieldLength)
				do label = "..."+label.substr(4); while(label.length() > cachedFieldLength);

			if(i == selectedIndex)
			{
				Graphics::drawFilledRectangle(bounds.x, bounds.y + offset, bounds.w, font->getHeight()+2, focusedEntryBgColor);
				font->drawText(label, bounds.x+2, bounds.y + offset, focusedEntryFontColor);
			}
			else
				font->drawText(label, bounds.x+2, bounds.y + offset, enabled? entryColor : Color::GREY);
		}
	}
}
