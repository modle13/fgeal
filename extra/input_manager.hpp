/*
 * input_manager.hpp
 *
 *  Created on: 23/05/2018
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2016  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef FGEAL_EXTRA_INPUT_MANAGER_HPP_
#define FGEAL_EXTRA_INPUT_MANAGER_HPP_
#include <ciso646>

#include "fgeal/fgeal.hpp"

namespace fgeal
{
	/**  A class that manage input events and notify registered listeners about them.
	 * To use this class, its needed to make sure that each call to Game::update()
	 * should call InputManager::update() as well. Its adviseable to make each game
	 * state subclass also a subclass of InputListener, and react to input events.
	 *   Note that this manager consumes all events from the current event queue. So
	 * to deal with the event queue directly, this manager must not be used at all. */
	class InputManager
	{
		private:
		bool closeRequestFlag;

		public:
		InputManager();

		/// Updates this manager and notifies all listeners about input events.
		void update();

		/// Returns true if an attempt to close the display has been done by the user.
		bool isCloseRequested();

		/// Describes classes capable of responding to key presses
		class KeyListener
		{
			public:
			/**
			 * Notification that a key was pressed
			 *
			 * @param key The key code that was pressed
			 */
			virtual void onKeyPressed(Keyboard::Key k) abstract;

			/**
			 * Notification that a key was released
			 *
			 * @param key The key code that was released
			 */
			virtual void onKeyReleased(Keyboard::Key k) abstract;

			/// Virtual destructor
			virtual ~KeyListener() {}
		};

		/**
		 * Add a key listener to be notified of key input events
		 *
		 * @param listener The listener to be notified
		 */
		void addKeyListener(KeyListener* listener);

		/**
		 * Remove a key listener that will no longer be notified
		 *
		 * @param listener The listen to be removed
		 */
		void removeKeyListener(KeyListener* listener);

		/**
		 * Remove all the key listeners from this input manager
		 */
		void removeAllKeyListeners();

		private:
		std::vector<KeyListener*> keyListeners;
		std::vector<KeyListener*> keyListenersToAdd;

		public:
		/// Description of classes that respond to mouse related input events
		class MouseListener
		{
			public:
			/**
			 * Notification that a mouse button was pressed
			 *
			 * @param button The button that was pressed
			 * @param x The x position of the mouse when the button was pressed
			 * @param y The y position of the mouse when the button was pressed
			 */
			virtual void onMouseButtonPressed(Mouse::Button, int x, int y) abstract;

			/**
			 * Notification that a mouse button was released
			 *
			 * @param button The button that was released
			 * @param x The x position of the mouse when the button was released
			 * @param y The y position of the mouse when the button was released
			 */
			virtual void onMouseButtonReleased(Mouse::Button, int x, int y) abstract;

			/**
			 * Notification that mouse cursor was moved
			 *
			 * @param oldx The old x position of the mouse
			 * @param oldy The old y position of the mouse
			 * @param newx The new x position of the mouse
			 * @param newy The new y position of the mouse
			 */
			virtual void onMouseMoved(int oldx, int oldy, int newx, int newy) abstract;

			/**
			 * Notification that the mouse wheel position was updated
			 *
			 * @param change The amount of the wheel has moved
			 */
			virtual void onMouseWheelMoved(int change) abstract;

			/// Virtual destructor
			virtual ~MouseListener() {}
		};

		/**
		 * Add a mouse listener to be notified of mouse input events
		 *
		 * @param listener The listener to be notified
		 */
		void addMouseListener(MouseListener* listener);

		/**
		 * Remove a mouse listener that will no longer be notified
		 *
		 * @param listener The listen to be removed
		 */
		void removeMouseListener(MouseListener* listener);

		/**
		 * Remove all the mouse listeners from this input manager
		 */
		void removeAllMouseListeners();

		private:
		std::vector<MouseListener*> mouseListeners;
		std::vector<MouseListener*> mouseListenersToAdd;

		public:
		/// Description of classes capable of responding to joystick events
		class JoystickListener
		{
			public:
			/**
			 * Notification that a button control has been pressed on
			 * the joystick.
			 *
			 * @param joystick The index of the joystick on which the control
			 * was pressed.
			 * @param button The index of the button pressed (starting at 1)
			 */
			virtual void onJoystickButtonPressed(unsigned joystick, unsigned button) abstract;

			/**
			 * Notification that a button control has been released on
			 * the joystick.
			 *
			 * @param joystick The index of the joystick on which the control
			 * was released.
			 * @param button The index of the button released (starting at 1)
			 */
			virtual void onJoystickButtonReleased(unsigned joystick, unsigned button) abstract;

			/**
			 * Notification that the axis has been moved on the joystick.
			 *
			 * @param joystick The index of the joystick on which the control
			 * was pressed.
			 */
			virtual void onJoystickAxisMoved(unsigned joystick, unsigned axis, float oldValue, float newValue) abstract;

			/// Virtual destructor
			virtual ~JoystickListener() {}
		};

		/**
		 * Add a joystick listener to be notified of joystick input events
		 *
		 * @param listener The listener to be notified
		 */
		void addJoystickListener(JoystickListener* listener);

		/**
		 * Remove a joystick listener that will no longer be notified
		 *
		 * @param listener The listen to be removed
		 */
		void removeJoystickListener(JoystickListener* listener);

		/**
		 * Remove all the joystick listeners from this input manager
		 */
		void removeAllJoystickListeners();

		private:
		std::vector<JoystickListener*> joystickListeners;

		public:
		class InputListener extends public MouseListener, public KeyListener, public JoystickListener
		{};

		/**
		 * Add a listener to be notified of input events
		 *
		 * @param listener The listener to be notified
		 */
		void addListener(InputListener* listener);

		/**
		 * Remove a listener that will no longer be notified
		 *
		 * @param listener The listen to be removed
		 */
		void removeListener(InputListener* listener);

		/**
		 * Remove all the listeners from this input manager
		 */
		void removeAllListeners();
	};
}

#endif /* FGEAL_EXTRA_INPUT_MANAGER_HPP_ */
