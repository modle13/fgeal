/*
 * sprite.cpp
 *
 *  Created on: 16/11/2016
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2016  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "sprite.hpp"

#include "fgeal/timing.hpp"
#include "fgeal/exceptions.hpp"

#include <cmath>

namespace fgeal
{
	/// throw exception if image is null
	inline static Image* checkImageAndReturn(Image* image)
	{
		if(image == null)
			throw AdapterException("Null image passed to Sprite constructor");
		else
			return image;
	}

	inline static unsigned computeRawRowCount(Image* image, unsigned height, int rawOffsetY)
	{
		if(image == null)
			throw AdapterException("Null image passed to Sprite constructor");
		else if(height == 0)
			return 0;
		else
			return (image->getHeight() - rawOffsetY) / height;
	}

	inline static unsigned computeRawColumnCount(Image* image, unsigned width, int rawOffsetX)
	{
		if(image == null)
			throw AdapterException("Null image passed to Sprite constructor");
		else if(width == 0)
			return 0;
		else
			return (image->getWidth() - rawOffsetX) / width;
	}

	inline static unsigned computeNumberOfRawFrames(Image* image, unsigned width, unsigned height, int rawOffsetX, int rawOffsetY)
	{
		return computeRawRowCount(image, height, rawOffsetY) * computeRawColumnCount(image, width, rawOffsetX);
	}

	Sprite::Sprite(Image* image, unsigned width, unsigned height, float intervalDuration, int rawFramesToLoad, int rawOffsetX, int rawOffsetY, bool deleteImageOnDeletion)
	: image(checkImageAndReturn(image)), width(width), height(height),
	  rawOffsetX(rawOffsetX), rawOffsetY(rawOffsetY),
	  numberOfFrames(rawFramesToLoad > 0 ? rawFramesToLoad : computeNumberOfRawFrames(image, width, height, rawOffsetX, rawOffsetY)),
	  referencePixelX(0), referencePixelY(0), frameSequence(), currentFrameSequenceIndex(0),
	  flipmode(Image::FLIP_NONE), scale(Image::NATURAL_SCALE), angle(0),
	  duration(intervalDuration), framePhaseOffset(0),
	  imageIsOwned(deleteImageOnDeletion),
	  rawRowCount(computeRawRowCount(image, height, rawOffsetY)),
	  rawColumnsCount(computeRawColumnCount(image, width, rawOffsetX))
	{
		for(unsigned i = 0; i < this->numberOfFrames; i++)
			this->frameSequence.push_back(i);
	}

	Sprite::Sprite(const Sprite& sprite, bool deleteImageOnDeletion)
	: image(sprite.image), width(sprite.width), height(sprite.height),
	  rawOffsetX(sprite.rawOffsetX), rawOffsetY(sprite.rawOffsetY),
	  numberOfFrames(sprite.numberOfFrames),
	  referencePixelX(sprite.referencePixelX), referencePixelY(sprite.referencePixelY),
	  frameSequence(sprite.frameSequence), currentFrameSequenceIndex(sprite.currentFrameSequenceIndex),
	  flipmode(sprite.flipmode), scale(sprite.scale), angle(sprite.angle),
	  duration(sprite.duration), framePhaseOffset(sprite.framePhaseOffset),
	  imageIsOwned(deleteImageOnDeletion),
	  rawRowCount(sprite.rawRowCount),
	  rawColumnsCount(sprite.rawColumnsCount)
	{}

	Sprite::~Sprite()
	{
		if(this->imageIsOwned)
			delete this->image;
	}

	Rectangle Sprite::getFrame(unsigned rawIndex)
	{
		if(numberOfFrames == 0) return Rectangle();
		if(rawIndex > numberOfFrames - 1)
			throw AdapterException("Frame index %d is out of bounds! Max raw frame index: %d", rawIndex, numberOfFrames);

		const Rectangle frameBounds = {
			(float) rawOffsetX + (rawIndex % rawColumnsCount) * width,
			(float) rawOffsetY + (rawIndex / rawColumnsCount) * height,
			(float) width, (float) height
		};
		return frameBounds;
	}

	// fixme the sprite can't be paused or reset natively. Manipulating 'framePhaseOffset' is neccessary for that.
	void Sprite::computeCurrentFrame()
	{
		if(frameSequence.empty() or frameSequence.size() == 1 or duration <= 0 or numberOfFrames == 0)
			return;
		else
		{
			static double dummy;
			currentFrameSequenceIndex = (unsigned)(modf((framePhaseOffset + fgeal::uptime())/duration, &dummy) * (float) frameSequence.size());
		}
	}

	Rectangle Sprite::getCurrentFrame()
	{
		return getFrame(frameSequence.at(currentFrameSequenceIndex));
	}

	void Sprite::draw(float x, float y)
	{
		const Rectangle frame = getCurrentFrame();
		const Point center = {frame.w/2, frame.h/2},
					where = {x - referencePixelX + (angle!=0?center.x:0), y - referencePixelY + (angle!=0?center.y:0)};

		if(angle == 0)
			if(scale.x == 1.0 and scale.y == 1.0)
				if(flipmode == Image::FLIP_NONE)
					image->drawRegion(where, frame);
				else
					image->drawFlippedRegion(where, flipmode, frame);
			else
				image->drawScaledRegion(where, scale, flipmode, frame);
		else
			if(scale.x == 1.0 and scale.y == 1.0)
				image->drawRotatedRegion(where, angle, center, flipmode, frame);
			else
				image->drawScaledRotatedRegion(where, scale, angle, center, flipmode, frame);
	}
}
