/*
 * agnostic.cpp (originally core_agnostic.cpp)
 *
 *  Created on: 24/08/2014
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2014  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "version.h"

#include "core.hpp"
#include "exceptions.hpp"
#include "filesystem.hpp"
#include "timing.hpp"
#include "geometry.hpp"
#include "colors.hpp"

#include "display.hpp"
#include "image.hpp"
#include "event.hpp"
#include "sound.hpp"

#include <iostream>
#include <algorithm>

#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <cerrno>

#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>

#ifndef M_PI
	#define M_PI		3.14159265358979323846
#endif

#ifndef M_PI_2
	#define M_PI_2     	1.57079632679489661923
#endif

#ifdef _MSC_VER
	#include <direct.h>
	#ifndef getcwd
		#define getcwd _getcwd
	#endif
	#ifndef chdir
		#define chdir _chdir
	#endif
	#ifndef stat
		#define stat _stat
	#endif
#else
	#include <unistd.h>
#endif

// max path name size expected by getCurrentWorkingDirectory()
#ifndef FGEAL_MAXPATHLEN
	#define FGEAL_MAXPATHLEN 1024
#endif

using std::string;
using std::vector;
using std::pair;

namespace fgeal
{
	//******************* FILE-SCOPE MISC.
	static float random_decimal_between(float min, float max)
	{
		if (max == min)
			return min;

		if(max < min) //swap
		{
			float t = max;
			max = min;
			min = t;
		}

	    float f = (static_cast<float>(rand())) / RAND_MAX;
	    return min + f * (max - min);
	}

	//******************* STATICS AND GLOBAL VARIABLES/CONSTANTS

	// as defined in core.hpp
	const char *VERSION = FGEAL_VERSION_STR;
	bool fgeal::core::isInitialized = false;
	core::AbsentImplementationPolicy core::absentImplementationPolicy = core::THROW_EXCEPTION;

	// as defined in vector2d.hpp
	const Vector2D Vector2D::NULL_VECTOR = {0.0f, 0.0f};
	const Vector2D Vector2D::X_VERSOR = {1.0f, 0.0f};
	const Vector2D Vector2D::Y_VERSOR = {0.0f, 1.0f};

	// as defined in image.hpp
	const Rectangle Image::WHOLE_REGION = {0.0f, 0.0f, 0.0f, 0.0f};
	const Vector2D Image::NATURAL_SCALE = {1.0f, 1.0f};
	const Point Image::IMAGE_CENTER = {0.0f, 0.0f};

	//******************* CORE FUNCTIONS

	void initialize()
	{
		core::initialize();
		core::isInitialized = true;
		Joystick::configureAll();
	}

	void finalize()
	{
		if(Display::instance != null)
		{
			delete Display::instance;
			Display::instance = null;
		}
		if(EventQueue::instance != null)
		{
			delete EventQueue::instance;
			EventQueue::instance = null;
		}

		Joystick::releaseAll();
		core::finalize();
		core::isInitialized = false;
	}

	bool isInitialized()
	{
		return core::isInitialized;
	}

	void core::checkInit()
	{
		if(not fgeal::isInitialized())
			throw AdapterException("Fatal error: attempt to use fgeal library without initialization!");
	}

	void core::reportAbsentImplementation(const std::string& description)
	{
		switch(core::absentImplementationPolicy)
		{
			case IGNORE_SILENTLY:
				break;

			case IGNORE_BUT_PRINT_STDERR:
				std::cerr << "Error:" << description << std::endl;
				break;

			case THROW_EXCEPTION:
				throw NotImplementedException(description);
				break;

			default:
			case HALT_PROGRAM:
				std::cerr << "Error:" << description << std::endl;
				exit(EXIT_FAILURE);
				break;
		}
	}

	//******************* EXCEPTIONS

	#define fgeal_exception_variadic_constructor(fmt) \
		va_list args; \
		va_start(args, fmt); \
		char buffer[1024]; \
		vsprintf(buffer, fmt, args); \
		message = std::string(buffer); \
		va_end(args)

	Exception::Exception(const char* format, ...)
	: message()
	{
		fgeal_exception_variadic_constructor(format);
	}

	AdapterException::AdapterException(const char* format, ...)
	{
		fgeal_exception_variadic_constructor(format);
	}

	NotImplementedException::NotImplementedException(const char* format, ...)
	{
		fgeal_exception_variadic_constructor(format);
	}

	GameException::GameException(const char* format, ...)
	{
		fgeal_exception_variadic_constructor(format);
	}

	//******************* FILE SYSTEM

	vector<string> filesystem::getFilenamesWithinDirectory(const string& directoryPath)
	{
		FGEAL_CHECK_INIT();
		vector<string> filenames;
		DIR *dir;
		struct dirent *ent;
		if((dir = opendir (directoryPath.c_str())) != null)
		{
			while((ent = readdir (dir)) != null)
			{
				if(string(ent->d_name).compare("..") != 0 && string(ent->d_name).compare(".") != 0)  // exclude . and ..
					filenames.push_back(directoryPath+"/"+string(ent->d_name));
			}
			closedir (dir);
		}
		else throw AdapterException("Could not open directory \"%s\"", directoryPath.c_str());
		std::sort(filenames.begin(), filenames.end());
		return filenames;
	}

	string filesystem::getCurrentWorkingDirectory()
	{
		FGEAL_CHECK_INIT();
		static char buffer[FGEAL_MAXPATHLEN];
		errno = 0;
		if(getcwd(buffer, FGEAL_MAXPATHLEN) != null)
			return string(buffer);
		else
		{
			if(errno == EACCES)
				throw AdapterException("Could not fetch current working directory filename. Permission to read or search a component of the file name was denied.");
			else if(errno == ERANGE)
				throw AdapterException("Could not fetch current working directory filename. Name is too long. (>%d)", FGEAL_MAXPATHLEN);
			else if(errno == EINVAL)
				throw AdapterException("Could not fetch current working directory filename. Internal logic error.");
			else
				throw AdapterException("Could not fetch current working directory filename (errno: %d).", errno);
		}
	}

	void filesystem::setWorkingDirectory(const std::string& directoryPath)
	{
		FGEAL_CHECK_INIT();
		errno = 0;
		if(chdir(directoryPath.c_str()) != 0)
		{
			if(errno == ENOENT)
				throw AdapterException("Could not set working directory \"%s\". Path could not be found.", directoryPath.c_str());
			else if(errno == ENOTDIR)
				throw AdapterException("Could not set working directory \"%s\". Path is not a directory.", directoryPath.c_str());
			else
				throw AdapterException("Could not set working directory \"%s\". errno: %d", directoryPath.c_str(), errno);
		}
	}

	bool filesystem::isFilenameDirectory(const string& path)
	{
		FGEAL_CHECK_INIT();
		struct stat info;
		errno = 0;
		if(stat(path.c_str(), &info) == 0)
			return info.st_mode & S_IFDIR;
		else if(errno == ENOENT)
			return false;
		else
			throw AdapterException("Could not fetch directory information. errno: %d", errno);
	}

	bool filesystem::isFilenameArchive(const string& path)
	{
		FGEAL_CHECK_INIT();
		struct stat info;
		errno = 0;
		if(stat(path.c_str(), &info) == 0)
			return info.st_mode & S_IFREG;
		else if(errno == ENOENT)
			return false;
		else
			throw AdapterException("Could not fetch directory information. errno: %d", errno);
	}

	//******************* VECTOR2D

	Vector2D Vector2D::random(float magnitude)
	{
		const float angle = random_decimal_between(0.0, 2.0*M_PI);
		Vector2D v = {magnitude*static_cast<float>(cos(angle)), magnitude*static_cast<float>(sin(angle))};
		return v;
	}

	bool Vector2D::operator ==(const Vector2D& v) const
	{
		return v.x == x and v.y == y;
	}

	bool Vector2D::operator !=(const Vector2D& v) const
	{
		return v.x != x or v.y != y;
	}

	/** Returns true if this vector coordinates equals the ones from the given vector */
	bool Vector2D::equals(const Vector2D& vector) const
	{
		return (*this) == vector;
	}

	/** Creates and returns a copy of this vector via copy constructor */
	Vector2D Vector2D::clone() const
	{
		return Vector2D(*this);
	}

	// ------- utils

	/** Creates a string with this vector coordinates (x, y) */
	string Vector2D::toString() const
	{
		static char buffer[32];
		string str("(");
		sprintf(buffer, "%f", x);
		str.append(buffer).append(", ");
		sprintf(buffer, "%f", y);
		str.append(buffer).append(")");
		return str;
	}

	/** Creates and returns an array with this Vectors coordinates, in correct order.  */
	float* Vector2D::getCoordinates() const
	{
		float* coord = new float[2];
		coord[0] = x;
		coord[1] = y;
		return coord;
	}

	// ------- magnitude/length

	float Vector2D::operator ~() const
	{
		return sqrt( x*x + y*y );
	}

	/** Returns the length/magnitude of this vector. */
	float Vector2D::magnitude() const
	{
		return ~(*this);
	}

	// ------- normalization

	Vector2D Vector2D::operator !() const
	{
		const Vector2D v = {this->isZero()? 0 : x/magnitude(), this->isZero()? 0 : y/magnitude()};
		return v;
	}

	/** Creates a vector with length 1 and same direction as this vector. In other words, a new vector that is a normalized version of this vector. Note that <b>the original vector remains unchanged</b>. */
	Vector2D Vector2D::unit() const
	{
		return !(*this);
	}

	/** Divides this vector's coordinates by its length/magnitude, normalizing it.
	The returned object is the vector instance itself after normalization. */
	Vector2D& Vector2D::normalize()
	{
		if(not isZero())
		{
			static float length = magnitude();
			if(length > 0)
			{
				x /= length;
				y /= length;
			}
		}
		return *this;
	}

	// ------- reflection

	Vector2D Vector2D::operator -() const
	{
		const Vector2D v = {-x, -y};
		return v;
	}

	/** Creates and returns the opposite of this vector. In other words, returns a vector with same coordinates as this, but with changed signal. Note that <b>the original vector remains unchanged</b>. */
	Vector2D Vector2D::opposite() const
	{
		return -(*this);
	}

	Vector2D& Vector2D::operator --()
	{
		x = -x; y = -y;
		return *this;
	}

	/** Changes the signal of this vector coordinates, effectively reflecting it.
	<br> The returned object is <b>the vector instance itself</b> after reflection. */
	Vector2D& Vector2D::reflect()
	{
		return --(*this);
	}

	Vector2D& Vector2D::reflectX()
	{
		x = -x;
		return *this;
	}

	Vector2D& Vector2D::reflectY()
	{
		y = -y;
		return *this;
	}

	// ------- basic arithmetic

	Vector2D Vector2D::operator +(const Vector2D& v) const
	{
		const Vector2D result = {x + v.x, y + v.y};
		return result;
	}

	/** Creates and returns a vector that represents the sum of this vector and the given vector. Note that <b>the original vector remains unchanged</b>.*/
	Vector2D Vector2D::sum(const Vector2D& v) const
	{
		return *this + v;
	}

	Vector2D& Vector2D::operator +=(const Vector2D& v)
	{
		x += v.x;
		y += v.y;
		return *this;
	}

	/** Adds to this vector the given vector. In other words, it performs an addition to this vector coordinates.
	<br> The returned object is <b>the vector instance itself</b> after summation. */
	Vector2D& Vector2D::add(const Vector2D& v)
	{
		(*this) += v;
		return *this;
	}

	Vector2D Vector2D::operator -(const Vector2D& v) const
	{
		const Vector2D result = {x - v.x, y - v.y};
		return result;
	}

	/** Creates a vector that represents the difference/displacement of this vector and the given vector, in this order. It's useful to remember that vector subtraction is <b>not commutative</b>: a-b != b-a.
	<br> Note that <b>the original vector remains unchanged</b>. */
	Vector2D Vector2D::difference(const Vector2D& v) const
	{
		return (*this) - v;
	}

	Vector2D& Vector2D::operator -=(const Vector2D& v)
	{
		x -= v.x;
		y -= v.y;
		return *this;
	}

	/** Subtracts from this vector the given vector. In other words, it performs an subtraction to this vector coordinates.
	It's useful to remember that vector subtraction is <b>not commutative</b>: a-b != b-a.
	<br> The returned object is the <b>the vector instance itself</b> after subtraction. */
	Vector2D& Vector2D::subtract(const Vector2D& v)
	{
		(*this) -= v;
		return *this;
	}

	Vector2D Vector2D::operator *(const float& factor) const
	{
		const Vector2D result = {x * factor, y * factor};
		return result;
	}

	/** Creates a vector that represents the scalar multiplication of this vector by the given factor. Note that <b>the original vector remains unchanged</b>.*/
	Vector2D Vector2D::times(const float factor) const
	{
		return (*this) * factor;
	}

	Vector2D& Vector2D::operator *=(const float& factor)
	{
		x *= factor;
		y *= factor;
		return *this;
	}

	/** Multiply this vectors coordinates by the given factor. The returned object is <b>the vector instance itself</b> after multiplication.*/
	Vector2D& Vector2D::scale(float factor)
	{
		(*this) *= factor;
		return *this;
	}

	// ------- miscellaneous operations

	float Vector2D::operator %(const Vector2D& v) const
	{
		return ~((*this) - v);
	}

	/** Compute the distance between this vector and the given vector. In other words, returns the length/magnitude of the displacement between this and the given vector. */
	float Vector2D::distance(const Vector2D& v) const
	{
		return (*this)%v;
	}

	float Vector2D::operator ^(const Vector2D& v) const
	{
		return x*v.x + y*v.y;
	}

	/** Compute the inner/dot product between this and the given vector. */
	float Vector2D::innerProduct(const Vector2D& v) const
	{
		return (*this) ^ v;
	}

	// ------- projection operations (XXX not tested)

	Vector2D Vector2D::operator ||(const Vector2D& v) const  // FIXME projection operator is incorrect, not passing tests
	{
		return (*this) * (((*this)^v)/(v^v));
	}

	/* Creates a vector that represents the projection of this vector on the given vector v. */
	Vector2D Vector2D::projection(const Vector2D& v) const
	{
		return (*this) || v;
	}

	/* Creates a vector that represents the rejection of this vector on the given vector v. The rejection is defined as rej(u, v) = u - proj(u, v) */
	Vector2D Vector2D::rejection(const Vector2D& v) const
	{
		return (*this) - ((*this) || v);
	}

	Vector2D Vector2D::operator |(const Vector2D& v) const
	{
		return (*this) - (this->rejection(v)*2);
	}

	/* Creates a vector that represents the reflection of this vector in the axis represented by the given vector v. */
	Vector2D Vector2D::reflection(const Vector2D& v) const
	{
		return (*this)|v;
	}

	// ------- rotation operations

	Vector2D Vector2D::operator <(const float& radians) const
	{
		const Vector2D result = { x*static_cast<float>(cos(radians)) - y*static_cast<float>(sin(radians)) , x*static_cast<float>(sin(radians)) + y*static_cast<float>(cos(radians)) };
		return result;
	}

	Vector2D Vector2D::rotation(const float& radians) const
	{
		return (*this) < radians;
	}

	Vector2D& Vector2D::operator <<(const float& radians)
	{
		float
		nx = x*cos(radians) - y*sin(radians),
		ny = x*sin(radians) + y*cos(radians);
		x = nx; y = ny;
		return *this;
	}

	Vector2D& Vector2D::rotate(const float& radians)
	{
		return (*this) << radians;
	}

	Vector2D Vector2D::perpendicular() const
	{
		return (*this) < M_PI_2; // rotate by pi/2 radians
	}

	//******************* SINGLETONS

	Display* Display::instance = null;
	Display& Display::getInstance()
	{
		return *Display::instance;
	}

	Display& Display::create(unsigned width, unsigned height, bool isFullscreen, const string& title, const string& iconFilename)
	{
		Options options;
		options.width = width;
		options.height = height;
		options.fullscreen = isFullscreen;
		options.title = title;
		options.iconFilename = iconFilename;
		return Display::create(options);
	}

	Display& Display::create(const Display::Options& options)
	{
		// this check should NOT be ommited for performance reasons, as this funtion is usually called once.
		if(Display::instance != null)
			throw AdapterException("Attempt to create a Display object twice (currently not supported).");

		Display::instance = new Display(options);

		return *Display::instance;
	}

	bool Display::wasCreated()
	{
		return (Display::instance != null);
	}

	EventQueue* EventQueue::instance = null;
	EventQueue& EventQueue::getInstance()
	{
		if(EventQueue::instance == null)
			EventQueue::instance = new EventQueue();

		return *EventQueue::instance;
	}

	//******************* DISPLAY

	#define aspectRatio(x, y) std::make_pair(x, y)

	Display::Resolution::Resolution(unsigned width, unsigned height, std::pair<unsigned, unsigned> aspect, std::string description)
	: width(width), height(height), aspect(aspect), description(description)
	{}

	vector<Display::Resolution> Display::Resolution::getList(pair<unsigned, unsigned> requestedAspect)
	{
		const pair<unsigned, unsigned> undecidedAspect = std::make_pair(0, 0);
		pair<unsigned, unsigned> aspect;

		vector<Resolution> resolutions;

		aspect = aspectRatio(4, 3);
		if(aspect == requestedAspect or requestedAspect == undecidedAspect)
		{
			resolutions.push_back(Resolution( 320,  240, aspect, "QVGA"));
			resolutions.push_back(Resolution( 400,  300, aspect, "qSVGA"));
			resolutions.push_back(Resolution( 480,  360, aspect, "HVGA"));
			resolutions.push_back(Resolution( 512,  384, aspect, "Macintosh Color Classic"));
			resolutions.push_back(Resolution( 640,  480, aspect, "VGA / SD"));
			resolutions.push_back(Resolution( 800,  600, aspect, "SVGA / UVGA"));
			resolutions.push_back(Resolution( 832,  624, aspect, "SVGA, Macintosh LC III"));
			resolutions.push_back(Resolution( 960,  720, aspect, "720p DVCPRO HD"));
			resolutions.push_back(Resolution(1024,  768, aspect, "XGA"));
			resolutions.push_back(Resolution(1152,  864, aspect, "XGA+"));
			resolutions.push_back(Resolution(1200,  900, aspect, ""));
			resolutions.push_back(Resolution(1280,  960, aspect, "SXGA- / UVGA"));
			resolutions.push_back(Resolution(1360, 1020, aspect, ""));
			resolutions.push_back(Resolution(1400, 1050, aspect, "SXGA+"));
			resolutions.push_back(Resolution(1440, 1080, aspect, "HDV 1080i"));
			resolutions.push_back(Resolution(1600, 1200, aspect, "UXGA"));
			resolutions.push_back(Resolution(1920, 1440, aspect, ""));
			resolutions.push_back(Resolution(2048, 1536, aspect, "QXGA"));
			resolutions.push_back(Resolution(2560, 1920, aspect, "max. CRT"));
			resolutions.push_back(Resolution(2800, 2100, aspect, "QSXGA+"));
			resolutions.push_back(Resolution(3200, 2400, aspect, "QUXGA"));
			resolutions.push_back(Resolution(4096, 3072, aspect, "HXGA"));
			resolutions.push_back(Resolution(6400, 4800, aspect, "HUXGA"));
		}

		aspect = aspectRatio(16, 9);
		if(aspect == requestedAspect or requestedAspect == undecidedAspect)
		{
			resolutions.push_back(Resolution( 640,  360, aspect, "nHD"));
			resolutions.push_back(Resolution( 480,  270, aspect, "HVGA"));
			resolutions.push_back(Resolution( 854,  480, aspect, "FWVGA"));
			resolutions.push_back(Resolution( 960,  540, aspect, "qHD"));
			resolutions.push_back(Resolution(1024,  576, aspect, "WSVGA"));
			resolutions.push_back(Resolution(1280,  720, aspect, "HD / 720p / WXGA-H"));
			resolutions.push_back(Resolution(1360,  768, aspect, "WXGA*"));
			resolutions.push_back(Resolution(1366,  768, aspect, "WXGA"));
			resolutions.push_back(Resolution(1600,  900, aspect, "HD+ / 900p"));
			resolutions.push_back(Resolution(1920, 1080, aspect, "Full HD / 1080p"));
			resolutions.push_back(Resolution(2048, 1152, aspect, "QWXGA"));
			resolutions.push_back(Resolution(2560, 1440, aspect, "QHD / WQHD / 1440p"));
			resolutions.push_back(Resolution(3200, 1800, aspect, "QHD+ / WQXGA+"));
			resolutions.push_back(Resolution(3840, 2160, aspect, "4K UHD"));
			resolutions.push_back(Resolution(5120, 2880, aspect, "5K/UHD+"));
			resolutions.push_back(Resolution(7680, 4320, aspect, "8K UHD"));
		}

		aspect = aspectRatio(16, 10);
		if(aspect == requestedAspect or requestedAspect == undecidedAspect)
		{
			resolutions.push_back(Resolution( 320,  200, aspect, "CGA"));
			resolutions.push_back(Resolution( 384,  240, aspect, "WQVGA"));
			resolutions.push_back(Resolution( 768,  480, aspect, "WVGA"));
			resolutions.push_back(Resolution(1280,  800, aspect, "WXGA"));
			resolutions.push_back(Resolution(1440,  900, aspect, "WXGA+ / WSXGA"));
			resolutions.push_back(Resolution(1680, 1050, aspect, "WSXGA+"));
			resolutions.push_back(Resolution(1920, 1200, aspect, "WUXGA"));
			resolutions.push_back(Resolution(2560, 1600, aspect, "WQXGA"));
			resolutions.push_back(Resolution(3840, 2400, aspect, "WQUXGA"));
			resolutions.push_back(Resolution(5120, 3200, aspect, "WHXGA"));
			resolutions.push_back(Resolution(7680, 4800, aspect, "WHUXGA"));
		}

		aspect = aspectRatio(3, 2);
		if(aspect == requestedAspect or requestedAspect == undecidedAspect)
		{
			resolutions.push_back(Resolution(360, 240, aspect, "WQVGA"));
			resolutions.push_back(Resolution(480, 320, aspect, "HVGA"));
			resolutions.push_back(Resolution(720, 480, aspect, "WVGA"));
			resolutions.push_back(Resolution(960, 640, aspect, "DVGA"));
			resolutions.push_back(Resolution(2160, 1440, aspect, "Full HD+"));
		}

		aspect = aspectRatio(5, 3);
		if(aspect == requestedAspect or requestedAspect == undecidedAspect)
		{
			resolutions.push_back(Resolution(400, 240, aspect, "WQVGA"));
			resolutions.push_back(Resolution(800, 480, aspect, "WVGA"));
			resolutions.push_back(Resolution(1280, 768, aspect, "WXGA"));
		}

		aspect = aspectRatio(5, 4);
		if(aspect == requestedAspect or requestedAspect == undecidedAspect)
		{
			resolutions.push_back(Resolution(1280, 1024, aspectRatio( 5,  4), "SXGA"));
			resolutions.push_back(Resolution(2560, 2048, aspectRatio( 5,  4), "QSXGA"));
			resolutions.push_back(Resolution(5120, 4096, aspectRatio( 5,  4), "HSXGA"));
		}

		if(requestedAspect == undecidedAspect)
		{
			resolutions.push_back(Resolution( 432,  240, aspectRatio( 9,  5), "WQVGA"));
			resolutions.push_back(Resolution( 640,  350, aspectRatio(64, 35), "EGA"));
			resolutions.push_back(Resolution( 720,  348, aspectRatio(60, 29), "HGC"));
			resolutions.push_back(Resolution(1600,  768, aspectRatio(25, 12), "UWXGA"));
			resolutions.push_back(Resolution(1920, 1400, aspectRatio( 7,  5), "TXGA"));
			resolutions.push_back(Resolution(2048, 1080, aspectRatio(19, 10), "DCI 2K"));
			resolutions.push_back(Resolution(2560, 1080, aspectRatio(64, 27), "UW-UXGA"));
			resolutions.push_back(Resolution(3200, 2048, aspectRatio(25, 16), "WQSXGA"));
			resolutions.push_back(Resolution(3440, 1440, aspectRatio(43, 18), "UWQHD"));
			resolutions.push_back(Resolution(3840, 1600, aspectRatio(12,  5), "UW4K"));
			resolutions.push_back(Resolution(4096, 2160, aspectRatio(19, 10), "DCI 4K"));
			resolutions.push_back(Resolution(5120, 2160, aspectRatio(64, 27), "UW5K"));
			resolutions.push_back(Resolution(6400, 4096, aspectRatio(25, 16), "WHSXGA"));
			resolutions.push_back(Resolution(10240, 4320, aspectRatio(21, 9), "UW10K"));
		}

		return resolutions;
	}

	vector< pair<unsigned, unsigned> > Display::Resolution::getAspectList()
	{
		vector< pair<unsigned, unsigned> > aspects;
		aspects.push_back(aspectRatio(4, 3));
		aspects.push_back(aspectRatio(16, 9));
		aspects.push_back(aspectRatio(16, 10));
		aspects.push_back(aspectRatio(3, 2));
		aspects.push_back(aspectRatio(5, 3));
		aspects.push_back(aspectRatio(5, 4));
		return aspects;
	}


	//******************* EVENT

	Point Event::getEventMousePosition()
	{
		const Point pos = { (float) this->getEventMouseX(), (float) this->getEventMouseY() };
		return pos;
	}

	Event::implementation& EventQueue::getEventImplementation(Event& event)  // for internal use
	{
		return event.self;
	}

	//******************* IMAGE

	void Image::draw(const Point& position)
	{
		this->draw(position.x, position.y);
	}

	void Image::drawRegion(const Point& position, const Rectangle& region)
	{
		if(&region == &Image::WHOLE_REGION)
			this->draw(position.x, position.y);
		else
			this->drawRegion(position.x, position.y, region.x, region.y, region.w, region.h);
	}

	void Image::drawFlippedRegion(const Point& position, const FlipMode flipmode, const Rectangle& region)
	{
		if(&region == &Image::WHOLE_REGION)
			this->drawFlipped(position.x, position.y, flipmode);
		else
			this->drawFlippedRegion(position.x, position.y, flipmode, region.x, region.y, region.w, region.h);
	}

	void Image::drawScaledRegion(const Point& position, const Vector2D& scale, const FlipMode flipmode, const Rectangle& region)
	{
		if(&region == &Image::WHOLE_REGION)
			this->drawScaled(position.x, position.y, scale.x, scale.y, flipmode);
		else
			this->drawScaledRegion(position.x, position.y, scale.x, scale.y, flipmode, region.x, region.y, region.w, region.h);
	}

	void Image::drawRotatedRegion(const Point& position, float angle, const Point& center, const FlipMode flipmode, const Rectangle& region)
	{
		const bool customCenter = (&center != &Image::IMAGE_CENTER);

		if(&region == &Image::WHOLE_REGION)
			this->drawRotated(position.x, position.y, angle, (customCenter? center.x : this->getWidth()/2.0), (customCenter? center.y : this->getHeight()/2.0), flipmode);
		else
			this->drawRotatedRegion(position.x, position.y, angle, (customCenter? center.x : this->getWidth()/2.0), (customCenter? center.y : this->getHeight()/2.0), flipmode, region.x, region.y, region.w, region.h);
	}

	void Image::drawScaledRotatedRegion(const Point& position, const Vector2D& scale, float angle, const Point& center, const FlipMode flipmode, const Rectangle& region)
	{
		const bool customCenter = (&center != &Image::IMAGE_CENTER);

		if(&region == &Image::WHOLE_REGION)
			this->drawScaledRotated(position.x, position.y, scale.x, scale.y, angle, (customCenter? center.x : this->getWidth()/2.0), (customCenter? center.y : this->getHeight()/2.0), flipmode);
		else
			this->drawScaledRotatedRegion(position.x, position.y, scale.x, scale.y, angle, (customCenter? center.x : this->getWidth()/2.0), (customCenter? center.y : this->getHeight()/2.0), flipmode, region.x, region.y, region.w, region.h);
	}

	void Image::blit(Image& img2, const Point& position, const Vector2D& scale, float angle, const Point& centerOfRotation, const FlipMode flipmode, const Rectangle& region)
	{
		const bool customCenter = (&centerOfRotation != &Image::IMAGE_CENTER),
				   customRegion = (&region != &Image::WHOLE_REGION);

		this->blit(img2, position.x, position.y, scale.x, scale.y, angle,
				(customCenter? centerOfRotation.x : 0.5*getWidth()),
				(customCenter? centerOfRotation.y : 0.5*getHeight()),
				flipmode,
				(customRegion? region.x : 0),
				(customRegion? region.y : 0),
				(customRegion? region.w : this->getWidth()),
				(customRegion? region.h : this->getHeight()));
	}

	Image* Image::getCopy(const Vector2D& scaledBy, float rotatedBy, const FlipMode flipped, const Rectangle& croppedBy)
	{
		return this->getCopy(scaledBy.x, scaledBy.y, rotatedBy, flipped, croppedBy.x, croppedBy.y, croppedBy.w, croppedBy.h);
	}

	Image* Image::getCopy(float xScaledBy, float yScaledBy, float rotatedBy, const FlipMode flipped, float cropFromX, float cropFromY, float cropFromWidth, float cropFromHeight)
	{
		FGEAL_CHECK_INIT();
		Image* copy = null;
		if(cropFromWidth != 0 and cropFromHeight != 0)
		{
			if(rotatedBy != 0)
			{
				int newWidth  = xScaledBy * cropFromWidth * cos(rotatedBy) + yScaledBy * cropFromHeight * sin(rotatedBy),
					newHeight = xScaledBy * cropFromWidth * sin(rotatedBy) + yScaledBy * cropFromHeight * cos(rotatedBy);

				copy = new Image(newWidth, newHeight);
			}
			else
			{
				copy = new Image(xScaledBy * cropFromWidth, yScaledBy * cropFromHeight);
			}
		}
		else
		{
			if(rotatedBy != 0)
			{
				int newWidth  = this->getWidth() * cos(rotatedBy) + this->getHeight() * sin(rotatedBy),
					newHeight = this->getWidth() * sin(rotatedBy) + this->getHeight() * cos(rotatedBy);

				copy = new Image(newWidth, newHeight);
			}
			else
			{
				copy = new Image(this->getWidth(), this->getHeight());
			}
		}

		this->blit(*copy, 0, 0, xScaledBy, yScaledBy, rotatedBy, 0.5*copy->getWidth(), 0.5*copy->getHeight(), flipped, cropFromX, cropFromY, cropFromWidth, cropFromHeight);
		return copy;
	}

	void Image::delegateDraw(float x, float y, float xScale, float yScale, float angle, float centerX, float centerY, const FlipMode flipmode, float fromX, float fromY, float fromWidth, float fromHeight)
	{
		if(fromWidth == 0 and fromHeight == 0)
			if(angle == 0)
				if(xScale == 1.0 and yScale == 1.0)
					this->drawFlipped(x, y, flipmode);
				else
					this->drawScaled(x, y, xScale, yScale, flipmode);
			else
				if(xScale == 1.0 and yScale == 1.0)
					this->drawRotated(x, y, angle, centerX, centerY, flipmode);
				else
					this->drawScaledRotated(x, y, xScale, yScale, angle, centerX, centerY, flipmode);
		else
			if(angle == 0)
				if(xScale == 1.0 and yScale == 1.0)
					this->drawFlippedRegion(x, y, flipmode, fromX, fromY, fromWidth, fromHeight);
				else
					this->drawScaledRegion(x, y, xScale, yScale, flipmode, fromX, fromY, fromWidth, fromHeight);
			else
				if(xScale == 1.0 and yScale == 1.0)
					this->drawRotatedRegion(x, y, angle, centerX, centerY, flipmode, fromX, fromY, fromWidth, fromHeight);
				else
					this->drawScaledRotatedRegion(x, y, xScale, yScale, angle, centerX, centerY, flipmode, fromX, fromY, fromWidth, fromHeight);
	}

	//******************* SOUND

	void Sound::halt(bool rewind)
	{
		if(rewind)
			this->stop();
		else
			this->pause();
	}

	void SoundStream::halt(bool rewind)
	{
		if(rewind)
			this->stop();
		else
			this->pause();
	}

	void Music::halt(bool rewind)
	{
		if(rewind)
			this->stop();
		else
			this->pause();
	}

	void Sound::setPitch(float factor, bool hintDynamicChange)
	{
		this->setPlaybackSpeed(factor, hintDynamicChange);
	}

	float Sound::getPitch()
	{
		return this->getPlaybackSpeed();
	}

	//******************* COLOR

	Color::Color(unsigned char r, unsigned char g, unsigned char b, unsigned char a)
	: r(r), g(g), b(b), a(a)
	{}

	Color Color::fromFloat(float r, float g, float b, float a)
	{
		return Color(255*r, 255*g, 255*b, 255*a);
	}

	bool Color::operator == (const Color& color) const
	{
		return this->r == color.r
		   and this->g == color.g
		   and this->b == color.b
		   and this->a == color.a;
	}

	bool Color::operator != (const Color& color) const
	{
		return this->r != color.r
			or this->g != color.g
			or this->b != color.b
			or this->a != color.a;
	}

	/** Creates a color object with the given hex triplet (0x000000-0xFFFFFF range). */
	Color Color::fromHex(unsigned long hex, unsigned char a)
	{
		return Color((hex & 0xFF0000) >> 16, (hex & 0xFF00) >> 8, (hex & 0xFF), a);
	}

	/** Creates a color object with the given hex triplet (0x000000-0xFFFFFF range). */
	Color Color::fromHex(unsigned long hex, float alpha)
	{
		return Color((hex & 0xFF0000) >> 16, (hex & 0xFF00) >> 8, (hex & 0xFF), 255*alpha);
	}

	/** Returns a hex triplet (0x000000-0xFFFFFF range) representation of this color. */
	unsigned long Color::toHex()
	{
		return ((r & 0xff) << 16) + ((g & 0xff) << 8) + (b & 0xff);
	}

	/** Creates a color object with the given ARGB hex color (0x00000000-0xFFFFFFFF range). */
	Color Color::fromArgbHex(unsigned long hex)
	{
		return Color((hex & 0xFF0000) >> 16, (hex & 0xFF00) >> 8, (hex & 0xFF), (hex & 0xFF000000) >> 24);
	}

	/** Returns a ARGB hex code (0x00000000-0xFFFFFFFF range) representation of this color. */
	unsigned long Color::toArgbHex()
	{
		return ((a & 0xff) << 24) + ((r & 0xff) << 16) + ((g & 0xff) << 8) + (b & 0xff);
	}

	/** Creates a color object with the given RGBA hex color (0x00000000-0xFFFFFFFF range). */
	Color Color::fromRgbaHex(unsigned long hex)
	{
		return Color((hex & 0xFF000000) >> 24, (hex & 0xFF0000) >> 16, (hex & 0xFF00) >> 8, (hex & 0xFF));
	}

	/** Returns a RGBA hex code (0x00000000-0xFFFFFFFF range) representation of this color. */
	unsigned long Color::toRgbaHex()
	{
		return ((r & 0xff) << 24) + ((g & 0xff) << 16) + ((b & 0xff) << 8) + (a & 0xff);
	}

	// aux. function used by Color::parseCStr()
	static vector<string> split(const string& str, const string& delimiter)
	{
		vector<string> tokens;
		if(delimiter.size() == 0)
			tokens.push_back(str);
		else
		{
			size_t start = 0, end = 0;
			while(end != string::npos)
			{
				end = str.find(delimiter, start);
				tokens.push_back( str.substr( start, (end == string::npos) ? string::npos : end - start));
				start = ( (end > (string::npos - delimiter.size()))? string::npos : end + delimiter.size());
			}
		}
		return tokens;
	}

	// aux. function used by Color::parseCStr()
	static long parseHex(const char* str)
	{
		char* end;
		errno = 0;
		const long x = strtol(str, &end, 16);
		if(errno == ERANGE) throw std::out_of_range(str);
		if(*str == '\0' or *end != '\0') throw std::invalid_argument(str);
		return x;
	}

	// aux. function used by Color::parseCStr()
	static double parseDouble(const char* str)
	{
		char* end;
		errno = 0;
		const double x = strtod(str, &end);
		if(errno == ERANGE) throw std::out_of_range(str);
		if(*str == '\0' or *end != '\0') throw std::invalid_argument(str);
		return x;
	}

	// aux. function used by Color::parseCStr()
	static string trim(string str)
	{
		str.erase(str.find_last_not_of(" \t\n\r\f\v") + 1); //trailing
		str.erase(0, str.find_first_not_of(" \t\n\r\f\v")); //leading
		return str;
	}

	Color Color::parseCStr(const char* cstr, bool alphaFirst)
	{
		const vector<string> tokens = split(cstr, ",");
		if(tokens.size() == 4)
			return Color(parseDouble(trim(tokens[alphaFirst? 1 : 0]).c_str()),
						 parseDouble(trim(tokens[alphaFirst? 2 : 1]).c_str()),
						 parseDouble(trim(tokens[alphaFirst? 3 : 2]).c_str()),
						 parseDouble(trim(tokens[alphaFirst? 0 : 3]).c_str()));

		else if(tokens.size() == 3)
			return Color(parseDouble(trim(tokens[0]).c_str()),
						 parseDouble(trim(tokens[1]).c_str()),
						 parseDouble(trim(tokens[2]).c_str()));

		else if(tokens.size() == 1)
		{
			string str = trim(tokens[0]);
			if(str.empty()) return Color();
			if(str[0] == '#') str = str.substr(1);
			long value = parseHex(str.c_str());
			return alphaFirst? fromArgbHex(value) : fromRgbaHex(value);
		}

		else throw std::invalid_argument(cstr);
	}

	const Color
		Color::_TRANSPARENT (  0,   0,   0,   0),
		Color::BLACK		(  0,   0,   0),
		Color::GREY			(127, 127, 127),
		Color::WHITE		(255, 255, 255),

		// Basic colors (color wheel)
		Color::RED			(255,   0,   0),
		Color::MAROON		(127,   0,   0),
		Color::GREEN		(  0, 255,   0),
		Color::DARK_GREEN	(  0, 127,   0),
		Color::BLUE			(  0,   0, 255),
		Color::NAVY			(  0,   0, 127),
		Color::YELLOW		(255, 255,   0),
		Color::OLIVE		(127, 127,   0),
		Color::CYAN			(  0, 255, 255),
		Color::TEAL			(  0, 127, 127),
		Color::MAGENTA		(255,   0, 255),
		Color::PURPLE		(127,   0, 127),

		Color::ORANGE		(255, 127,   0),
		Color::CHARTREUSE	(127, 255,   0),
		Color::SPRING_GREEN	(0  , 255, 127),
		Color::AZURE		(0  , 127, 255),
		Color::ROSE			(255,   0, 127),
		Color::VIOLET		(127,   0, 255),

		// Basic shades of grey
		Color::LIGHT_GREY	(192, 192, 192),
		Color::DARK_GREY	( 96,  96,  96),

		// Other common colors
		Color::PINK			(255, 192, 192),
		Color::BROWN		(144,  92,  48);

	Color::Color(ColorName name)
	: r(), g(), b(), a()
	{
		switch(name)
		{
			case VERMILION: *this = Color(255,  69,   0); return;
			case AMBER:     *this = Color(255, 192,   0); return;
			case GOLD:      *this = Color(255, 215,   0); return;
			case FUSCHIA:   *this = Color(MAGENTA);       return;
			case INDIGO:    *this = Color( 75,   0, 130); return;
			case BEIGE:     *this = Color(245, 245, 220); return;
			case LIME:      *this = Color(192, 255,   0); return;
			case SCARLET:   *this = Color(253,  14,  53); return;
			case MINT:      *this = Color(116, 195, 101); return;
			case TURQUOISE: *this = Color( 64, 224, 208); return;
			case SALMON:    *this = Color(250, 127, 114); return;
			case BRONZE:    *this = Color(205, 127,  50); return;
			case WINE:      *this = Color(196,  30,  58); return;
			case CELESTE:   *this = Color(  0, 191, 255); return;
			case FLAME:     *this = Color(226,  88,  34); return;
			case CREAM:     *this = Color(253, 252, 143); return;
			case CARAMEL:   *this = Color(193, 154, 107); return;
			case RUBY:      *this = Color(255, 255, 255); return;
			case JADE:      *this = Color(224,  17,  95); return;
			case CERULEAN:  *this = Color(  0, 123, 167); return;
			case AQUA:      *this = Color(CYAN);          return;

			// X11/W3C colors
			case X11_ALICE_BLUE:          *this = Color(240, 248, 255); return;
			case X11_ANTIQUE_WHITE:       *this = Color(250, 235, 215); return;
			case X11_AQUA:                *this = Color(X11_CYAN); return;
			case X11_AQUAMARINE:          *this = Color(127, 255, 212); return;
			case X11_AZURE:               *this = Color(240, 255, 255); return;
			case X11_BEIGE:               *this = Color(245, 245, 220); return;
			case X11_BISQUE:              *this = Color(255, 228, 196); return;
			case X11_BLACK:               *this = Color(BLACK); return;
			case X11_BLANCHED_ALMOND:     *this = Color(255, 235, 205); return;
			case X11_BLUE:                *this = Color(BLUE); return;
			case X11_BLUE_VIOLET:         *this = Color(138,  43, 226); return;
			case X11_BROWN:               *this = Color(165,  42,  42); return;
			case X11_BURLYWOOD:           *this = Color(222, 184, 135); return;
			case X11_CADET_BLUE:          *this = Color( 95, 158, 160); return;
			case X11_CHARTREUSE:          *this = Color(CHARTREUSE); return;
			case X11_CHOCOLATE :          *this = Color(210, 105,  30); return;
			case X11_CORAL:               *this = Color(255, 127,  30); return;
			case X11_CORNFLOWER:          *this = Color(100, 149, 237); return;
			case X11_CORNSILK:            *this = Color(255, 248, 220); return;
			case X11_CRIMSON:             *this = Color(220,  20,  60); return;
			case X11_CYAN:                *this = Color(CYAN); return;
			case X11_DARK_BLUE:           *this = Color(  0,   0, 139); return;
			case X11_DARK_CYAN:           *this = Color(  0, 139, 139); return;
			case X11_DARK_GOLDENROD:      *this = Color(184, 134,  11); return;
			case X11_DARK_GRAY:           *this = Color(X11_DARK_GREY); return;
			case X11_DARK_GREY:           *this = Color(169, 169, 169); return;
			case X11_DARK_GREEN:          *this = Color(  0, 100,   0); return;
			case X11_DARK_KHAKI:          *this = Color(189, 183, 107); return;
			case X11_DARK_MAGENTA:        *this = Color(139,   0, 139); return;
			case X11_DARK_OLIVE_GREEN:    *this = Color( 85, 107,  47); return;
			case X11_DARK_ORANGE:         *this = Color(255, 140,   0); return;
			case X11_DARK_ORCHID:         *this = Color(153,  50, 204); return;
			case X11_DARK_RED:            *this = Color(139,   0,   0); return;
			case X11_DARK_SALMON:         *this = Color(233, 150, 122); return;
			case X11_DARK_SEA_GREEN:      *this = Color(143, 188, 143); return;
			case X11_DARK_SLATE_BLUE:     *this = Color( 72,  61, 139); return;
			case X11_DARK_SLATE_GRAY:     *this = Color(X11_DARK_SLATE_GREY); return;
			case X11_DARK_SLATE_GREY:     *this = Color( 47,  79,  79); return;
			case X11_DARK_TURQUOISE:      *this = Color(  0, 206, 209); return;
			case X11_DARK_VIOLET:         *this = Color(148,   0, 211); return;
			case X11_DEEP_PINK:           *this = Color(255,  20, 147); return;
			case X11_DEEP_SKY_BLUE:       *this = Color(  0, 191, 255); return;
			case X11_DIM_GRAY:            *this = Color(X11_DIM_GREY); return;
			case X11_DIM_GREY:            *this = Color(105, 105, 105); return;
			case X11_DODGE_BLUE:          *this = Color( 30, 144, 255); return;
			case X11_FIREBRICK:           *this = Color(178,  34,  34); return;
			case X11_FLORAL_WHITE:        *this = Color(255, 240, 240); return;
			case X11_FOREST_GREEN:        *this = Color( 34, 139,  34); return;
			case X11_FUCHSIA:             *this = Color(X11_MAGENTA); return;
			case X11_GAINSBORO:           *this = Color(220, 220, 220); return;
			case X11_GHOST_WHITE:         *this = Color(248, 248, 255); return;
			case X11_GOLD:                *this = Color(GOLD); return;
			case X11_GOLDENROD:           *this = Color(218, 165,  32); return;
			case X11_GRAY:                *this = Color(X11_GREY); return;
			case X11_GREY:                *this = Color(190, 190, 190); return;
			case X11_GREEN:               *this = Color(GREEN); return;
			case X11_GREEN_YELLOW:        *this = Color(173, 255,  47); return;
			case X11_HONEYDEW:            *this = Color(240, 255, 240); return;
			case X11_HOT_PINK:            *this = Color(255, 105, 180); return;
			case X11_INDIAN_RED:          *this = Color(205,  92,  92); return;
			case X11_INDIGO:              *this = Color(INDIGO); return;
			case X11_IVORY:               *this = Color(255, 255, 240); return;
			case X11_KHAKI:               *this = Color(240, 230, 140); return;
			case X11_LAVENDER:            *this = Color(230, 230, 250); return;
			case X11_LAVENDER_BUSH:       *this = Color(255, 240, 245); return;
			case X11_LAWN_GREEN:          *this = Color(124, 252,   0); return;
			case X11_LEMON_CHIFFON:       *this = Color(255, 250, 205); return;
			case X11_LIGHT_BLUE:          *this = Color(173, 216, 230); return;
			case X11_LIGHT_CORAL:         *this = Color(240, 128, 128); return;
			case X11_LIGHT_CYAN:          *this = Color(224, 255, 255); return;
			case X11_LIGHT_GOLDENROD:     *this = Color(250, 250, 210); return;
			case X11_LIGHT_GRAY:          *this = Color(X11_LIGHT_GREY); return;
			case X11_LIGHT_GREY:          *this = Color(211, 211, 211); return;
			case X11_LIGHT_GREEN:         *this = Color(144, 238, 144); return;
			case X11_LIGHT_PINK:          *this = Color(255, 182, 193); return;
			case X11_LIGHT_SALMON:        *this = Color(255, 160, 122); return;
			case X11_LIGHT_SEA_GREEN:     *this = Color( 32, 178, 170); return;
			case X11_LIGHT_SKY_BLUE:      *this = Color(135, 206, 250); return;
			case X11_LIGHT_SLATE_GRAY:    *this = Color(X11_LIGHT_SLATE_GREY); return;
			case X11_LIGHT_SLATE_GREY:    *this = Color(119, 136, 153); return;
			case X11_LIGHT_STEEL_BLUE:    *this = Color(176, 196, 222); return;
			case X11_LIGHT_YELLOW:        *this = Color(255, 255, 224); return;
			case X11_LIME:                *this = Color(X11_GREEN); return;
			case X11_LIME_GREEN:          *this = Color( 50, 205,  50); return;
			case X11_LINEN:               *this = Color(250, 240, 230); return;
			case X11_MAGENTA:             *this = Color(MAGENTA); return;
			case X11_MAROON:              *this = Color(176,  48,  96); return;
			case X11_MEDIUM_AQUAMARINE:   *this = Color(102, 205, 170); return;
			case X11_MEDIUM_BLUE:         *this = Color(  0,   0, 205); return;
			case X11_MEDIUM_ORCHID:       *this = Color(186,  85, 211); return;
			case X11_MEDIUM_PURPLE:       *this = Color(147, 112, 219); return;
			case X11_MEDIUM_SEA_GREEN:    *this = Color( 60, 179, 113); return;
			case X11_MEDIUM_SLATE_BLUE:   *this = Color(123, 104, 238); return;
			case X11_MEDIUM_SPRING_GREEN: *this = Color(  0, 250, 154); return;
			case X11_MEDIUM_TURQUOISE:    *this = Color( 72, 209, 204); return;
			case X11_MEDIUM_VIOLET_RED:   *this = Color(199,  21, 133); return;
			case X11_MIDNIGHT_BLUE:       *this = Color( 25,  25, 112); return;
			case X11_MINT_CREAM:          *this = Color(245, 255, 250); return;
			case X11_MISTY_ROSE:          *this = Color(255, 228, 225); return;
			case X11_MOCCASIN:            *this = Color(255, 228, 181); return;
			case X11_NAVAJO_WHITE:        *this = Color(255, 222, 173); return;
			case X11_NAVY_BLUE:           *this = Color(X11_NAVY); return;
			case X11_NAVY:                *this = Color(NAVY); return;
			case X11_OLD_LACE:            *this = Color(253, 245, 230); return;
			case X11_OLIVE:               *this = Color(OLIVE); return;
			case X11_OLIVE_DRAB:          *this = Color(107, 142,  35); return;
			case X11_ORANGE:              *this = Color(255, 165,   0); return;
			case X11_ORANGE_RED:          *this = Color(255,  69,   0); return;
			case X11_ORCHID:              *this = Color(218, 112, 214); return;
			case X11_PALE_GOLDENROD:      *this = Color(238, 232, 170); return;
			case X11_PALE_GREEN:          *this = Color(152, 251, 152); return;
			case X11_PALE_TURQUOISE:      *this = Color(175, 238, 238); return;
			case X11_PALE_VIOLET_RED:     *this = Color(219, 112, 147); return;
			case X11_PAPAYA_WHIP:         *this = Color(255, 239, 213); return;
			case X11_PEACH_PUFF:          *this = Color(255, 218, 185); return;
			case X11_PERU:                *this = Color(205, 133,  63); return;
			case X11_PINK:                *this = Color(255, 192, 203); return;
			case X11_PLUM:                *this = Color(221, 160, 221); return;
			case X11_POWDER_BLUE:         *this = Color(176, 224, 230); return;
			case X11_PURPLE:              *this = Color(160,  32, 240); return;
			case X11_REBECCA_PURPLE:      *this = Color(102,  51, 153); return;
			case X11_RED:                 *this = Color(RED); return;
			case X11_ROSY_BROWN:          *this = Color(188, 143, 143); return;
			case X11_ROYAL_BLUE:          *this = Color( 65, 105, 225); return;
			case X11_SADDLE_BROWN:        *this = Color(139,  69,  19); return;
			case X11_SALMON:              *this = Color(SALMON); return;
			case X11_SANDY_BROWN:         *this = Color(244, 164,  96); return;
			case X11_SEA_GREEN:           *this = Color( 46, 139,  87); return;
			case X11_SEASHELL:            *this = Color(255, 245, 238); return;
			case X11_SIENNA:              *this = Color(160,  82,  45); return;
			case X11_SILVER:              *this = Color(LIGHT_GREY); return;
			case X11_SKY_BLUE:            *this = Color(135, 206, 235); return;
			case X11_SLATE_BLUE:          *this = Color(106,  90, 205); return;
			case X11_SLATE_GRAY:          *this = Color(X11_SLATE_GREY); return;
			case X11_SLATE_GREY:          *this = Color(112, 128, 144); return;
			case X11_SNOW:                *this = Color(255, 250, 250); return;
			case X11_SPRING_GREEN:        *this = Color(SPRING_GREEN); return;
			case X11_STEEL_BLUE:          *this = Color( 70, 130, 180); return;
			case X11_TAN:                 *this = Color(210, 180, 140); return;
			case X11_TEAL:                *this = Color(TEAL); return;
			case X11_THISTLE:             *this = Color(216, 191, 216); return;
			case X11_TOMATO:              *this = Color(255,  99,  71); return;
			case X11_TURQUOISE:           *this = Color( 64, 224, 208); return;
			case X11_VIOLET:              *this = Color(238, 130, 238); return;
			case X11_WHEAT:               *this = Color(245, 222, 179); return;
			case X11_WHITE:               *this = Color(WHITE); return;
			case X11_WHITE_SMOKE:         *this = Color(245, 245, 245); return;
			case X11_YELLOW:              *this = Color(YELLOW); return;
			case X11_YELLOW_GREEN:        *this = Color(154, 205,  50); return;

			// W3C colors (name clashed with X11)
			case WEB_GRAY:     *this = Color(WEB_GREY); return;
			case WEB_GREY:     *this = Color(GREY); return;
			case WEB_GREEN:    *this = Color(DARK_GREEN); return;
			case WEB_MAROON:   *this = Color(MAROON); return;
			case WEB_PURPLE:   *this = Color(PURPLE); return;
		}
	}
}
