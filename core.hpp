/*
 * core.hpp
 *
 *  Created on: 26/11/2015
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2015  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef FGEAL_CORE_HPP_
#define FGEAL_CORE_HPP_
#include <ciso646>

#include <string>
#include <vector>

#define null NULL
#define extends :
#define abstract =0

/** Uncomment this macro to enable fgeal initialization check in methods and functions.
 *  By enabling this behavior, all methods and functions check if fgeal was initialized
 *  before executing anything. So, if a call to any of these functions is done when
 *  fgeal was not initialized, an exception is thrown.
 *  On the other hand, by leaving this macro commented, no check is done and there are
 *  some performance gains. However, any attempt to call fgeal methods/functions before
 *  calling initialization will cause undefined behavior (possibly seg. fault). */
//#define FGEAL_ENABLE_SAFE_MODE

/** A game engine interface for use with a proper adapter. */
namespace fgeal
{
	/** Stores a human-readable version string. */
	extern const char* VERSION;

	/** Stores a human-readable name of the backend adapter. */
	extern const char* ADAPTER_NAME;

	/** Stores a human-readable name of the underlying library. */
	extern const char* ADAPTED_LIBRARY_NAME;

	/** Stores a human-readable version string of the underlying library. */
	extern const char* ADAPTED_LIBRARY_VERSION;

	/// Foward declaring all wrapper classes.
	class Display;		// See display.hpp for class details.
	class Graphics;     // See graphics.hpp for class details.
	class Image;		// See image.hpp for class details.
	class Event;		// See event.hpp for class details.
	class EventQueue;	// See event.hpp for class details.
	class Font;			// See font.hpp for class details.
	class Sound;		// See sound.hpp for class details.
	class SoundStream;  // See sound.hpp for class details.
	class Music;		// See sound.hpp for class details.
	class Keyboard;     // See input.hpp for class details.
	class Mouse;        // See input.hpp for class details.
	class Joystick;     // See input.hpp for class details.

	// forward declare these to friend'em
	void initialize();
	void finalize();
	bool isInitialized();

	//internals
	class core
	{
		//friends
		friend void fgeal::initialize();
		friend void fgeal::finalize();
		friend bool fgeal::isInitialized();

		// flag to indicate that the library was initialized
		static bool isInitialized;

		core();  // non-instantiable

		// to be implemeted by adapter
		static void initialize();
		static void finalize();

		public:

		// checks if fgeal has been initialized and throws an AdapterException if not. (agnosticly implemented)
		static void checkInit();

		/** Different policies that can be applied when a call is made to a function or method that is not implemented on the underlying library.
		 *  Sometimes some functions and methods are not implemented, either because the adapter is not complete yet (and is experimental)
		 *  or it could not be implemented for lack of support from the underlying library (which should not happen often). */
		enum AbsentImplementationPolicy
		{
			IGNORE_SILENTLY,         // Does nothing and continue program execution. This should be fine for most functions but can cause undefined behavior.
			IGNORE_BUT_PRINT_STDERR, // Continue program execution, but prints the incident to stderr. Same as IGNORE_SILENTLY, but more verbose.
			THROW_EXCEPTION,         // Throws an NotImplementedException (default policy). This way, if the program catches the exception, it may continue execution.
			HALT_PROGRAM,            // Exits the program with a -1 result after printing the error to stderr. The harshest reaction as it halts the program altogether.
		};

		/** Policy applied when a call is made to a function or method that is not implemented on the underlying library.
		 *  Sometimes some functions and methods are not implemented, either because the adapter is not complete yet (and is experimental)
		 *  or it could not be implemented for lack of support from the underlying library (which should not happen often). */
		static AbsentImplementationPolicy absentImplementationPolicy;

		// signals that a not-implemented method has been called, triggering the action specified by the current policy (agnosticly implemented)
		static void reportAbsentImplementation(const std::string& description);
	};
}

#ifdef FGEAL_ENABLE_SAFE_MODE
	#define FGEAL_CHECK_INIT() core::checkInit()
#else
	#define FGEAL_CHECK_INIT()
#endif

#endif /* FGEAL_CORE_HPP_ */
