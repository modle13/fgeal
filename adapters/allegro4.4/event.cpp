/*
 * event.cpp
 *
 *  Created on: 03/03/2018
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2018  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/event.hpp"

#include "fgeal/exceptions.hpp"

#include <allegro.h>

#define isKeyRelease(rawByte) (rawByte & 0x80)
#define isKeyPress(rawByte) (!isKeyRelease(rawByte))
#define toScancode(rawByte) (rawByte & 0x7f)

#ifndef FGEAL_ALLEGRO_4_DISABLE_JOYSTICK_EVENTS_IMPL
	#define updateJoystickEvents(queueImpl) queueImpl.generateJoystickEvents()
#else
	#define updateJoystickEvents(queueImpl)  // replace with nothing at all
#endif

namespace fgeal
{
	// https://www.allegro.cc/forums/thread/607359
	// https://en.wikipedia.org/wiki/Event-driven_programming

	Event::Event()
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();
	}

	Event::~Event()
	{
		FGEAL_CHECK_INIT();
		delete &self;
	}

	Event::Type Event::getEventType()
	{
		FGEAL_CHECK_INIT();
		return self.type;
	}

	Keyboard::Key Event::getEventKeyCode()
	{
		FGEAL_CHECK_INIT();
		return KeyboardKeyMapping::toGenericKey(self.allegroKeyboardScancode);
	}

	Mouse::Button Event::getEventMouseButton()
	{
		FGEAL_CHECK_INIT();
		switch(self.allegroMouseFlag)
		{
			case MOUSE_FLAG_LEFT_DOWN:
			case MOUSE_FLAG_LEFT_UP:     return Mouse::BUTTON_LEFT;
			case MOUSE_FLAG_RIGHT_DOWN:
			case MOUSE_FLAG_RIGHT_UP:    return Mouse::BUTTON_RIGHT;
			case MOUSE_FLAG_MIDDLE_DOWN:
			case MOUSE_FLAG_MIDDLE_UP:   return Mouse::BUTTON_MIDDLE;
			default:                     return Mouse::BUTTON_UNKNOWN;
		}
	}

	int Event::getEventMouseX()
	{
		FGEAL_CHECK_INIT();
		return self.mouseX;
	}

	int Event::getEventMouseY()
	{
		FGEAL_CHECK_INIT();
		return self.mouseY;
	}

	int Event::getEventMouseWheelMotionAmount()
	{
		FGEAL_CHECK_INIT();
		return self.mouseWheelMotionAmount;
	}

	int Event::getEventJoystickIndex()
	{
		FGEAL_CHECK_INIT();
		#ifndef FGEAL_ALLEGRO_4_DISABLE_JOYSTICK_EVENTS_IMPL

		return self.joystickIndex;

		#else

		core::reportAbsentImplementation("This adapter does not support joystick events. Poll joystick state instead or re-compile fgeal without flag FGEAL_ALLEGRO_4_DISABLE_JOYSTICK_EVENTS_IMPL.");
		return -1;

		#endif  /* FGEAL_ALLEGRO_4_DISABLE_JOYSTICK_EVENTS_IMPL */
	}

	int Event::getEventJoystickButtonIndex()
	{
		FGEAL_CHECK_INIT();
		#ifndef FGEAL_ALLEGRO_4_DISABLE_JOYSTICK_EVENTS_IMPL

		return self.joystickButtonIndex;

		#else

		core::reportAbsentImplementation("This adapter does not support joystick events. Poll joystick state instead or re-compile fgeal without flag FGEAL_ALLEGRO_4_DISABLE_JOYSTICK_EVENTS_IMPL.");
		return -1;

		#endif  /* FGEAL_ALLEGRO_4_DISABLE_JOYSTICK_EVENTS_IMPL */
	}

	int Event::getEventJoystickAxisIndex()
	{
		FGEAL_CHECK_INIT();
		#ifndef FGEAL_ALLEGRO_4_DISABLE_JOYSTICK_EVENTS_IMPL

		// xxx this seems to be overly complex and possibly an overhead source
		for(int s = 0, previousAxesCount = 0; s < joy[self.joystickIndex].num_sticks; s++)
			if(s == self.joystickStickIndex)
				return previousAxesCount + self.joystickAxisIndex;
			else
				previousAxesCount += joy[self.joystickIndex].stick[s].num_axis;

		return -1;  // should never happen unless this is not a joystick event

		#else

		core::reportAbsentImplementation("This adapter does not support joystick events. Poll joystick state instead or re-compile fgeal without flag FGEAL_ALLEGRO_4_DISABLE_JOYSTICK_EVENTS_IMPL.");
		return -1;

		#endif  /* FGEAL_ALLEGRO_4_DISABLE_JOYSTICK_EVENTS_IMPL */
	}

	float Event::getEventJoystickAxisPosition()
	{
		FGEAL_CHECK_INIT();
		#ifndef FGEAL_ALLEGRO_4_DISABLE_JOYSTICK_EVENTS_IMPL

		// xxx Allegro 4 adapter's Event::getEventJoystickAxisPosition(): there may be a problem here because axis range may be [0 to 255] depending on the type of the control
		return self.joystickAxisPosition/127.0f;

		#else

		core::reportAbsentImplementation("This adapter does not support joystick events. Poll joystick state instead or re-compile fgeal without flag FGEAL_ALLEGRO_4_DISABLE_JOYSTICK_EVENTS_IMPL.");
		return 0;

		#endif  /* FGEAL_ALLEGRO_4_DISABLE_JOYSTICK_EVENTS_IMPL */
	}

	int Event::getEventJoystickSecondAxisIndex()
	{
		FGEAL_CHECK_INIT();
		return -1;
	}

	float Event::getEventJoystickSecondAxisPosition()
	{
		FGEAL_CHECK_INIT();
		return 0;
	}

	// ------------------------------------------------------------------------------------------------------------------------------

	EventQueue::EventQueue()
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();
		static bool needsToSetCallbacks = true;

		if(needsToSetCallbacks)
		{
			// set allegro 4 function pointers
			LOCK_VARIABLE(EventQueue::instance);

			LOCK_FUNCTION(implementation::customAllegroDisplayCloseButtonCallback);
			set_close_button_callback(implementation::customAllegroDisplayCloseButtonCallback);

			LOCK_FUNCTION(implementation::customAllegroKeyboardLowlevelCallback);
			keyboard_lowlevel_callback = implementation::customAllegroKeyboardLowlevelCallback;

			LOCK_FUNCTION(implementation::customAllegroMouseCallback);
			mouse_callback = implementation::customAllegroMouseCallback;

			needsToSetCallbacks = false;
		}

		#ifndef FGEAL_ALLEGRO_4_DISABLE_JOYSTICK_EVENTS_IMPL

		self.joystickState.resize(num_joysticks);
		for(int j = 0; j < num_joysticks; j++)
		{
			self.joystickState[j].buttonState.resize(joy[j].num_buttons, false);  // resize buttons array
			self.joystickState[j].axisState.resize(joy[j].num_sticks);  // resize axis array by the "sticks" dimension
			for(int s = 0; s < joy[j].num_sticks; s++)
				self.joystickState[j].axisState[s].resize(joy[j].stick[s].num_axis, 0);  // resize axis array by the "axis" dimension
		}

		#endif  /* FGEAL_ALLEGRO_4_DISABLE_JOYSTICK_EVENTS_IMPL */
	}

	EventQueue::~EventQueue()
	{
		FGEAL_CHECK_INIT();
		while(not self.events.empty())
		if(self.events.front() != null)
		{
			delete self.events.front();
			self.events.pop();
		}

		delete &self;
	}

	bool EventQueue::isEmpty()
	{
		FGEAL_CHECK_INIT();
		updateJoystickEvents(self);
		return self.events.empty();
	}

	bool EventQueue::hasEvents()
	{
		FGEAL_CHECK_INIT();
		updateJoystickEvents(self);
		return not self.events.empty();
	}

	Event* EventQueue::getNextEvent()
	{
		FGEAL_CHECK_INIT();
		updateJoystickEvents(self);
		if(self.events.empty())
			return null;

		Event* const nextEvent = self.events.front();  // copy pointer to event
		self.events.pop();  // remove it from queue
		return nextEvent;  // and return it
	}

	bool EventQueue::getNextEvent(Event* container)
	{
		FGEAL_CHECK_INIT();
		updateJoystickEvents(self);
		if(self.events.empty())
			return false;

		container->self = self.events.front()->self;  // make a copy of the event (NOT of its pointer) to the container
		delete self.events.front();  // delete event
		self.events.pop();  // and remove its pointer from the queue
		return true;
	}

	bool EventQueue::skipNextEvent()
	{
		FGEAL_CHECK_INIT();
		updateJoystickEvents(self);
		if(self.events.empty())
			return false;

		delete self.events.front();  // delete event
		self.events.pop();  // and remove its pointer from the queue
		return true;
	}

	void EventQueue::waitNextEvent(Event* container)
	{
		FGEAL_CHECK_INIT();
		updateJoystickEvents(self);
		while(self.events.empty())
			::rest(100);

		if(container != null)
		{
			container->self = self.events.front()->self;  // make a copy of the event (NOT of its pointer) to the container
			delete self.events.front();  // delete event
			self.events.pop();  // and remove its pointer from the queue
		}
	}

	void EventQueue::flushEvents()
	{
		FGEAL_CHECK_INIT();
		updateJoystickEvents(self);
		while(not self.events.empty())
		{
			delete self.events.front();  // delete event
			self.events.pop();  // and remove its pointer from the queue
		}
	}

	void EventQueue::setEventBypassingEnabled(bool bypass)
	{
		FGEAL_CHECK_INIT();
		self.isBypassEnabled = bypass;
	}

	// ------------------------------------------------------------------------------------------------------------------------------

	#ifndef FGEAL_ALLEGRO_4_DISABLE_JOYSTICK_EVENTS_IMPL

	void EventQueue::implementation::generateJoystickEvents()
	{
		if(joystickState.empty())
			return;

		poll_joystick();
		for(unsigned j = 0; j < joystickState.size(); j++)
		{
			for(unsigned b = 0; b < joystickState[j].buttonState.size(); b++)
			if(joystickState[j].buttonState[b] and not joy[j].button[b].b)
			{
				joystickState[j].buttonState[b] = false;
				Event* const joystickEvent = new Event();
				getEventImplementation(*joystickEvent).type = Event::TYPE_JOYSTICK_BUTTON_RELEASE;
				getEventImplementation(*joystickEvent).joystickIndex = j;
				getEventImplementation(*joystickEvent).joystickButtonIndex = b;
				EventQueue::instance->self.events.push(joystickEvent);
			}
			else if(not joystickState[j].buttonState[b] and joy[j].button[b].b)
			{
				joystickState[j].buttonState[b] = true;
				Event* const joystickEvent = new Event();
				getEventImplementation(*joystickEvent).type = Event::TYPE_JOYSTICK_BUTTON_PRESS;
				getEventImplementation(*joystickEvent).joystickIndex = j;
				getEventImplementation(*joystickEvent).joystickButtonIndex = b;
				EventQueue::instance->self.events.push(joystickEvent);
			}

			for(unsigned s = 0; s < joystickState[j].axisState.size(); s++)
			for(unsigned a = 0; a < joystickState[j].axisState[s].size(); a++)
			if(joystickState[j].axisState[s][a] != joy[j].stick[s].axis[a].pos)
			{
				joystickState[j].axisState[s][a] = joy[j].stick[s].axis[a].pos;
				Event* const joystickEvent = new Event();
				getEventImplementation(*joystickEvent).type = Event::TYPE_JOYSTICK_AXIS_MOTION;
				getEventImplementation(*joystickEvent).joystickIndex = j;
				getEventImplementation(*joystickEvent).joystickStickIndex = s;
				getEventImplementation(*joystickEvent).joystickAxisIndex = a;
				getEventImplementation(*joystickEvent).joystickAxisPosition = joystickState[j].axisState[s][a];
				EventQueue::instance->self.events.push(joystickEvent);
			}
		}
	}

	#endif  /* FGEAL_ALLEGRO_4_DISABLE_JOYSTICK_EVENTS_IMPL */

	void EventQueue::implementation::customAllegroDisplayCloseButtonCallback()
	{
		if(EventQueue::instance == null or EventQueue::instance->self.isBypassEnabled) return;
		Event* const displayCloseEvent = new Event();
		getEventImplementation(*displayCloseEvent).type = Event::TYPE_DISPLAY_CLOSURE;
		EventQueue::instance->self.events.push(displayCloseEvent);
	}
	END_OF_FUNCTION(EventQueue::implementation::customAllegroDisplayCloseButtonCallback)

	void EventQueue::implementation::customAllegroKeyboardLowlevelCallback(int rawByte)
	{
		if(EventQueue::instance == null or EventQueue::instance->self.isBypassEnabled) return;
		Event* const keyboardEvent = new Event();
		getEventImplementation(*keyboardEvent).type = isKeyRelease(rawByte)? Event::TYPE_KEY_RELEASE : Event::TYPE_KEY_PRESS;
		getEventImplementation(*keyboardEvent).allegroKeyboardScancode = toScancode(rawByte);
		EventQueue::instance->self.events.push(keyboardEvent);
	}
	END_OF_FUNCTION(EventQueue::implementation::customAllegroKeyboardLowlevelCallback)

	void EventQueue::implementation::customAllegroMouseCallback(int flag)
	{
		if(EventQueue::instance == null or EventQueue::instance->self.isBypassEnabled) return;
		const int currentMousePosition = mouse_pos;
		static int lastMouseZ = 0;
		Event* const mouseEvent = new Event();
		switch (flag)
		{
			case MOUSE_FLAG_MOVE:
				getEventImplementation(*mouseEvent).type = Event::TYPE_MOUSE_MOTION; break;
			case MOUSE_FLAG_LEFT_DOWN:
			case MOUSE_FLAG_RIGHT_DOWN:
			case MOUSE_FLAG_MIDDLE_DOWN:
				getEventImplementation(*mouseEvent).type = Event::TYPE_MOUSE_BUTTON_PRESS; break;
			case MOUSE_FLAG_LEFT_UP:
			case MOUSE_FLAG_RIGHT_UP:
			case MOUSE_FLAG_MIDDLE_UP:
				getEventImplementation(*mouseEvent).type = Event::TYPE_MOUSE_BUTTON_RELEASE; break;
			#if ALLEGRO_SUB_VERSION >= 2 and ALLEGRO_WIP_VERSION >= 2
			case MOUSE_FLAG_MOVE_W:  // supported on Allegro v4.2.2 RC1 and onwards
			#endif
			case MOUSE_FLAG_MOVE_Z:
				getEventImplementation(*mouseEvent).type = Event::TYPE_MOUSE_WHEEL_MOTION; break;
			default: break;
		}
		getEventImplementation(*mouseEvent).allegroMouseFlag = flag;
		getEventImplementation(*mouseEvent).mouseX = currentMousePosition >> 16;
		getEventImplementation(*mouseEvent).mouseY = currentMousePosition & 0x0000ffff;
		getEventImplementation(*mouseEvent).mouseWheelMotionAmount = (lastMouseZ == 0? mouse_z : mouse_z - lastMouseZ);
		lastMouseZ = mouse_z;

		EventQueue::instance->self.events.push(mouseEvent);
	}
	END_OF_FUNCTION(EventQueue::implementation::customAllegroMouseCallback)
}
