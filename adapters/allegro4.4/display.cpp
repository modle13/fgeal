/*
 * display.cpp
 *
 *  Created on: 20/03/2018
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2018  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/display.hpp"

#include "fgeal/exceptions.hpp"

#include <allegro.h>

#include <string>
using std::string;

#define array_sizeof(arr) (sizeof(arr)/sizeof(arr[0]))
#define usingHardwareCursor (gfx_capabilities & GFX_HW_CURSOR)

namespace fgeal
{
	#if ALLEGRO_SUB_VERSION < 4
	#define GFX_NONE                       AL_ID('N','O','N','E')
	int get_gfx_mode()
	{
		return (gfx_driver? gfx_driver->id : GFX_NONE);
	}
	#endif

	// ------------------------------------------------------------------------------------------------------------------------------

	Display::Display(const Options& options)
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();

		self.currentTitle = options.title;
		set_window_title(self.currentTitle.c_str());

		//set graphics mode, trying all acceptable depths
		const int allegroSupportedDepths[5] = {32, 24, 16, 15, 8}, gfxCardFlag = options.fullscreen? GFX_AUTODETECT : GFX_AUTODETECT_WINDOWED;
		bool setGfxModeSuccess = false;
		for(int i = 0; i < (int) array_sizeof(allegroSupportedDepths) and not setGfxModeSuccess; i++)
		{
			set_color_depth(allegroSupportedDepths[i]);
			if(set_gfx_mode(gfxCardFlag, options.width, options.height, 0, 0) == 0)
				setGfxModeSuccess = true;  // success
		}

		// if set gfx fails, tries safe mode
		if(not setGfxModeSuccess)
			if(set_gfx_mode(GFX_SAFE, options.width, options.height, 0, 0) != 0)  // if safe mode failes, throw exception
				throw AdapterException("Could not set graphics mode! %s", allegro_error);

		// create back buffer to draw to
		self.backBufferBitmap = create_bitmap(screen->w, screen->h);
		if(self.backBufferBitmap == null)
			throw AdapterException("Could not create the display backbuffer! %s", allegro_error);

		// ensure alpha blending mode for blitting and drawing
		set_alpha_blender();

		// make primitive drawing in trans mode
		drawing_mode(DRAW_MODE_TRANS, null, 0, 0);

		// Note: do not use show_os_cursor() because it's complicated to use since it may fail and cannot be used with other cursor visibility functions
		// Note 2: avoiding usign select_mouse_cursor(MOUSE_CURSOR_ARROW) for enabling system cursor since it may not be work on some platforms
		enable_hardware_cursor();  // attempt to let the OS render the cursor for better results

		// sets the bitmap where the mouse should be drawn
		show_mouse(screen);

		// cursor visibility state
		self.cursorVisible = true;

		fgeal::drawTargetBitmap = self.backBufferBitmap;

		// xxx Allegro 4 adapter ignoring resizeable flag
		// xxx Allegro 4 adapter ignoring positioning flag
		// xxx Allegro 4 adapter ignoring icon setting
	}

	Display::~Display()
	{
		FGEAL_CHECK_INIT();
		destroy_bitmap(self.backBufferBitmap);
		delete &self;
	}

	unsigned Display::getWidth()
	{
		FGEAL_CHECK_INIT();
		return self.backBufferBitmap->w;
	}

	unsigned Display::getHeight()
	{
		FGEAL_CHECK_INIT();
		return self.backBufferBitmap->h;
	}

	string Display::getTitle() const
	{
		FGEAL_CHECK_INIT();
		return self.currentTitle;
	}

	void Display::setTitle(const string& title)
	{
		FGEAL_CHECK_INIT();
		self.currentTitle = title;
		set_window_title(self.currentTitle.c_str());
	}

	//fixme Allegro 4 adapter's Display::setIcon() not implemented
	void Display::setIcon(const std::string& iconFilename)
	{
		FGEAL_CHECK_INIT();
		core::reportAbsentImplementation("Display::setIcon is not implemented in this adapter.");
		/* On Windows:
		 * When creating the main window, Allegro searches the executable for an ICON resource named "allegro_icon".
		 * If it is present, Allegro automatically loads it and uses it as its application icon; otherwise, Allegro uses the default IDI_APPLICATION icon.
		 * See the manual of your compiler for a method to create an ICON resource, or use the wfixicon utility from the tools/win directory.
		 *
		 * On Linux:
		 * extern void *allegro_icon is a pointer to the Allegro X11 icon, which is in the format of standard .xpm bitmap data.
		 * You do not normally have to bother with this at all.
		 * You can use the xfixicon.sh utility from the tools/x11 directory to convert a true colour bitmap to a C file that you only need to link with your own code to set the icon.
		 */
	}

	void Display::setSize(unsigned width, unsigned height)
	{
		FGEAL_CHECK_INIT();
		if(set_gfx_mode(get_gfx_mode(), width, height, 0, 0) != 0)
			throw AdapterException("Could not resize display! %s", allegro_error);

		if(self.cursorVisible)
			show_mouse(screen);  // points to the new screen where the mouse should be drawn
	}

	void Display::refresh()
	{
		FGEAL_CHECK_INIT();
		if(not usingHardwareCursor) scare_mouse();
		draw_sprite(screen, self.backBufferBitmap, 0, 0);
		if(not usingHardwareCursor) unscare_mouse();
	}

	void Display::clear()
	{
		FGEAL_CHECK_INIT();
		clear_bitmap(self.backBufferBitmap);
	}

	bool Display::isFullscreen()
	{
		FGEAL_CHECK_INIT();
		return (is_windowed_mode() == 0);
	}

	void Display::setFullscreen(bool choice)
	{
		FGEAL_CHECK_INIT();
		if(set_gfx_mode(choice==true? GFX_AUTODETECT : GFX_AUTODETECT_WINDOWED, screen->w, screen->h, 0, 0) != 0)
			throw AdapterException("Could not toogle fullscreen on display! %s", allegro_error);

		// attempt again to let the OS render the cursor for better results since toogling fullscreen/windowed affects its availability
		enable_hardware_cursor();

		if(self.cursorVisible)
			show_mouse(screen);  // points to the new screen where the mouse should be drawn
	}

	//fixme Allegro 4 adapter's Display::setPosition() not implemented
	void Display::setPosition(const Point& pos)
	{
		FGEAL_CHECK_INIT();
		core::reportAbsentImplementation("Display::setPosition is not implemented in this adapter.");
	}

	//fixme Allegro 4 adapter's Display::setPositionOnCenter() not implemented
	void Display::setPositionOnCenter()
	{
		FGEAL_CHECK_INIT();
		core::reportAbsentImplementation("Display::setPositionOnCenter is not implemented in this adapter.");
	}

	void Display::setMouseCursorVisible(bool enabled)
	{
		FGEAL_CHECK_INIT();
		show_mouse(enabled? screen : null);
		self.cursorVisible = enabled;
	}
}
