/*
 * core.cpp
 *
 *  Created on: 20/03/2018
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2018  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"

#include "fgeal/core.hpp"

#include "fgeal/exceptions.hpp"

#include <allegro.h>

#ifdef FGEAL_ALLEGRO_4_LOADPNG_INSTALLED
	#include <loadpng.h>
#endif
#ifdef FGEAL_ALLEGRO_4_JPGALLEG_INSTALLED
	#include <jpgalleg.h>
#endif
#ifdef FGEAL_ALLEGRO_4_ALGIF_INSTALLED
	#include <algif.h>
#endif
#ifdef FGEAL_ALLEGRO_4_LOGG_INSTALLED
	#include <logg.h>
#endif
#ifdef FGEAL_ALLEGRO_4_ALOGG_INSTALLED
	#include <alogg/alogg.h>
#endif
#ifdef FGEAL_ALLEGRO_4_ALLEGROMP3_INSTALLED
	#include <almp3.h>
#endif
#ifdef FGEAL_ALLEGRO_4_ALLEGTTF_INSTALLED
	#include <allegttf.h>
#endif

#if ALLEGRO_SUB_VERSION <= 2 and ALLEGRO_WIP_VERSION < 1
	#define file_size_ex(filename) file_size(filename)
#endif

#include <algorithm>
#include <string>

namespace fgeal
{
	/* fgeal code based on Allegro 4.4 */
	const char* ADAPTER_NAME = "Allegro 4.4 Adapter for fgeal";
	const char* ADAPTED_LIBRARY_NAME = "Allegro";
	const char* ADAPTED_LIBRARY_VERSION = ALLEGRO_VERSION_STR;

	static bool isAllegroInit;
	static long allegroTickCount;
	static void customAllegroUserTimerHandler();

	#ifdef FGEAL_ALLEGRO_4_ALLEGTTF_INSTALLED
	static FONT* customAllegTtfLoadFont(AL_CONST char *filename, RGB *dummy, void *params);
	#endif

	#ifdef FGEAL_ALLEGRO_4_ALLEGROMP3_INSTALLED
	static SAMPLE* customAllegroMp3LoadSample(AL_CONST char* filename);
	#endif

	static int mouseButtonCount;  // needs to be of type int

	// initialize all allegro stuff
	void core::initialize()
	{
		if(allegro_init() != 0)                                        throw AdapterException("Allegro could not be initialized: %s", allegro_error);
		if(install_timer() != 0)                                       throw AdapterException("Could not install timer routines: %s", allegro_error);
		if(install_keyboard() != 0)                                    throw AdapterException("Could not install keyboard input subsytem: %s", allegro_error);
		if((mouseButtonCount = install_mouse()) == -1)                 throw AdapterException("Could not install mouse input subsytem: %s", allegro_error);
		if(install_joystick(JOY_TYPE_AUTODETECT) != 0)                 throw AdapterException("Could not install joystick input subsytem: %s", allegro_error);
		if(install_sound(DIGI_AUTODETECT, MIDI_AUTODETECT, null) != 0) throw AdapterException("Could not install sound subsytem: %s", allegro_error);

		#ifdef FGEAL_ALLEGRO_4_LOADPNG_INSTALLED
		loadpng_init();  // registers png loader
		#endif

		#ifdef FGEAL_ALLEGRO_4_JPGALLEG_INSTALLED
		jpgalleg_init();  // registers jpg loader
		register_bitmap_file_type("jpeg", load_jpg, save_jpg);  // register ".jpeg" extension as well
		#endif

		#ifdef FGEAL_ALLEGRO_4_ALGIF_INSTALLED
		algif_init();  // registers gif loader
		#endif

		#ifdef FGEAL_ALLEGRO_4_LOGG_INSTALLED
		register_sample_file_type("ogg", logg_load, null);
		#elif defined(FGEAL_ALLEGRO_4_ALOGG_INSTALLED)
		alogg_init();
		#endif

		#ifdef FGEAL_ALLEGRO_4_ALLEGROMP3_INSTALLED
		register_sample_file_type("mp3", customAllegroMp3LoadSample, null);
		#endif

		#ifdef FGEAL_ALLEGRO_4_ALLEGTTF_INSTALLED
		register_font_file_type("ttf", customAllegTtfLoadFont);
		#endif

		set_keyboard_rate(0, 0);

		isAllegroInit = true;
		LOCK_VARIABLE(allegroTickCount);
		LOCK_FUNCTION(customAllegroUserTimerHandler);
		install_int(customAllegroUserTimerHandler, 1);
		setlocale(LC_NUMERIC, "C");  // avoids allegro's setting of current locale
	}

	void core::finalize()
	{
		FGEAL_CHECK_INIT();
		allegro_exit();
	}

	bool isProperlyInitialized()
	{
		return isAllegroInit;
	}

	void rest(double seconds)
	{
		FGEAL_CHECK_INIT();
		::rest(seconds*1000);
	}

	double uptime()
	{
		FGEAL_CHECK_INIT();
		return allegroTickCount/1000.0f;
	}

	// --------------------------------------------------------------------

	static void customAllegroUserTimerHandler()
	{
		allegroTickCount++;
	}
	END_OF_FUNCTION(customAllegroUserTimerHandler)

	#ifdef FGEAL_ALLEGRO_4_ALLEGTTF_INSTALLED
	static FONT* customAllegTtfLoadFont(AL_CONST char* filename, RGB* dummy, void* params)
	{
			const int size = static_cast<int*>(params)[0]*0.8;
			const int smoothFlag = static_cast<int*>(params)[1] == 0? ALLEGTTF_NOSMOOTH : size > 8 ? ALLEGTTF_TTFSMOOTH : ALLEGTTF_REALSMOOTH;
			return load_ttf_font(filename, size, smoothFlag);
	}
	END_OF_FUNCTION(customAllegTtfLoadFont)
	#endif /* FGEAL_ALLEGRO_4_ALLEGTTF_INSTALLED */

	#ifdef FGEAL_ALLEGRO_4_ALLEGROMP3_INSTALLED
	static SAMPLE* customAllegroMp3LoadSample(AL_CONST char* filename)
	{
		PACKFILE* const mp3packfile = pack_fopen(filename, "r");  // open file for reading
		if(mp3packfile == null)
			return null;

		const long mp3filesize = file_size_ex(filename);  // estimate file size
		char* const mp3data = new char[mp3filesize];  // allocate buffer for data
		const long mp3datasize = pack_fread(mp3data, mp3filesize, mp3packfile);  // read and save actual data size (the amount of bytes read)
		ALMP3_MP3* const mp3 = almp3_create_mp3(mp3data, mp3datasize);  // create ALMP3_MP3 instance
		if(mp3 == null)
		{
			delete[] mp3data;
			pack_fclose(mp3packfile);
			return null;
		}

		SAMPLE* const sample = almp3_create_sample_from_mp3(mp3);
		// free temporary data (xxx Allegro 4 adapter: uncertain if these buffers should be free'd or kept until SAMPLE's lifetime - see https://allegro.cc/forums/thread/553582)
		almp3_destroy_mp3(mp3);
		delete[] mp3data;
		pack_fclose(mp3packfile);
		return sample;  // if 'sample'==null, null is returned, but we have already free'd 'mp3', 'mp3data' and 'mp3packfile'
	}
	END_OF_FUNCTION(customAllegroMp3LoadSample)
	#endif /* FGEAL_ALLEGRO_4_ALLEGROMP3_INSTALLED */
}
