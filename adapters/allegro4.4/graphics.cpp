/*
 * graphics.cpp
 *
 *  Created on: 21/06/2018
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2018  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/graphics.hpp"

#include <allegro.h>
#include <cmath>

using std::vector;

namespace fgeal
{
	// extern-expected on implementation.hpp
	BITMAP* drawTargetBitmap;

	// static
	void Graphics::setDrawTarget(const Image* image)
	{
		FGEAL_CHECK_INIT();
		if(image == null)
			fgeal::drawTargetBitmap = Display::instance->self.backBufferBitmap;
		else
			fgeal::drawTargetBitmap = image->self.allegroBitmap;
	}

	// static
	void Graphics::setDrawTarget(const Display& display)
	{
		FGEAL_CHECK_INIT();
		if(&display == null)
			fgeal::drawTargetBitmap = Display::instance->self.backBufferBitmap;
		else
			fgeal::drawTargetBitmap = display.self.backBufferBitmap;
	}

	// static
	void Graphics::setDefaultDrawTarget()
	{
		FGEAL_CHECK_INIT();
		fgeal::drawTargetBitmap = Display::instance->self.backBufferBitmap;
	}

	///xxx dirty workaround for allegro 4 magenta transparency behavior.
	static inline int makeacol2(const int r, const int g, const int b, const int a)
	{
		return (r == 255 and g == 0 and b == 255)? makeacol(255, 1, 255, a) : makeacol(r, g, b, a);
	}

	// =====================================================================================================================================================
	// Primitives

	void Graphics::drawLine(float x1, float y1, float x2, float y2, const Color& c)
	{
		FGEAL_CHECK_INIT();
		fastline(fgeal::drawTargetBitmap, x1, y1, x2, y2, makeacol2(c.r, c.g, c.b, c.a));
	}

	void Graphics::drawRectangle(float x, float y, float w, float h, const Color& c)
	{
		FGEAL_CHECK_INIT();
		rect(fgeal::drawTargetBitmap, x, y, x+w, y+h, makeacol2(c.r, c.g, c.b, c.a));
	}

	void Graphics::drawFilledRectangle(float x, float y, float w, float h, const Color& c)
	{
		FGEAL_CHECK_INIT();
		rectfill(fgeal::drawTargetBitmap, x, y, x+w, y+h, makeacol2(c.r, c.g, c.b, c.a));
	}

	void Graphics::drawCircle(float cx, float cy, float radius, const Color& c)
	{
		FGEAL_CHECK_INIT();
		circle(fgeal::drawTargetBitmap, cx, cy, radius, makeacol2(c.r, c.g, c.b, c.a));
	}

	void Graphics::drawFilledCircle(float cx, float cy, float radius, const Color& c)
	{
		FGEAL_CHECK_INIT();
		circlefill(fgeal::drawTargetBitmap, cx, cy, radius, makeacol2(c.r, c.g, c.b, c.a));
	}

	void Graphics::drawEllipse(float cx, float cy, float rx, float ry, const Color& c)
	{
		FGEAL_CHECK_INIT();
		ellipse(fgeal::drawTargetBitmap, cx, cy, rx, ry, makeacol2(c.r, c.g, c.b, c.a));
	}

	void Graphics::drawFilledEllipse(float cx, float cy, float rx, float ry, const Color& c)
	{
		FGEAL_CHECK_INIT();
		ellipsefill(fgeal::drawTargetBitmap, cx, cy, rx, ry, makeacol2(c.r, c.g, c.b, c.a));
	}

	void Graphics::drawTriangle(float x1, float y1, float x2, float y2, float x3, float y3, const Color& c)
	{
		FGEAL_CHECK_INIT();
		fastline(fgeal::drawTargetBitmap, x1, y1, x2, y2, makeacol2(c.r, c.g, c.b, c.a));
		fastline(fgeal::drawTargetBitmap, x1, y1, x3, y3, makeacol2(c.r, c.g, c.b, c.a));
		fastline(fgeal::drawTargetBitmap, x3, y3, x2, y2, makeacol2(c.r, c.g, c.b, c.a));
	}

	void Graphics::drawFilledTriangle(float x1, float y1, float x2, float y2, float x3, float y3, const Color& c)
	{
		FGEAL_CHECK_INIT();
		triangle(fgeal::drawTargetBitmap, x1, y1, x2, y2, x3, y3, makeacol(c.r, c.g, c.b, c.a));
	}

	void Graphics::drawQuadrangle(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4, const Color& c)
	{
		FGEAL_CHECK_INIT();
		fastline(fgeal::drawTargetBitmap, x1, y1, x2, y2, makeacol2(c.r, c.g, c.b, c.a));
		fastline(fgeal::drawTargetBitmap, x2, y2, x3, y3, makeacol2(c.r, c.g, c.b, c.a));
		fastline(fgeal::drawTargetBitmap, x3, y3, x4, y4, makeacol2(c.r, c.g, c.b, c.a));
		fastline(fgeal::drawTargetBitmap, x4, y4, x1, y1, makeacol2(c.r, c.g, c.b, c.a));
	}

	void Graphics::drawFilledQuadrangle(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4, const Color& c)
	{
		FGEAL_CHECK_INIT();
		const int vertices[8] = {(int) x1, (int) y1, (int) x2, (int) y2, (int) x3, (int) y3, (int) x4, (int) y4};
		polygon(fgeal::drawTargetBitmap, 4, vertices, makeacol2(c.r, c.g, c.b, c.a));
	}

	void Graphics::drawArc(float cx, float cy, float r, float ai, float af, const Color& c)
	{
		FGEAL_CHECK_INIT();
		arc(fgeal::drawTargetBitmap, cx, cy, -toAllegroFixedAngle(ai), -toAllegroFixedAngle(af), r, makeacol2(c.r, c.g, c.b, c.a));
	}

	namespace aux
	{
		static std::vector<int> coordCache;

		//xxx (NOT THREAD SAFE) callback to save points of do_arc() calls
		static void copyArcPoint(BITMAP *bmp, int x, int y, int)
		{
			aux::coordCache.push_back(x);
			aux::coordCache.push_back(y);
		}
	}

	void Graphics::drawCircleSector(float cx, float cy, float r, float ai, float af, const Color& c)
	{
		FGEAL_CHECK_INIT();
		aux::coordCache.clear();
		aux::coordCache.push_back(cx);
		aux::coordCache.push_back(cy);
		do_arc(fgeal::drawTargetBitmap, cx, cy, -toAllegroFixedAngle(ai), -toAllegroFixedAngle(af), r, makeacol2(c.r, c.g, c.b, c.a), aux::copyArcPoint);
		for(unsigned i = 0; i < aux::coordCache.size()-3; i += 2)
			fastline(fgeal::drawTargetBitmap, aux::coordCache[i], aux::coordCache[i+1], aux::coordCache[i+2], aux::coordCache[i+3], makeacol2(c.r, c.g, c.b, c.a));
		fastline(fgeal::drawTargetBitmap, aux::coordCache[aux::coordCache.size()-2], aux::coordCache[aux::coordCache.size()-1], aux::coordCache[0], aux::coordCache[1], makeacol2(c.r, c.g, c.b, c.a));
	}

	void Graphics::drawFilledCircleSector(float cx, float cy, float r, float ai, float af, const Color& c)
	{
		FGEAL_CHECK_INIT();
		aux::coordCache.clear();
		aux::coordCache.push_back(cx);
		aux::coordCache.push_back(cy);
		do_arc(fgeal::drawTargetBitmap, cx, cy, -toAllegroFixedAngle(ai), -toAllegroFixedAngle(af), r, makeacol2(c.r, c.g, c.b, c.a), aux::copyArcPoint);
		polygon(fgeal::drawTargetBitmap, aux::coordCache.size()/2, &(aux::coordCache[0]), makeacol2(c.r, c.g, c.b, c.a));
	}

	void Graphics::drawRoundedRectangle(float x, float y, float width, float height, float rd, const Color& c)
	{
		FGEAL_CHECK_INIT();
		Graphics::genericDrawRoundedRectangle(x, y, width, height, rd, c);
	}

	void Graphics::drawFilledRoundedRectangle(float x, float y, float width, float height, float rd, const Color& c)
	{
		FGEAL_CHECK_INIT();
		Graphics::genericDrawFilledRoundedRectangle(x, y, width, height, rd, c);
	}

	void Graphics::drawPolygon(const vector<float>& x, const vector<float>& y, const Color& c)
	{
		FGEAL_CHECK_INIT();
		const unsigned n = std::min(x.size(), y.size());
		if(n == 1)
			putpixel(fgeal::drawTargetBitmap, x[0], y[0], makeacol2(c.r, c.g, c.b, c.a));
		else if(n > 1)
		{
			fastline(fgeal::drawTargetBitmap, x[0], y[0], x[n-1], y[n-1], makeacol2(c.r, c.g, c.b, c.a));
			if(n > 2) for(unsigned i = 1; i < n; i++)
				fastline(fgeal::drawTargetBitmap, x[i], y[i], x[i-1], y[i-1], makeacol2(c.r, c.g, c.b, c.a));
		}
	}

	void Graphics::drawFilledPolygon(const vector<float>& x, const vector<float>& y, const Color& c)
	{
		FGEAL_CHECK_INIT();
		const unsigned n = std::min(x.size(), y.size());
		vector<int> vertices(n*2);
		for(unsigned i = 0; i < n; i++)
			vertices[2*i] = x[i], vertices[2*i+1] = y[i];
		polygon(fgeal::drawTargetBitmap, n, &vertices[0], makeacol2(c.r, c.g, c.b, c.a));
	}
}
