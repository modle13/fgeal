/*
 * implementation.hpp
 *
 *  Created on: 20/03/2018
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2018  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef FGEAL_ADAPTERS_ALLEGRO4_4_IMPL_HPP_
#define FGEAL_ADAPTERS_ALLEGRO4_4_IMPL_HPP_

#include "fgeal/display.hpp"
#include "fgeal/image.hpp"
#include "fgeal/event.hpp"
#include "fgeal/font.hpp"
#include "fgeal/sound.hpp"
#include "fgeal/input.hpp"

#define ALLEGRO_NO_KEY_DEFINES
#include <allegro.h>
#include <cmath>

#define FGEAL_ALLEGRO_4_LOADPNG_INSTALLED     // uncomment if loadpng library is present  (linker flag -lloadpng)
#define FGEAL_ALLEGRO_4_JPGALLEG_INSTALLED    // uncomment if jpgalleg library is present  (linker flag -ljpgalleg)
#define FGEAL_ALLEGRO_4_ALGIF_INSTALLED       // uncomment if algif library is present  (linker flag -lalgif)
#define FGEAL_ALLEGRO_4_LOGG_INSTALLED        // uncomment if logg library is present   (linker flag -llogg)
//#define FGEAL_ALLEGRO_4_ALOGG_INSTALLED     // uncomment if alogg library is present  (linker flag -lalogg)
#define FGEAL_ALLEGRO_4_ALLEGROMP3_INSTALLED  // uncomment if AllegroMP3 is available   (linker flag -lalmp3)
#define FGEAL_ALLEGRO_4_ALLEGTTF_INSTALLED    // uncomment if AllegTTF is available   (linker flag -lalttf)

//#define FGEAL_ALLEGRO_4_DISABLE_JOYSTICK_EVENTS_IMPL  // uncomment if you don't want/need fgeal's custom joystick events implementation

#include <string>
#include <queue>
#include <algorithm>

#define fgealFilenameHasExtension(filename, extension) util::stringEndsWith(util::stringToLowercase(filename), extension)

namespace fgeal
{
	/// Current draw target bitmap
	extern BITMAP* drawTargetBitmap;

	/// Converts angle, in radians, of float type to Allegro's 0-255 format (8-bit), of custom type "fixed"
	inline fixed toAllegroFixedAngle(const float angle)
	{
		return ftofix(-128.0*angle/M_PI);
	}

	inline bool filenameHasExtension(std::string filename, std::string extension)
	{
		std::transform(filename.begin(), filename.end(), filename.begin(), ::tolower);
		std::transform(extension.begin(), extension.end(), extension.begin(), ::tolower);
		extension.insert(0, ".");

		if(filename.length() >= extension.length())
			return (0 == filename.compare (filename.length() - extension.length(), extension.length(), extension));
		else
			return false;
	}

	struct Display::implementation
	{
		std::string currentTitle;
		BITMAP* backBufferBitmap;
		bool cursorVisible;
	};

	struct Image::implementation
	{
		BITMAP* allegroBitmap;
		bool needsTransFunction;
	};

	struct Event::implementation
	{
		Type type;
		int allegroKeyboardScancode, allegroMouseFlag;
		int mouseX, mouseY;

		#ifndef FGEAL_ALLEGRO_4_DISABLE_JOYSTICK_EVENTS_IMPL

		int joystickIndex, joystickButtonIndex, joystickStickIndex, joystickAxisIndex, joystickAxisPosition;

		#endif  /* #ifndef FGEAL_ALLEGRO_4_DISABLE_JOYSTICK_EVENTS_IMPL */

		Mouse::Button mouseButton;
		int mouseWheelMotionAmount;

		// todo Allegro 4 adapter: put joystick-related event data here
	};

	struct EventQueue::implementation
	{
		#ifndef FGEAL_ALLEGRO_4_DISABLE_JOYSTICK_EVENTS_IMPL

		struct CustomAllegro4JoystickStateInfo
		{
			std::vector<bool> buttonState;
			std::vector< std::vector<int> > axisState;
		};
		std::vector< CustomAllegro4JoystickStateInfo > joystickState;

		void generateJoystickEvents();

		#endif  /* #ifndef FGEAL_ALLEGRO_4_DISABLE_JOYSTICK_EVENTS_IMPL */

		std::queue<Event*> events;
		bool isBypassEnabled;

		static void customAllegroDisplayCloseButtonCallback(void);
		static void customAllegroKeyboardLowlevelCallback(int);
		static void customAllegroMouseCallback(int);
	};

	struct Font::implementation
	{
		FONT* allegroFont;
	};

	struct Sound::implementation
	{
		SAMPLE* allegroSample;
		int volume, frequency;
	};

	struct SoundStream::implementation
	{
		//AUDIOSTREAM* allegroAudioStream;
		Sound* fgealSound;
	};

	struct Music::implementation
	{
		MIDI* allegroMidi;
		long midiPausedPosition;
		fgeal::SoundStream* fgealSoundStream;
	};

	namespace KeyboardKeyMapping
	{
		/// Returns the fgeal-equivalent key enum of the given Allegro 4 keycode.
		Keyboard::Key toGenericKey(int allegroKeycode);

		/// Returns the Allegro 4 keycode of the given fgeal-equivalent key enum.
		int toUnderlyingLibraryKey(Keyboard::Key key);
	}
}

#endif /* FGEAL_ADAPTERS_ALLEGRO4_4_IMPL_HPP_ */
