/*
 * input.cpp
 *
 *  Created on: 28/03/2018
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2018  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/input.hpp"

#include <allegro.h>

#define FORWARD_MAPPING(a, b)  case a: return b;
#define BACKWARD_MAPPING(a, b) case b: return a;

using std::vector;
using std::string;

namespace fgeal
{
	// =======================================================================================
	// Keyboard

	namespace KeyboardKeyMapping
	{
		#define ALLEGRO_4_4_KEY_MAPPINGS(mapping)\
		mapping(__allegro_KEY_A,  Keyboard::KEY_A)\
		mapping(__allegro_KEY_B,  Keyboard::KEY_B)\
		mapping(__allegro_KEY_C,  Keyboard::KEY_C)\
		mapping(__allegro_KEY_D,  Keyboard::KEY_D)\
		mapping(__allegro_KEY_E,  Keyboard::KEY_E)\
		mapping(__allegro_KEY_F,  Keyboard::KEY_F)\
		mapping(__allegro_KEY_G,  Keyboard::KEY_G)\
		mapping(__allegro_KEY_H,  Keyboard::KEY_H)\
		mapping(__allegro_KEY_I,  Keyboard::KEY_I)\
		mapping(__allegro_KEY_J,  Keyboard::KEY_J)\
		mapping(__allegro_KEY_K,  Keyboard::KEY_K)\
		mapping(__allegro_KEY_L,  Keyboard::KEY_L)\
		mapping(__allegro_KEY_M,  Keyboard::KEY_M)\
		mapping(__allegro_KEY_N,  Keyboard::KEY_N)\
		mapping(__allegro_KEY_O,  Keyboard::KEY_O)\
		mapping(__allegro_KEY_P,  Keyboard::KEY_P)\
		mapping(__allegro_KEY_Q,  Keyboard::KEY_Q)\
		mapping(__allegro_KEY_R,  Keyboard::KEY_R)\
		mapping(__allegro_KEY_S,  Keyboard::KEY_S)\
		mapping(__allegro_KEY_T,  Keyboard::KEY_T)\
		mapping(__allegro_KEY_U,  Keyboard::KEY_U)\
		mapping(__allegro_KEY_V,  Keyboard::KEY_V)\
		mapping(__allegro_KEY_W,  Keyboard::KEY_W)\
		mapping(__allegro_KEY_X,  Keyboard::KEY_X)\
		mapping(__allegro_KEY_Y,  Keyboard::KEY_Y)\
		mapping(__allegro_KEY_Z,  Keyboard::KEY_Z)\
		\
		mapping(__allegro_KEY_0,  Keyboard::KEY_0)\
		mapping(__allegro_KEY_1,  Keyboard::KEY_1)\
		mapping(__allegro_KEY_2,  Keyboard::KEY_2)\
		mapping(__allegro_KEY_3,  Keyboard::KEY_3)\
		mapping(__allegro_KEY_4,  Keyboard::KEY_4)\
		mapping(__allegro_KEY_5,  Keyboard::KEY_5)\
		mapping(__allegro_KEY_6,  Keyboard::KEY_6)\
		mapping(__allegro_KEY_7,  Keyboard::KEY_7)\
		mapping(__allegro_KEY_8,  Keyboard::KEY_8)\
		mapping(__allegro_KEY_9,  Keyboard::KEY_9)\
		\
		mapping(__allegro_KEY_F1,  Keyboard::KEY_F1)\
		mapping(__allegro_KEY_F2,  Keyboard::KEY_F2)\
		mapping(__allegro_KEY_F3,  Keyboard::KEY_F3)\
		mapping(__allegro_KEY_F4,  Keyboard::KEY_F4)\
		mapping(__allegro_KEY_F5,  Keyboard::KEY_F5)\
		mapping(__allegro_KEY_F6,  Keyboard::KEY_F6)\
		mapping(__allegro_KEY_F7,  Keyboard::KEY_F7)\
		mapping(__allegro_KEY_F8,  Keyboard::KEY_F8)\
		mapping(__allegro_KEY_F9,  Keyboard::KEY_F9)\
		mapping(__allegro_KEY_F10, Keyboard::KEY_F10)\
		mapping(__allegro_KEY_F11, Keyboard::KEY_F11)\
		mapping(__allegro_KEY_F12, Keyboard::KEY_F12)\
		\
		mapping(__allegro_KEY_UP,    Keyboard::KEY_ARROW_UP)\
		mapping(__allegro_KEY_DOWN,  Keyboard::KEY_ARROW_DOWN)\
		mapping(__allegro_KEY_LEFT,  Keyboard::KEY_ARROW_LEFT)\
		mapping(__allegro_KEY_RIGHT, Keyboard::KEY_ARROW_RIGHT)\
		\
		mapping(__allegro_KEY_ENTER,      Keyboard::KEY_ENTER)\
		mapping(__allegro_KEY_SPACE,      Keyboard::KEY_SPACE)\
		mapping(__allegro_KEY_ESC,        Keyboard::KEY_ESCAPE)\
		mapping(__allegro_KEY_LCONTROL,   Keyboard::KEY_LEFT_CONTROL)\
		mapping(__allegro_KEY_RCONTROL,   Keyboard::KEY_RIGHT_CONTROL)\
		mapping(__allegro_KEY_LSHIFT,     Keyboard::KEY_LEFT_SHIFT)\
		mapping(__allegro_KEY_RSHIFT,     Keyboard::KEY_RIGHT_SHIFT)\
		mapping(__allegro_KEY_ALT,        Keyboard::KEY_LEFT_ALT)\
		mapping(__allegro_KEY_ALTGR,      Keyboard::KEY_RIGHT_ALT)\
		mapping(__allegro_KEY_LWIN,       Keyboard::KEY_LEFT_SUPER)\
		mapping(__allegro_KEY_RWIN,       Keyboard::KEY_RIGHT_SUPER)\
		mapping(__allegro_KEY_MENU,       Keyboard::KEY_MENU)\
		mapping(__allegro_KEY_TAB,        Keyboard::KEY_TAB)\
		mapping(__allegro_KEY_BACKSPACE,  Keyboard::KEY_BACKSPACE)\
		mapping(__allegro_KEY_MINUS,      Keyboard::KEY_MINUS)\
		mapping(__allegro_KEY_EQUALS,     Keyboard::KEY_EQUALS)\
		mapping(__allegro_KEY_OPENBRACE,  Keyboard::KEY_LEFT_BRACKET)\
		mapping(__allegro_KEY_CLOSEBRACE, Keyboard::KEY_RIGHT_BRACKET)\
		mapping(__allegro_KEY_SEMICOLON,  Keyboard::KEY_SEMICOLON)\
		mapping(__allegro_KEY_COMMA,      Keyboard::KEY_COMMA)\
		mapping(__allegro_KEY_STOP,       Keyboard::KEY_PERIOD)\
		mapping(__allegro_KEY_SLASH,      Keyboard::KEY_SLASH)\
		mapping(__allegro_KEY_BACKSLASH,  Keyboard::KEY_BACKSLASH)\
		mapping(__allegro_KEY_QUOTE,      Keyboard::KEY_QUOTE)\
		mapping(__allegro_KEY_TILDE,      Keyboard::KEY_TILDE)\
		\
		mapping(__allegro_KEY_INSERT, Keyboard::KEY_INSERT)\
		mapping(__allegro_KEY_DEL,    Keyboard::KEY_DELETE)\
		mapping(__allegro_KEY_HOME,   Keyboard::KEY_HOME)\
		mapping(__allegro_KEY_END,    Keyboard::KEY_END)\
		mapping(__allegro_KEY_PGUP,   Keyboard::KEY_PAGE_UP)\
		mapping(__allegro_KEY_PGDN,   Keyboard::KEY_PAGE_DOWN)\
		\
		mapping(__allegro_KEY_0_PAD,  Keyboard::KEY_NUMPAD_0)\
		mapping(__allegro_KEY_1_PAD,  Keyboard::KEY_NUMPAD_1)\
		mapping(__allegro_KEY_2_PAD,  Keyboard::KEY_NUMPAD_2)\
		mapping(__allegro_KEY_3_PAD,  Keyboard::KEY_NUMPAD_3)\
		mapping(__allegro_KEY_4_PAD,  Keyboard::KEY_NUMPAD_4)\
		mapping(__allegro_KEY_5_PAD,  Keyboard::KEY_NUMPAD_5)\
		mapping(__allegro_KEY_6_PAD,  Keyboard::KEY_NUMPAD_6)\
		mapping(__allegro_KEY_7_PAD,  Keyboard::KEY_NUMPAD_7)\
		mapping(__allegro_KEY_8_PAD,  Keyboard::KEY_NUMPAD_8)\
		mapping(__allegro_KEY_9_PAD,  Keyboard::KEY_NUMPAD_9)\
		\
/*		mapping(__allegro_KEY_PLUS_PAD,   Keyboard::KEY_NUMPAD_ADDITION)\
		mapping(__allegro_KEY_MINUS_PAD,  Keyboard::KEY_NUMPAD_SUBTRACTION)\
		mapping(__allegro_KEY_ASTERISK,   Keyboard::KEY_NUMPAD_MULTIPLICATION)\
		mapping(__allegro_KEY_SLASH_PAD,  Keyboard::KEY_NUMPAD_DIVISION)\
		mapping(__allegro_KEY_DEL_PAD,    Keyboard::KEY_NUMPAD_DECIMAL)\
		mapping(__allegro_KEY_ENTER_PAD,  Keyboard::KEY_NUMPAD_ENTER)\ */
		//end

		// Returns the fgeal-equivalent key enum of the given allegro keycode.
		Keyboard::Key toGenericKey(int allegroKeycode)
		{
			switch(allegroKeycode)
			{
				ALLEGRO_4_4_KEY_MAPPINGS(FORWARD_MAPPING);
				default: return Keyboard::KEY_UNKNOWN;
			}
		}

		// Returns the Allegro 5.0 keycode of the given fgeal-equivalent key enum.
		int toUnderlyingLibraryKey(Keyboard::Key key)
		{
			switch(key)
			{
				ALLEGRO_4_4_KEY_MAPPINGS(BACKWARD_MAPPING);
				default: return __allegro_KEY_MAX;
			}
		}
	}


	// API functions

	bool Keyboard::isKeyPressed(Keyboard::Key k)
	{
		FGEAL_CHECK_INIT();
		if(keyboard_needs_poll()) poll_keyboard();
		return key[KeyboardKeyMapping::toUnderlyingLibraryKey(k)];
	}

	// =======================================================================================
	// Mouse
	// API functions

	bool Mouse::isButtonPressed(Mouse::Button btn)
	{
		FGEAL_CHECK_INIT();
		if(mouse_needs_poll()) poll_mouse();
		switch(btn)
		{
			case Mouse::BUTTON_LEFT:   return (mouse_b & 0);
			case Mouse::BUTTON_MIDDLE: return (mouse_b & 1);
			case Mouse::BUTTON_RIGHT:  return (mouse_b & 2);
			default: return false;
		}
	}

	Point Mouse::getPosition()
	{
		FGEAL_CHECK_INIT();
		if(mouse_needs_poll()) poll_mouse();
		const int position = mouse_pos;
		const Point pt = {static_cast<float>(position >> 16), static_cast<float>(position & 0x0000ffff)};
		return pt;
	}

	void Mouse::setPosition(const Point& position)
	{
		FGEAL_CHECK_INIT();
		position_mouse(position.x, position.y);
	}

	int Mouse::getPositionX()
	{
		FGEAL_CHECK_INIT();
		if(mouse_needs_poll()) poll_mouse();
		return mouse_x;
	}

	int Mouse::getPositionY()
	{
		FGEAL_CHECK_INIT();
		if(mouse_needs_poll()) poll_mouse();
		return mouse_y;
	}

	void Mouse::setPosition(int x, int y)
	{
		FGEAL_CHECK_INIT();
		if(mouse_needs_poll()) poll_mouse();
		position_mouse(x, y);
	}

	// =======================================================================================
	// Joystick

	// nothing is needed
	void Joystick::configureAll()
	{}

	// nothing is needed
	void Joystick::releaseAll()
	{}

	unsigned Joystick::getCount()
	{
		FGEAL_CHECK_INIT();
		poll_joystick();
		return num_joysticks;
	}

	string Joystick::getJoystickName(unsigned joyIndex)
	{
		FGEAL_CHECK_INIT();
		core::reportAbsentImplementation("Getting joystick name is not supported on this adapter.");
		return std::string();  //xxx Allegro 4 adapter: there is no way to get a joystick's name
	}

	/** Returns the number of buttons on this joystick. */
	unsigned Joystick::getButtonCount(unsigned joyIndex)
	{
		FGEAL_CHECK_INIT();
		poll_joystick();
		return joy[joyIndex].num_buttons;
	}

	/** Returns the number of axis on this joystick. */
	unsigned Joystick::getAxisCount(unsigned joyIndex)
	{
		FGEAL_CHECK_INIT();
		poll_joystick();
		unsigned axesCount = 0;
		for(int s = 0; s < joy[joyIndex].num_sticks; s++)
			axesCount += joy[joyIndex].stick[s].num_axis;
		return axesCount;
	}

	/** Returns true if the given button (by index) is being pressed on this joystick. */
	bool Joystick::isButtonPressed(unsigned joyIndex, unsigned buttonIndex)
	{
		FGEAL_CHECK_INIT();
		poll_joystick();
		return joy[joyIndex].button[buttonIndex].b;
	}

	/** Returns the current position of the given axis (by index) on this joystick. */
	float Joystick::getAxisPosition(unsigned joyIndex, unsigned axisIndex)
	{
		FGEAL_CHECK_INIT();
		poll_joystick();
		// xxx Allegro 4 adapter's Joystick::getAxisPosition(): there may be a problem here because axis range may be [0 to 255] depending on the type of the control
		return joy[joyIndex].stick[axisIndex/2].axis[axisIndex%2].pos/127.0f;  // converting range [-128 to 128] to [-1.0 to 1.0]
	}
}
