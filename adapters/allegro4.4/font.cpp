/*
 * font.cpp
 *
 *  Created on: 03/03/2018
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2018  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/font.hpp"

#include "fgeal/exceptions.hpp"

#include <allegro.h>

using std::string;

namespace fgeal
{
	Font::Font(const string& filename, int size, bool antialiasing)
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();

		#ifdef FGEAL_ALLEGRO_4_ALLEGTTF_INSTALLED
		if(filenameHasExtension(filename, "ttf"))
		{
			int paramsAllegTTF[2] = { size, antialiasing? 1 : 0 };  // parameters to be passed to AllegTTF functions if using it
			solid_mode();  // to render TTF fonts, we need the drawing mode to be the solid one
			self.allegroFont = load_font(filename.c_str(), null, paramsAllegTTF);  // call load_font, passing TTF parameters
			if(self.allegroFont == null)
				throw AdapterException("Font %s could not be loaded: Error %s", filename.c_str(), allegro_error);
			drawing_mode(DRAW_MODE_TRANS, null, 0, 0);  // restore trans drawing mode
			return;
		}
		#endif

		self.allegroFont = load_font(filename.c_str(), null, null);
		if(self.allegroFont == null)
			throw AdapterException("Font %s could not be loaded: Error %s", filename.c_str(), allegro_error);
	}

	Font::~Font()
	{
		FGEAL_CHECK_INIT();
		destroy_font(self.allegroFont);
		delete &self;
	}

	void Font::drawText(const string& text, float x, float y, Color c)
	{
		FGEAL_CHECK_INIT();
		textout_ex(fgeal::drawTargetBitmap, self.allegroFont, text.c_str(), x, y, makecol(c.r, c.g, c.b), -1);
	}

	float Font::getHeight() const
	{
		FGEAL_CHECK_INIT();
		return text_height(self.allegroFont);
	}

	float Font::getTextWidth(const std::string& text) const
	{
		FGEAL_CHECK_INIT();
		return text_length(self.allegroFont, text.c_str());
	}
}
