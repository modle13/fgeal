/*
 * font.cpp
 *
 *  Created on: 23/01/2017
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2017  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/font.hpp"

#include "fgeal/exceptions.hpp"

using std::string;

#define renderWindow Display::instance->self.sfmlRenderWindow

namespace fgeal
{
	Font::Font(const string& filename, int size, bool antialiasing)
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();
		sf::Font* font = new sf::Font();
		if(not font->LoadFromFile(filename, size))
		{
			delete font; font = null;
			throw AdapterException("Font \"%s\" could not be loaded.", filename.c_str());
		}

		self.sfmlString.SetFont(*font);
		self.sfmlString.SetText("");
		self.sfmlString.SetBlendMode(antialiasing? sf::Blend::Alpha : sf::Blend::None);
		self.sfmlString.SetSize(size);
	}

	Font::~Font()
	{
		delete &self.sfmlString.GetFont();
		delete &self;
	}

	void Font::drawText(const string& text, float x, float y, Color color)
	{
		FGEAL_CHECK_INIT();
		self.sfmlString.SetText(text);
		self.sfmlString.SetPosition(x, y);
		self.sfmlString.SetColor(sf::Color(color.r, color.g, color.b, color.a));
		renderWindow.Draw(self.sfmlString);
	}

	float Font::getHeight() const
	{
		FGEAL_CHECK_INIT();
		self.sfmlString.SetText("|");
		return self.sfmlString.GetRect().GetHeight();
//		return self.sfmlString.GetSize();  // bad results
//		return self.sfmlString.GetFont().GetCharacterSize();  // bad results, same as sf::String::GetSize()
	}
	
	float Font::getTextWidth(const string& text) const
	{
		FGEAL_CHECK_INIT();
		self.sfmlString.SetText(text);
		return self.sfmlString.GetRect().GetWidth();
	}
}
