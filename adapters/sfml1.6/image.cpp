/*
 * image.cpp
 *
 *  Created on: 22/01/2017
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2017  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/image.hpp"

#include "fgeal/exceptions.hpp"

#include <algorithm>

using std::string;

const static double toDegree = (180.0/M_PI);

// auxiliary macros
#define renderWindow Display::instance->self.sfmlRenderWindow
#define imageWidth sfmlSprite.GetImage()->GetWidth()
#define imageHeight sfmlSprite.GetImage()->GetHeight()

namespace fgeal
{
	Image::Image(const string& filename)
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();
		sf::Image* image = new sf::Image();
		if(not image->LoadFromFile(filename))
		{
			delete image; image = null;
			throw AdapterException("Could not load image: \"%s\"", filename.c_str());
		}

		self.sfmlSprite.SetImage(*image);
	}

	Image::Image(int w, int h)
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();
		self.sfmlSprite.SetImage(*new sf::Image(w, h));
	}

	Image::~Image()
	{
		FGEAL_CHECK_INIT();
		delete self.sfmlSprite.GetImage();
		delete &self;
	}

	int Image::getWidth()
	{
		FGEAL_CHECK_INIT();
		return self.imageWidth;
	}

	int Image::getHeight()
	{
		FGEAL_CHECK_INIT();
		return self.imageHeight;
	}

	void Image::draw(float x, float y)
	{
		FGEAL_CHECK_INIT();

		//draws all source region
		self.sfmlSprite.SetPosition(x, y);
		renderWindow.Draw(self.sfmlSprite);
	}

	void Image::drawRegion(float x, float y, float from_x, float from_y, float w, float h)
	{
		FGEAL_CHECK_INIT();

		self.sfmlSprite.SetSubRect(sf::IntRect(from_x, from_y, from_x+w, from_y+h));
		self.sfmlSprite.SetPosition(x, y);
		renderWindow.Draw(self.sfmlSprite);

		//restore source region to full
		self.sfmlSprite.SetSubRect(sf::IntRect(0, 0, self.imageWidth, self.imageHeight));
	}

	void Image::drawFlipped(float x, float y, const FlipMode flipmode)
	{
		FGEAL_CHECK_INIT();

		//draws all source region
		self.sfmlSprite.FlipX(flipmode == FLIP_HORIZONTAL);
		self.sfmlSprite.FlipY(flipmode == FLIP_VERTICAL);
		self.sfmlSprite.SetPosition(x, y);
		renderWindow.Draw(self.sfmlSprite);

		self.sfmlSprite.FlipX(false);
		self.sfmlSprite.FlipY(false);
	}

	void Image::drawFlippedRegion(float x, float y, const FlipMode flipmode, float fromX, float fromY, float fromWidth, float fromHeight)
	{
		FGEAL_CHECK_INIT();

		self.sfmlSprite.SetSubRect(sf::IntRect(fromX, fromY, fromX+fromWidth, fromY+fromHeight));
		self.sfmlSprite.FlipX(flipmode == FLIP_HORIZONTAL);
		self.sfmlSprite.FlipY(flipmode == FLIP_VERTICAL);
		self.sfmlSprite.SetPosition(x, y);
		renderWindow.Draw(self.sfmlSprite);

		//restore source region to full, clear flip
		self.sfmlSprite.FlipX(false);
		self.sfmlSprite.FlipY(false);
		self.sfmlSprite.SetSubRect(sf::IntRect(0, 0, self.imageWidth, self.imageHeight));
	}

	void Image::drawScaled(float x, float y, float xScale, float yScale, const FlipMode flipmode)
	{
		FGEAL_CHECK_INIT();

		//draws all source region
		self.sfmlSprite.FlipX(flipmode == FLIP_HORIZONTAL);
		self.sfmlSprite.FlipY(flipmode == FLIP_VERTICAL);
		self.sfmlSprite.SetScale(xScale, yScale);
		self.sfmlSprite.SetPosition(x, y);
		renderWindow.Draw(self.sfmlSprite);

		self.sfmlSprite.SetScale(1.0, 1.0);
		self.sfmlSprite.FlipX(false);
		self.sfmlSprite.FlipY(false);
	}

	void Image::drawScaledRegion(float x, float y, float xScale, float yScale, const FlipMode flipmode, float fromX, float fromY, float fromWidth, float fromHeight)
	{
		FGEAL_CHECK_INIT();

		self.sfmlSprite.SetSubRect(sf::IntRect(fromX, fromY, fromX+fromWidth, fromY+fromHeight));
		self.sfmlSprite.FlipX(flipmode == FLIP_HORIZONTAL);
		self.sfmlSprite.FlipY(flipmode == FLIP_VERTICAL);
		self.sfmlSprite.SetScale(xScale, yScale);
		self.sfmlSprite.SetPosition(x, y);
		renderWindow.Draw(self.sfmlSprite);

		self.sfmlSprite.SetScale(1.0, 1.0);
		self.sfmlSprite.FlipX(false);
		self.sfmlSprite.FlipY(false);
		self.sfmlSprite.SetSubRect(sf::IntRect(0, 0, self.imageWidth, self.imageHeight));
	}

	void Image::drawRotated(float x, float y, float angle, float centerX, float centerY, const FlipMode flipmode)
	{
		FGEAL_CHECK_INIT();

		//draw all source region
		self.sfmlSprite.FlipX(flipmode == FLIP_HORIZONTAL);
		self.sfmlSprite.FlipY(flipmode == FLIP_VERTICAL);
		self.sfmlSprite.SetCenter(centerX, centerY);
		self.sfmlSprite.SetRotation(angle*toDegree);
		self.sfmlSprite.SetPosition(x, y);
		renderWindow.Draw(self.sfmlSprite);

		// restore center to top-left and clear rotation
		self.sfmlSprite.SetCenter(0, 0);
		self.sfmlSprite.SetRotation(0);
		self.sfmlSprite.FlipX(false);
		self.sfmlSprite.FlipY(false);
	}

	void Image::drawRotatedRegion(float x, float y, float angle, float centerX, float centerY, const FlipMode flipmode, float fromX, float fromY, float fromWidth, float fromHeight)
	{
		FGEAL_CHECK_INIT();

		self.sfmlSprite.SetSubRect(sf::IntRect(fromX, fromY, fromX+fromWidth, fromY+fromHeight));
		self.sfmlSprite.FlipX(flipmode == FLIP_HORIZONTAL);
		self.sfmlSprite.FlipY(flipmode == FLIP_VERTICAL);
		self.sfmlSprite.SetCenter(centerX, centerY);
		self.sfmlSprite.SetRotation(angle*toDegree);
		self.sfmlSprite.SetPosition(x, y);
		renderWindow.Draw(self.sfmlSprite);

		// restore center to top-left, clear rotation and restore source region to full
		self.sfmlSprite.SetCenter(0, 0);
		self.sfmlSprite.SetRotation(0);
		self.sfmlSprite.FlipX(false);
		self.sfmlSprite.FlipY(false);
		self.sfmlSprite.SetSubRect(sf::IntRect(0, 0, self.imageWidth, self.imageHeight));
	}

	void Image::drawScaledRotated(float x, float y, float xScale, float yScale, float angle, float centerX, float centerY, const FlipMode flipmode)
	{
		FGEAL_CHECK_INIT();

		//draw all source region
		self.sfmlSprite.FlipX(flipmode == FLIP_HORIZONTAL);
		self.sfmlSprite.FlipY(flipmode == FLIP_VERTICAL);
		self.sfmlSprite.SetScale(xScale, yScale);
		self.sfmlSprite.SetCenter(centerX, centerY);
		self.sfmlSprite.SetRotation(angle*toDegree);
		self.sfmlSprite.SetPosition(x, y);
		renderWindow.Draw(self.sfmlSprite);

		// restore center to top-left and clear rotation
		self.sfmlSprite.SetCenter(0, 0);
		self.sfmlSprite.SetRotation(0);
		self.sfmlSprite.SetScale(1.0, 1.0);
		self.sfmlSprite.FlipX(false);
		self.sfmlSprite.FlipY(false);
	}

	void Image::drawScaledRotatedRegion(float x, float y, float xScale, float yScale, float angle, float centerX, float centerY, const FlipMode flipmode, float fromX, float fromY, float fromWidth, float fromHeight)
	{
		FGEAL_CHECK_INIT();

		self.sfmlSprite.SetSubRect(sf::IntRect(fromX, fromY, fromX+fromWidth, fromY+fromHeight));
		self.sfmlSprite.FlipX(flipmode == FLIP_HORIZONTAL);
		self.sfmlSprite.FlipY(flipmode == FLIP_VERTICAL);
		self.sfmlSprite.SetScale(xScale, yScale);
		self.sfmlSprite.SetCenter(centerX, centerY);
		self.sfmlSprite.SetRotation(angle*toDegree);
		self.sfmlSprite.SetPosition(x, y);
		renderWindow.Draw(self.sfmlSprite);

		// restore center to top-left and clear rotation
		self.sfmlSprite.SetCenter(0, 0);
		self.sfmlSprite.SetRotation(0);
		self.sfmlSprite.SetScale(1.0, 1.0);
		self.sfmlSprite.FlipX(false);
		self.sfmlSprite.FlipY(false);
		self.sfmlSprite.SetSubRect(sf::IntRect(0, 0, self.imageWidth, self.imageHeight));
	}

	//fixme SFML 1.6 Image::blit() not implemented
	void Image::blit(Image& img, float x, float y, float xScale, float yScale, float angle, float centerX, float centerY, const FlipMode flipmode, float fromX, float fromY, float fromWidth, float fromHeight)
	{
		FGEAL_CHECK_INIT();
		core::reportAbsentImplementation("Image::blit() is not implemented in this adapter.");
	}
}
