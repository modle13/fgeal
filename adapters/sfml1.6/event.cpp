/*
 * event.cpp
 *
 *  Created on: 14/01/2017
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2017  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/event.hpp"

#define window Display::instance->self.sfmlRenderWindow

namespace fgeal
{
	Event::Event()
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();
	}

	Event::~Event()
	{
		FGEAL_CHECK_INIT();
		delete &self;
	}

	Event::Type Event::getEventType()
	{
		FGEAL_CHECK_INIT();
		switch(self.sfmlEvent.Type)
		{
			case sf::Event::Closed:					return Event::TYPE_DISPLAY_CLOSURE;
			case sf::Event::KeyPressed: 			return Event::TYPE_KEY_PRESS;
			case sf::Event::KeyReleased:			return Event::TYPE_KEY_RELEASE;
			case sf::Event::MouseButtonPressed: 	return Event::TYPE_MOUSE_BUTTON_PRESS;
			case sf::Event::MouseButtonReleased:	return Event::TYPE_MOUSE_BUTTON_RELEASE;
			case sf::Event::MouseMoved:				return Event::TYPE_MOUSE_MOTION;
			case sf::Event::MouseWheelMoved:		return Event::TYPE_MOUSE_WHEEL_MOTION;
			case sf::Event::JoyButtonPressed:	    return Event::TYPE_JOYSTICK_BUTTON_PRESS;
			case sf::Event::JoyButtonReleased:      return Event::TYPE_JOYSTICK_BUTTON_RELEASE;
			case sf::Event::JoyMoved:			    return Event::TYPE_JOYSTICK_AXIS_MOTION;

			default:								return Event::TYPE_UNKNOWN;
		}
	}

	Keyboard::Key Event::getEventKeyCode()
	{
		FGEAL_CHECK_INIT();
		return KeyboardKeyMapping::toGenericKey(self.sfmlEvent.Key.Code);
	}

	Mouse::Button Event::getEventMouseButton()
	{
		FGEAL_CHECK_INIT();
		return MouseButtonMapping::toGenericMouseButton(self.sfmlEvent.MouseButton.Button);
	}

	int Event::getEventMouseX()
	{
		FGEAL_CHECK_INIT();
		return self.sfmlEvent.Type == sf::Event::MouseMoved? self.sfmlEvent.MouseMove.X : self.sfmlEvent.MouseButton.X;
	}

	int Event::getEventMouseY()
	{
		FGEAL_CHECK_INIT();
		return self.sfmlEvent.Type == sf::Event::MouseMoved? self.sfmlEvent.MouseMove.Y : self.sfmlEvent.MouseButton.Y;
	}

	int Event::getEventMouseWheelMotionAmount()
	{
		FGEAL_CHECK_INIT();
		return self.sfmlEvent.MouseWheel.Delta;
	}

	int Event::getEventJoystickIndex()
	{
		FGEAL_CHECK_INIT();
		return self.sfmlEvent.Type == sf::Event::JoyButtonPressed
				or self.sfmlEvent.Type == sf::Event::JoyButtonReleased? self.sfmlEvent.JoyButton.JoystickId
                          : self.sfmlEvent.Type == sf::Event::JoyMoved? self.sfmlEvent.JoyMove.JoystickId
                                                                            : -1;
	}

	int Event::getEventJoystickButtonIndex()
	{
		FGEAL_CHECK_INIT();
		return self.sfmlEvent.JoyButton.Button;
	}

	int Event::getEventJoystickAxisIndex()
	{
		FGEAL_CHECK_INIT();
		return self.sfmlEvent.JoyMove.Axis + (self.sfmlEvent.JoyMove.Axis == sf::Joy::AxisPOV and self.hatPovMotion == POV_Y? 1 : 0);
	}

	float Event::getEventJoystickAxisPosition()
	{
		FGEAL_CHECK_INIT();
		if(self.sfmlEvent.JoyMove.Axis != sf::Joy::AxisPOV)
			return self.sfmlEvent.JoyMove.Position/100.0f;
		else
			return self.hatPovMotion == POV_Y? self.hatPovY : self.hatPovX;
	}

	int Event::getEventJoystickSecondAxisIndex()
	{
		FGEAL_CHECK_INIT();
		return (self.sfmlEvent.JoyMove.Axis == sf::Joy::AxisPOV and self.hatPovMotion == BOTH_AXIS)? sf::Joy::AxisPOV + 1 : -1;
	}

	float Event::getEventJoystickSecondAxisPosition()
	{
		FGEAL_CHECK_INIT();
		return (self.sfmlEvent.JoyMove.Axis == sf::Joy::AxisPOV and self.hatPovMotion == BOTH_AXIS)? self.hatPovY : 0;
	}

	void Event::implementation::recordJoystickEventHatPovStateIfNeeded()
	{
		// skip if it's not a joystick hat motion event or it's already recorded
		if(sfmlEvent.Type != sf::Event::JoyMoved or isHatPovStateUpdated)
			return;

		// lets use references to attempt to write shorter expressions
		const float& previousValue = JoystickManagement::lastHatsStates[sfmlEvent.JoyMove.JoystickId];
		const float& currentValue = sfmlEvent.JoyMove.Position;

		// identify motion type
		if(currentValue == previousValue)
			hatPovMotion = NONE;
		else switch((int) previousValue)
		{
			default:
			case -1:
				if(currentValue == 0 or currentValue == 180)
					hatPovMotion = POV_Y;
				if(currentValue == 270 or currentValue == 90)
					hatPovMotion = POV_X;
				else
					hatPovMotion = BOTH_AXIS;
				break;

			case 0:
				if(currentValue == -1 or currentValue == 180)
					hatPovMotion = POV_Y;
				if(currentValue == 225 or currentValue == 45)
					hatPovMotion = POV_X;
				else
					hatPovMotion = BOTH_AXIS;
				break;

			case 180:
				if(currentValue == 0 or currentValue == -1)
					hatPovMotion = POV_Y;
				if(currentValue == 315 or currentValue == 135)
					hatPovMotion = POV_X;
				else
					hatPovMotion = BOTH_AXIS;
				break;

			case 270:
				if(currentValue == 225 or currentValue == 315)
					hatPovMotion = POV_Y;
				if(currentValue == -1 or currentValue == 90)
					hatPovMotion = POV_X;
				else
					hatPovMotion = BOTH_AXIS;
				break;

			case 90:
				if(currentValue == 45 or currentValue == 135)
					hatPovMotion = POV_Y;
				if(currentValue == 270 or currentValue == -1)
					hatPovMotion = POV_X;
				else
					hatPovMotion = BOTH_AXIS;
				break;

			case 225:
				if(currentValue == 270 or currentValue == 315)
					hatPovMotion = POV_Y;
				if(currentValue == 0 or currentValue == 45)
					hatPovMotion = POV_X;
				else
					hatPovMotion = BOTH_AXIS;
				break;

			case 45:
				if(currentValue == 90 or currentValue == 135)
					hatPovMotion = POV_Y;
				if(currentValue == 225 or currentValue == 0)
					hatPovMotion = POV_X;
				else
					hatPovMotion = BOTH_AXIS;
				break;

			case 315:
				if(currentValue == 270 or currentValue == 225)
					hatPovMotion = POV_Y;
				if(currentValue == 180 or currentValue == 135)
					hatPovMotion = POV_X;
				else
					hatPovMotion = BOTH_AXIS;
				break;

			case 135:
				if(currentValue == 90 or currentValue == 45)
					hatPovMotion = POV_Y;
				if(currentValue == 315 or currentValue == 180)
					hatPovMotion = POV_X;
				else
					hatPovMotion = BOTH_AXIS;
				break;
		}

		// set current positions X and Y positions
		hatPovX = JoystickManagement::getAxisValueFromHatPov(currentValue, false);
		hatPovY = JoystickManagement::getAxisValueFromHatPov(currentValue, true);

		isHatPovStateUpdated = true;  // remember to do all of this only once
	}

	//******************* EVENTQUEUE

	EventQueue::EventQueue()
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();
		self.cached = false;
	}

	EventQueue::~EventQueue()
	{
		FGEAL_CHECK_INIT();
		delete &self;
	}

	bool EventQueue::isEmpty()
	{
		// hasEvents() already calls checkInit()
		return not hasEvents();
	}

	bool EventQueue::hasEvents()
	{
		FGEAL_CHECK_INIT();
		if(self.cached)
			return true;

		self.cached = true;
		if(window.GetEvent(self.cachedSfmlEvent) == true)
			return true;

		self.cached = false;
		return false;
	}

	Event* EventQueue::getNextEvent()
	{
		FGEAL_CHECK_INIT();
		Event* ev = new Event();
		if(getNextEvent(ev) == true)
			return ev;
		else
		{
			delete ev;
			return null;
		}
	}

	bool EventQueue::getNextEvent(Event* containerEvent)
	{
		FGEAL_CHECK_INIT();
		if(self.cached)
		{
			containerEvent->self.sfmlEvent = self.cachedSfmlEvent;
			containerEvent->self.recordJoystickEventHatPovStateIfNeeded();
			JoystickManagement::recordJoystickEventHatPovStateIfNeeded(containerEvent->self.sfmlEvent);
			self.cached = false;
			return true;
		}
		else
		{
			bool status = window.GetEvent(containerEvent->self.sfmlEvent);
			containerEvent->self.recordJoystickEventHatPovStateIfNeeded();
			JoystickManagement::recordJoystickEventHatPovStateIfNeeded(containerEvent->self.sfmlEvent);
			return status;
		}
	}

	bool EventQueue::skipNextEvent()
	{
		FGEAL_CHECK_INIT();
		if(self.cached)
		{
			JoystickManagement::recordJoystickEventHatPovStateIfNeeded(self.cachedSfmlEvent);
			self.cached = false;
			return true;
		}
		else
		{
			sf::Event tmp;
			return window.GetEvent(tmp);
		}
	}

	void EventQueue::waitNextEvent(Event* containerEvent)
	{
		FGEAL_CHECK_INIT();
		if(not self.cached)
		{
			self.cached = true;
			window.GetEvent(self.cachedSfmlEvent);
		}

		if(containerEvent != null)
		{
			containerEvent->self.sfmlEvent = self.cachedSfmlEvent;
			containerEvent->self.recordJoystickEventHatPovStateIfNeeded();
			self.cached = false;
		}
	}

	//XXX SFML 1.6 EventQueue::flushEvents() implementation is experimental and untested
	void EventQueue::flushEvents()
	{
		FGEAL_CHECK_INIT();

		if(self.cached)
			self.cached = false;

		// this is not safe and may loop indefinitely if events keep comming faster than we can dispose
		for(sf::Event ev; window.GetEvent(ev)==true; /*just continue*/);
	}

	//fixme SFML 1.6 EventQueue::setEventBypassingEnabled() not implemented
	void EventQueue::setEventBypassingEnabled(bool bypass)
	{
		FGEAL_CHECK_INIT();
		core::reportAbsentImplementation("EventQueue::setEventBypassingEnabled is not implemented in this adapter.");
	}
}
