/*
 * sound.cpp
 *
 *  Created on: 23/01/2017
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2017  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/sound.hpp"

#include "fgeal/exceptions.hpp"

using std::string;

namespace fgeal
{
	Sound::Sound(const string& filename)
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();
		sf::SoundBuffer* soundBuffer = new sf::SoundBuffer();
		if(not soundBuffer->LoadFromFile(filename))
		{
			delete soundBuffer; soundBuffer = null;
			throw AdapterException("Could not load audio file \"%s\".", filename.c_str());
		}
		self.sfmlSound.SetBuffer(*soundBuffer);
	}

	Sound::~Sound()
	{
		FGEAL_CHECK_INIT();
		delete self.sfmlSound.GetBuffer();
		delete &self;
	}


	void Sound::play()
	{
		FGEAL_CHECK_INIT();
		self.sfmlSound.SetLoop(false);
		self.sfmlSound.Play();
	}

	void Sound::loop()
	{
		FGEAL_CHECK_INIT();
		self.sfmlSound.SetLoop(true);
		self.sfmlSound.Play();
	}

	void Sound::stop()
	{
		FGEAL_CHECK_INIT();
		self.sfmlSound.Stop();
	}

	void Sound::pause()
	{
		FGEAL_CHECK_INIT();
		self.sfmlSound.Pause();
	}

	void Sound::resume()
	{
		FGEAL_CHECK_INIT();
		//xxx not sure if resumes from pause
		self.sfmlSound.Play();
	}

	bool Sound::isPlaying()
	{
		FGEAL_CHECK_INIT();
		return self.sfmlSound.GetStatus() == sf::Sound::Playing;
	}

	bool Sound::isPaused()
	{
		FGEAL_CHECK_INIT();
		return self.sfmlSound.GetStatus() == sf::Sound::Paused;
	}

	void Sound::setVolume(float value)
	{
		FGEAL_CHECK_INIT();
		self.sfmlSound.SetVolume(value*100);
	}

	float Sound::getVolume()
	{
		FGEAL_CHECK_INIT();
		return self.sfmlSound.GetVolume()*0.01;
	}

	void Sound::setPlaybackSpeed(float factor, bool hintDynamicChange)
	{
		FGEAL_CHECK_INIT();
		self.sfmlSound.SetPitch(factor);
	}

	float Sound::getPlaybackSpeed()
	{
		FGEAL_CHECK_INIT();
		return self.sfmlSound.GetPitch();
	}

	float Sound::getDuration()
	{
		FGEAL_CHECK_INIT();
		return self.sfmlSound.GetBuffer()->GetDuration();
	}

	// ##################################################################################################################

	SoundStream::SoundStream(const string& filename)
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();
		if(not self.sfmlSoundStream.OpenFromFile(filename))
			throw AdapterException("Could not load audio file \"%s\" (streaming mode).", filename.c_str());
	}

	SoundStream::~SoundStream()
	{
		FGEAL_CHECK_INIT();
		delete &self;
	}


	void SoundStream::play()
	{
		FGEAL_CHECK_INIT();
		self.sfmlSoundStream.SetLoop(false);
		self.sfmlSoundStream.Play();
	}

	void SoundStream::loop()
	{
		FGEAL_CHECK_INIT();
		self.sfmlSoundStream.SetLoop(true);
		self.sfmlSoundStream.Play();
	}

	void SoundStream::stop()
	{
		FGEAL_CHECK_INIT();
		self.sfmlSoundStream.Stop();
	}

	void SoundStream::pause()
	{
		FGEAL_CHECK_INIT();
		self.sfmlSoundStream.Pause();
	}

	void SoundStream::resume()
	{
		FGEAL_CHECK_INIT();
		//xxx not sure if resumes from pause
		self.sfmlSoundStream.Play();
	}

	bool SoundStream::isPlaying()
	{
		FGEAL_CHECK_INIT();
		return self.sfmlSoundStream.GetStatus() == sf::Sound::Playing;
	}

	bool SoundStream::isPaused()
	{
		FGEAL_CHECK_INIT();
		return self.sfmlSoundStream.GetStatus() == sf::Sound::Paused;
	}

	void SoundStream::setVolume(float value)
	{
		FGEAL_CHECK_INIT();
		self.sfmlSoundStream.SetVolume(value*100);
	}

	float SoundStream::getVolume()
	{
		FGEAL_CHECK_INIT();
		return self.sfmlSoundStream.GetVolume()*0.01;
	}

	void SoundStream::setPlaybackSpeed(float factor, bool hintDynamicChange)
	{
		FGEAL_CHECK_INIT();
		self.sfmlSoundStream.SetPitch(factor);
	}

	float SoundStream::getPlaybackSpeed()
	{
		FGEAL_CHECK_INIT();
		return self.sfmlSoundStream.GetPitch();
	}

	bool SoundStream::isStreaming()
	{
		FGEAL_CHECK_INIT();
		return true;
	}

	float SoundStream::getDuration()
	{
		FGEAL_CHECK_INIT();
		// we can do this because we know that sfmlSoundStream is a sf::Music instance
		return self.sfmlSoundStream.GetDuration();
	}

	// ##################################################################################################################

	Music::Music(const string& filename)
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();
		if(not self.sfmlMusic.OpenFromFile(filename))
			throw AdapterException("Could not load music file \"%s\".", filename.c_str());
	}

	Music::~Music()
	{
		FGEAL_CHECK_INIT();
		delete &self;
	}

	void Music::play()
	{
		FGEAL_CHECK_INIT();
		self.sfmlMusic.SetLoop(false);
		self.sfmlMusic.Play();
	}

	void Music::loop()
	{
		FGEAL_CHECK_INIT();
		self.sfmlMusic.SetLoop(true);
		self.sfmlMusic.Play();
	}

	void Music::stop()
	{
		FGEAL_CHECK_INIT();
		self.sfmlMusic.Stop();
	}

	void Music::pause()
	{
		FGEAL_CHECK_INIT();
		self.sfmlMusic.Pause();
	}

	void Music::resume()
	{
		FGEAL_CHECK_INIT();
		self.sfmlMusic.Play();
	}

	bool Music::isPlaying()
	{
		FGEAL_CHECK_INIT();
		return self.sfmlMusic.GetStatus() == sf::Music::Playing;
	}

	bool Music::isPaused()
	{
		FGEAL_CHECK_INIT();
		return self.sfmlMusic.GetStatus() == sf::Music::Paused;
	}

	void Music::setVolume(float value)
	{
		FGEAL_CHECK_INIT();
		self.sfmlMusic.SetVolume(value*100);
	}

	float Music::getVolume()
	{
		FGEAL_CHECK_INIT();
		return self.sfmlMusic.GetVolume()*0.01;
	}

	// todo SFML 1.6 supports calculating music length.
}
