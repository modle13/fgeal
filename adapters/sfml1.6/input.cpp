/*
 * input.cpp
 *
 *  Created on: 30/01/2017
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2017  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/input.hpp"

#define window Display::instance->self.sfmlRenderWindow

#define FORWARD_MAPPING(a, b)  case a: return b;
#define BACKWARD_MAPPING(a, b) case b: return a;
#define window Display::instance->self.sfmlRenderWindow

/* Tweaks to attempt to work with SFML versions older than 1.6. Not guaranteed, though.
 * Starting with SFML 1.6, sf::Mouse::Count was renamed to sf::Mouse::ButtonCount */
#ifdef FGEAL_SFML1_6_ENABLE_COMPATIBILITY_PRE_1_6
	#define SFML_MOUSE_BUTTON_COUNT sf::Mouse::Count
	#define SFML_JOYSTICK_AXIS_COUNT sf::Joy::Count
	#define SFML_JOYSTICK_BUTTON_COUNT 16
#else
	#define SFML_MOUSE_BUTTON_COUNT sf::Mouse::ButtonCount
	#define SFML_JOYSTICK_AXIS_COUNT sf::Joy::AxisCount
	#define SFML_JOYSTICK_BUTTON_COUNT sf::Joy::ButtonCount
#endif

namespace fgeal
{
	using std::vector;

	// =======================================================================================
	// Keyboard

	namespace KeyboardKeyMapping
	{
		#define SFML_1_6_KEY_MAPPINGS(mapping)\
		mapping(sf::Key::A, Keyboard::KEY_A)\
		mapping(sf::Key::B, Keyboard::KEY_B)\
		mapping(sf::Key::C, Keyboard::KEY_C)\
		mapping(sf::Key::D, Keyboard::KEY_D)\
		mapping(sf::Key::E, Keyboard::KEY_E)\
		mapping(sf::Key::F, Keyboard::KEY_F)\
		mapping(sf::Key::G, Keyboard::KEY_G)\
		mapping(sf::Key::H, Keyboard::KEY_H)\
		mapping(sf::Key::I, Keyboard::KEY_I)\
		mapping(sf::Key::J, Keyboard::KEY_J)\
		mapping(sf::Key::K, Keyboard::KEY_K)\
		mapping(sf::Key::L, Keyboard::KEY_L)\
		mapping(sf::Key::M, Keyboard::KEY_M)\
		mapping(sf::Key::N, Keyboard::KEY_N)\
		mapping(sf::Key::O, Keyboard::KEY_O)\
		mapping(sf::Key::P, Keyboard::KEY_P)\
		mapping(sf::Key::Q, Keyboard::KEY_Q)\
		mapping(sf::Key::R, Keyboard::KEY_R)\
		mapping(sf::Key::S, Keyboard::KEY_S)\
		mapping(sf::Key::T, Keyboard::KEY_T)\
		mapping(sf::Key::U, Keyboard::KEY_U)\
		mapping(sf::Key::V, Keyboard::KEY_V)\
		mapping(sf::Key::W, Keyboard::KEY_W)\
		mapping(sf::Key::X, Keyboard::KEY_X)\
		mapping(sf::Key::Y, Keyboard::KEY_Y)\
		mapping(sf::Key::Z, Keyboard::KEY_Z)\
		\
		mapping(sf::Key::Num0, Keyboard::KEY_0)\
		mapping(sf::Key::Num1, Keyboard::KEY_1)\
		mapping(sf::Key::Num2, Keyboard::KEY_2)\
		mapping(sf::Key::Num3, Keyboard::KEY_3)\
		mapping(sf::Key::Num4, Keyboard::KEY_4)\
		mapping(sf::Key::Num5, Keyboard::KEY_5)\
		mapping(sf::Key::Num6, Keyboard::KEY_6)\
		mapping(sf::Key::Num7, Keyboard::KEY_7)\
		mapping(sf::Key::Num8, Keyboard::KEY_8)\
		mapping(sf::Key::Num9, Keyboard::KEY_9)\
		\
		mapping(sf::Key::F1, Keyboard::KEY_F1)\
		mapping(sf::Key::F2, Keyboard::KEY_F2)\
		mapping(sf::Key::F3, Keyboard::KEY_F3)\
		mapping(sf::Key::F4, Keyboard::KEY_F4)\
		mapping(sf::Key::F5, Keyboard::KEY_F5)\
		mapping(sf::Key::F6, Keyboard::KEY_F6)\
		mapping(sf::Key::F7, Keyboard::KEY_F7)\
		mapping(sf::Key::F8, Keyboard::KEY_F8)\
		mapping(sf::Key::F9, Keyboard::KEY_F9)\
		mapping(sf::Key::F10, Keyboard::KEY_F10)\
		mapping(sf::Key::F11, Keyboard::KEY_F11)\
		mapping(sf::Key::F12, Keyboard::KEY_F12)\
		\
		mapping(sf::Key::Up,    Keyboard::KEY_ARROW_UP)\
		mapping(sf::Key::Down,  Keyboard::KEY_ARROW_DOWN)\
		mapping(sf::Key::Left,  Keyboard::KEY_ARROW_LEFT)\
		mapping(sf::Key::Right, Keyboard::KEY_ARROW_RIGHT)\
		\
		mapping(sf::Key::Return, Keyboard::KEY_ENTER)\
		mapping(sf::Key::Space,  Keyboard::KEY_SPACE)\
		mapping(sf::Key::Escape, Keyboard::KEY_ESCAPE)\
		mapping(sf::Key::LControl,  Keyboard::KEY_LEFT_CONTROL)\
		mapping(sf::Key::RControl,  Keyboard::KEY_RIGHT_CONTROL)\
		mapping(sf::Key::LShift,    Keyboard::KEY_LEFT_SHIFT)\
		mapping(sf::Key::RShift,    Keyboard::KEY_RIGHT_SHIFT)\
		mapping(sf::Key::LAlt,      Keyboard::KEY_LEFT_ALT)\
		mapping(sf::Key::RAlt,      Keyboard::KEY_RIGHT_ALT)\
		mapping(sf::Key::LSystem,   Keyboard::KEY_LEFT_SUPER)\
		mapping(sf::Key::RSystem,   Keyboard::KEY_RIGHT_SUPER)\
		mapping(sf::Key::Menu,      Keyboard::KEY_MENU)\
		mapping(sf::Key::Tab,       Keyboard::KEY_TAB)\
		mapping(sf::Key::Back, Keyboard::KEY_BACKSPACE)\
		\
		mapping(sf::Key::Dash,      Keyboard::KEY_MINUS)\
		mapping(sf::Key::Equal,     Keyboard::KEY_EQUALS)\
		mapping(sf::Key::LBracket,  Keyboard::KEY_LEFT_BRACKET)\
		mapping(sf::Key::RBracket,  Keyboard::KEY_RIGHT_BRACKET)\
		mapping(sf::Key::SemiColon, Keyboard::KEY_SEMICOLON)\
		mapping(sf::Key::Comma,     Keyboard::KEY_COMMA)\
		mapping(sf::Key::Period,    Keyboard::KEY_PERIOD)\
		mapping(sf::Key::Slash,     Keyboard::KEY_SLASH)\
		mapping(sf::Key::BackSlash, Keyboard::KEY_BACKSLASH)\
		mapping(sf::Key::Quote,     Keyboard::KEY_QUOTE)\
		mapping(sf::Key::Tilde,     Keyboard::KEY_TILDE)\
		\
		mapping(sf::Key::Insert,   Keyboard::KEY_INSERT)\
		mapping(sf::Key::Delete,   Keyboard::KEY_DELETE)\
		mapping(sf::Key::Home,     Keyboard::KEY_HOME)\
		mapping(sf::Key::End,      Keyboard::KEY_END)\
		mapping(sf::Key::PageUp,   Keyboard::KEY_PAGE_UP)\
		mapping(sf::Key::PageDown, Keyboard::KEY_PAGE_DOWN)\
		\
		mapping(sf::Key::Numpad0, Keyboard::KEY_NUMPAD_0)\
		mapping(sf::Key::Numpad1, Keyboard::KEY_NUMPAD_1)\
		mapping(sf::Key::Numpad2, Keyboard::KEY_NUMPAD_2)\
		mapping(sf::Key::Numpad3, Keyboard::KEY_NUMPAD_3)\
		mapping(sf::Key::Numpad4, Keyboard::KEY_NUMPAD_4)\
		mapping(sf::Key::Numpad5, Keyboard::KEY_NUMPAD_5)\
		mapping(sf::Key::Numpad6, Keyboard::KEY_NUMPAD_6)\
		mapping(sf::Key::Numpad7, Keyboard::KEY_NUMPAD_7)\
		mapping(sf::Key::Numpad8, Keyboard::KEY_NUMPAD_8)\
		mapping(sf::Key::Numpad9, Keyboard::KEY_NUMPAD_9)\
		\
/*      fixme SFML cant reference extra numpad keys directly. Instead, it passes a normal non-numpad version of the key.
		mapping(sf::Key::???, Keyboard::KEY_NUMPAD_ADDITION)\
		mapping(sf::Key::???, Keyboard::KEY_NUMPAD_SUBTRACTION)\
		mapping(sf::Key::???, Keyboard::KEY_NUMPAD_MULTIPLICATION)\
		mapping(sf::Key::???, Keyboard::KEY_NUMPAD_DIVISION)\
		mapping(sf::Key::???, Keyboard::KEY_NUMPAD_DECIMAL)\
		mapping(sf::Key::???, Keyboard::KEY_NUMPAD_ENTER)\ */
		//end

		// Returns the fgeal-equivalent key enum of the given SFML 1.6 keycode.
		Keyboard::Key toGenericKey(sf::Key::Code sfmlKeycode)
		{
			switch(sfmlKeycode)
			{
				SFML_1_6_KEY_MAPPINGS(FORWARD_MAPPING);
				default: return Keyboard::KEY_UNKNOWN;
			}
		}

		// Returns the SFML 1.6 keycode of the given fgeal-equivalent key enum.
		sf::Key::Code toUnderlyingLibraryKey(Keyboard::Key key)
		{
			switch(key)
			{
				SFML_1_6_KEY_MAPPINGS(BACKWARD_MAPPING);
				default: return sf::Key::Count;
			}
		}
	}

	// API functions

	bool Keyboard::isKeyPressed(Keyboard::Key key)
	{
		FGEAL_CHECK_INIT();
		return window.GetInput().IsKeyDown(KeyboardKeyMapping::toUnderlyingLibraryKey(key));
	}

	// =======================================================================================
	// Mouse

	namespace MouseButtonMapping
	{
		#define SFML_1_6_MOUSE_BUTTON_MAPPINGS(mapping)\
		mapping(sf::Mouse::Left,   Mouse::BUTTON_LEFT)\
		mapping(sf::Mouse::Middle, Mouse::BUTTON_MIDDLE)\
		mapping(sf::Mouse::Right,  Mouse::BUTTON_RIGHT)\
		//end

		// Returns the fgeal-equivalent mouse button enum of the given SFML 1.6 mouse button number.
		Mouse::Button toGenericMouseButton(sf::Mouse::Button sfmlMouseButton)
		{
			switch(sfmlMouseButton)
			{
				SFML_1_6_MOUSE_BUTTON_MAPPINGS(FORWARD_MAPPING);
				default: return Mouse::BUTTON_UNKNOWN;
			}
		}

		// Returns the SFML 1.6 mouse button number of the fgeal-equivalent mouse button enum.
		sf::Mouse::Button toUnderlyingLibraryMouseButton(Mouse::Button button)
		{
			switch(button)
			{
				SFML_1_6_MOUSE_BUTTON_MAPPINGS(BACKWARD_MAPPING);
				default: return SFML_MOUSE_BUTTON_COUNT;
			}
		}
	}

	bool Mouse::isButtonPressed(Mouse::Button btn)
	{
		FGEAL_CHECK_INIT();
		return window.GetInput().IsMouseButtonDown(MouseButtonMapping::toUnderlyingLibraryMouseButton(btn));
	}

	Point Mouse::getPosition()
	{
		FGEAL_CHECK_INIT();
		Point pt = {(float) window.GetInput().GetMouseX(), (float) window.GetInput().GetMouseY()};
		return pt;
	}

	void Mouse::setPosition(const Point& position)
	{
		FGEAL_CHECK_INIT();
		Mouse::setPosition(position.x, position.y);
	}

	int Mouse::getPositionX()
	{
		FGEAL_CHECK_INIT();
		return window.GetInput().GetMouseX();
	}

	int Mouse::getPositionY()
	{
		FGEAL_CHECK_INIT();
		return window.GetInput().GetMouseY();
	}

	void Mouse::setPosition(int x, int y)
	{
		FGEAL_CHECK_INIT();
		window.SetCursorPosition(x, y);
	}

	// =======================================================================================
	// Joystick

	vector<float> JoystickManagement::lastHatsStates;

	float JoystickManagement::getAxisValueFromHatPov(float value, bool isPovY)
	{
		return value < 0? 0
				: isPovY?  (value == 90 or value == 270? 0 : value < 90 or value > 270? -1 : 1)
				: /*PovX*/ (value == 0  or value == 180? 0 : value > 180?               -1 : 1 );
	}

	void JoystickManagement::recordJoystickEventHatPovStateIfNeeded(sf::Event& sfmlEvent)
	{
		// skip if it's not a joystick hat motion event
		if(sfmlEvent.Type == sf::Event::JoyMoved)
			JoystickManagement::lastHatsStates[sfmlEvent.JoyMove.JoystickId] = sfmlEvent.JoyMove.Position;
	}

	// nothing is needed
	void Joystick::configureAll()
	{
		JoystickManagement::lastHatsStates.resize(sf::Joy::Count, -1);
	}

	void Joystick::releaseAll()
	{
		JoystickManagement::lastHatsStates.clear();
	}

	unsigned Joystick::getCount()
	{
		FGEAL_CHECK_INIT();
		return sf::Joy::Count;  //xxx SFML 1.6 adapter: there is no way to get the actual number of joysticks
	}

	std::string Joystick::getJoystickName(unsigned joystickIndex)
	{
		core::reportAbsentImplementation("Getting joystick name is not supported on this adapter.");
		return std::string();  //xxx SFML 1.6 adapter: there is no way to get a joystick's name
	}

	unsigned Joystick::getButtonCount(unsigned joystickIndex)
	{
		return SFML_JOYSTICK_BUTTON_COUNT;  //xxx SFML 1.6 adapter: there is no way to get the actual number of buttons of a joystick
	}

	unsigned Joystick::getAxisCount(unsigned joystickIndex)
	{
		return SFML_JOYSTICK_AXIS_COUNT+1;  //xxx SFML 1.6 adapter: there is no way to get the actual number of axis of a joystick
	}

	bool Joystick::isButtonPressed(unsigned joystickIndex, unsigned buttonIndex)
	{
		return window.GetInput().IsJoystickButtonDown(joystickIndex, buttonIndex);
	}

	float Joystick::getAxisPosition(unsigned joystickIndex, unsigned axisIndex)
	{
		const sf::Joy::Axis sfmlAxis = static_cast<sf::Joy::Axis>(axisIndex < SFML_JOYSTICK_AXIS_COUNT? axisIndex : SFML_JOYSTICK_AXIS_COUNT-1);
		if(sfmlAxis!=sf::Joy::AxisPOV)
			return window.GetInput().GetJoystickAxis(joystickIndex, sfmlAxis)/100.0f;
		else
		{
			const bool isPovY = (axisIndex == SFML_JOYSTICK_AXIS_COUNT);
			const float value = window.GetInput().GetJoystickAxis(joystickIndex, sfmlAxis);
			return JoystickManagement::getAxisValueFromHatPov(value, isPovY);
		}
	}
}
