/*
 * graphics.cpp
 *
 *  Created on: 21/06/2018
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2016  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/graphics.hpp"

#include "SDL/SDL_gfxPrimitives.h"

using std::vector;

// Useful macro to improve readability and reduce typing
#define asQuad(a1, a2, a3, a4) {static_cast<Sint16>(a1), static_cast<Sint16>(a2), static_cast<Sint16>(a3), static_cast<Sint16>(a4)}

#if SDL_GFXPRIMITIVES_MAJOR < 2 or SDL_GFXPRIMITIVES_MINOR == 0
	#if SDL_GFXPRIMITIVES_MICRO < 17
		// not sure if arcRGBA was created at this version, but changelog suggests that arcColor at least was
		#define FGEAL_SDL1_2_GFX_MISSING_ARC_PRIMITIVE
	#endif

	#if SDL_GFXPRIMITIVES_MICRO < 22
		#define FGEAL_SDL1_2_GFX_MISSING_ROUNDED_RECTANGLE_PRIMITIVE
	#endif
#endif

#ifdef FGEAL_SDL1_2_USE_SMOOTH_PRIMITIVES
	#define lineRGBA aalineRGBA
	#define circleRGBA aacircleRGBA
	#define ellipseRGBA aaellipseRGBA
	#define trigonRGBA aatrigonRGBA
	#define polygonRGBA aapolygonRGBA
#endif

namespace fgeal
{
	/// extern-expected on implementation.hpp
	SDL_Surface* drawTargetSurface = null;

	// static
	void Graphics::setDrawTarget(const Image* image)
	{
		FGEAL_CHECK_INIT();
		if(image == null)
			fgeal::drawTargetSurface = Display::instance->self.sdlDisplaySurface;
		else
			fgeal::drawTargetSurface = image->self.sdlSurface;
	}

	// static
	void Graphics::setDrawTarget(const Display& display)
	{
		FGEAL_CHECK_INIT();
		if(&display == null)
			fgeal::drawTargetSurface = Display::instance->self.sdlDisplaySurface;
		else
			fgeal::drawTargetSurface = display.self.sdlDisplaySurface;
	}

	// static
	void Graphics::setDefaultDrawTarget()
	{
		FGEAL_CHECK_INIT();
		fgeal::drawTargetSurface = Display::instance->self.sdlDisplaySurface;
	}

	// =====================================================================================================================================================
	// Primitives
	// xxx SDL_gfx has anti-aliased versions of the primitive functions but we have no means to choose to use them throught the current fgeal interface. Currently it is only accessible with compile-time macro flag.

	void Graphics::drawLine(float x1, float y1, float x2, float y2, const Color& c)
	{
		FGEAL_CHECK_INIT();
		lineRGBA(fgeal::drawTargetSurface, x1, y1, x2, y2, c.r, c.g, c.b, c.a);
	}

	void Graphics::drawRectangle(float x, float y, float width, float height, const Color& c)
	{
		FGEAL_CHECK_INIT();
		rectangleRGBA(fgeal::drawTargetSurface, x, y, x+width, y+height, c.r, c.g, c.b, c.a);
	}

	void Graphics::drawFilledRectangle(float x, float y, float width, float height, const Color& c)
	{
		FGEAL_CHECK_INIT();
		boxRGBA(fgeal::drawTargetSurface, x, y, x+width, y+height, c.r, c.g, c.b, c.a);
	}

	void Graphics::drawCircle(float cx, float cy, float r, const Color& c)
	{
		FGEAL_CHECK_INIT();
		circleRGBA(fgeal::drawTargetSurface, cx, cy, r, c.r, c.g, c.b, c.a);
	}

	void Graphics::drawFilledCircle(float cx, float cy, float r, const Color& c)
	{
		FGEAL_CHECK_INIT();
		filledCircleRGBA(fgeal::drawTargetSurface, cx, cy, r, c.r, c.g, c.b, c.a);
	}

	void Graphics::drawEllipse(float cx, float cy, float rx, float ry, const Color& c)
	{
		FGEAL_CHECK_INIT();
		ellipseRGBA(fgeal::drawTargetSurface, cx, cy, rx, ry, c.r, c.g, c.b, c.a);
	}

	void Graphics::drawFilledEllipse(float cx, float cy, float rx, float ry, const Color& c)
	{
		FGEAL_CHECK_INIT();
		filledEllipseRGBA(fgeal::drawTargetSurface, cx, cy, rx, ry, c.r, c.g, c.b, c.a);
	}

	void Graphics::drawTriangle(float x1, float y1, float x2, float y2, float x3, float y3, const Color& c)
	{
		FGEAL_CHECK_INIT();
		trigonRGBA(fgeal::drawTargetSurface, x1, y1, x2, y2, x3, y3, c.r, c.g, c.b, c.a);
	}

	void Graphics::drawFilledTriangle(float x1, float y1, float x2, float y2, float x3, float y3, const Color& c)
	{
		FGEAL_CHECK_INIT();
		filledTrigonRGBA(fgeal::drawTargetSurface, x1, y1, x2, y2, x3, y3, c.r, c.g, c.b, c.a);
	}

	void Graphics::drawQuadrangle(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4, const Color& c)
	{
		FGEAL_CHECK_INIT();
		const Sint16 verticesX[4] = asQuad(x1, x2, x3, x4), verticesY[4] = asQuad(y1, y2, y3, y4);
		polygonRGBA(fgeal::drawTargetSurface, verticesX, verticesY, 4, c.r, c.g, c.b, c.a);
	}

	void Graphics::drawFilledQuadrangle(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4, const Color& c)
	{
		FGEAL_CHECK_INIT();
		const Sint16 verticesX[4] = asQuad(x1, x2, x3, x4), verticesY[4] = asQuad(y1, y2, y3, y4);
		filledPolygonRGBA(fgeal::drawTargetSurface, verticesX, verticesY, 4, c.r, c.g, c.b, c.a);
	}

	void Graphics::drawArc(float cx, float cy, float r, float ai, float af, const Color& c)
	{
		FGEAL_CHECK_INIT();
		#ifdef FGEAL_SDL1_2_GFX_MISSING_ARC_PRIMITIVE
			const unsigned arcSegmentCount = r*(af-ai)*0.1, arcVertexCount = arcSegmentCount+1;
			const float da = (af-ai)/arcSegmentCount;
			for(unsigned i = 0; i < arcVertexCount-1; i++)
			{
				const float x1 = cx + r*sin(M_PI_2 + ai + da*i),
							y1 = cy + r*cos(M_PI_2 + ai + da*i),
							x2 = cx + r*sin(M_PI_2 + ai + da*(i+1)),
							y2 = cy + r*cos(M_PI_2 + ai + da*(i+1));

				lineRGBA(fgeal::drawTargetSurface, x1, y1, x2, y2, c.r, c.g, c.b, c.a);
			}
		#else
			arcRGBA(fgeal::drawTargetSurface, cx, cy, r, rad2deg(-af), rad2deg(-ai), c.r, c.g, c.b, c.a);
		#endif
	}

	void Graphics::drawCircleSector(float cx, float cy, float r, float ai, float af, const Color& c)
	{
		FGEAL_CHECK_INIT();
		pieRGBA(fgeal::drawTargetSurface, cx, cy, r, rad2deg(-af), rad2deg(-ai), c.r, c.g, c.b, c.a);
	}

	void Graphics::drawFilledCircleSector(float cx, float cy, float r, float ai, float af, const Color& c)
	{
		FGEAL_CHECK_INIT();
		filledPieRGBA(fgeal::drawTargetSurface, cx, cy, r, rad2deg(-af), rad2deg(-ai), c.r, c.g, c.b, c.a);
	}

	void Graphics::drawRoundedRectangle(float x, float y, float width, float height, float rd, const Color& c)
	{
		FGEAL_CHECK_INIT();
		#ifdef FGEAL_SDL1_2_GFX_MISSING_ROUNDED_RECTANGLE_PRIMITIVE
			Graphics::genericDrawRoundedRectangle(x, y, width, height, rd, c);
		#else
			roundedRectangleRGBA(fgeal::drawTargetSurface, x, y, x+width, y+height, rd, c.r, c.g, c.b, c.a);
		#endif
	}

	void Graphics::drawFilledRoundedRectangle(float x, float y, float width, float height, float rd, const Color& c)
	{
		FGEAL_CHECK_INIT();
		#ifdef FGEAL_SDL1_2_GFX_MISSING_ROUNDED_RECTANGLE_PRIMITIVE
			Graphics::genericDrawFilledRoundedRectangle(x, y, width, height, rd, c);
		#else
			roundedBoxRGBA(fgeal::drawTargetSurface, x, y, x+width, y+height, rd, c.r, c.g, c.b, c.a);
		#endif
	}

	void Graphics::drawPolygon(const vector<float>& x, const vector<float>& y, const Color& c)
	{
		FGEAL_CHECK_INIT();
		const unsigned n = std::min(x.size(), y.size());
		vector<Sint16> verticesX(n), verticesY(n);
		for(unsigned i = 0; i < n; i++) { verticesX[i] = x[i]; verticesY[i] = y[i]; }
		polygonRGBA(fgeal::drawTargetSurface, &verticesX[0], &verticesY[0], n, c.r, c.g, c.b, c.a);
	}

	void Graphics::drawFilledPolygon(const vector<float>& x, const vector<float>& y, const Color& c)
	{
		FGEAL_CHECK_INIT();
		const unsigned n = std::min(x.size(), y.size());
		vector<Sint16> verticesX(n), verticesY(n);
		for(unsigned i = 0; i < n; i++) { verticesX[i] = x[i]; verticesY[i] = y[i]; }
		filledPolygonRGBA(fgeal::drawTargetSurface, &verticesX[0], &verticesY[0], n, c.r, c.g, c.b, c.a);
	}
}
