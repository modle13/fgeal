/*
 * display.cpp
 *
 *  Created on: 24/10/2016
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2016  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/display.hpp"

#include "fgeal/exceptions.hpp"

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <cstdio>

// case for old SDL versions
#ifndef SDL_putenv
	#define SDL_putenv putenv
#endif

using std::string;

namespace fgeal
{
	// automagically sets SDL environment variables to pre-set window position
	static void setupPositionEnvVars(Display::Options::Positioning positioning, const Point& pos = Point());

	Display::Display(const Options& options)
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();

		Uint32 flags = SDL_DOUBLEBUF | SDL_HWSURFACE | SDL_HWACCEL;
		if(options.fullscreen) flags |= SDL_FULLSCREEN;
		else
		{
			setupPositionEnvVars(options.positioning, options.position);

			if(options.isUserResizeable)
				flags |= SDL_RESIZABLE;
		}

		if(not options.iconFilename.empty())
		{
			SDL_Surface* iconSurface = IMG_Load(options.iconFilename.c_str());
			if(iconSurface == null)
				throw AdapterException("Error while trying to set display icon. Could not load image \"%s\": %s", options.iconFilename.c_str(), IMG_GetError());

			SDL_WM_SetIcon(iconSurface,  null);
			SDL_FreeSurface(iconSurface);
			iconSurface = null;
		}

		self.sdlDisplaySurface = SDL_SetVideoMode(options.width, options.height, 32, flags);

		if(self.sdlDisplaySurface == null)
			throw AdapterException("Could not create display! Error: %s", SDL_GetError());

		fgeal::drawTargetSurface = self.sdlDisplaySurface;

		SDL_WM_SetCaption(options.title.c_str(), options.title.c_str());
	}

	Display::~Display()
	{
		FGEAL_CHECK_INIT();
		SDL_FreeSurface(self.sdlDisplaySurface);
		delete &self;
	}

	void Display::refresh()
	{
		FGEAL_CHECK_INIT();
		//flip surface if double-buffered, update rect if single-buffered instead
		if( SDL_Flip(self.sdlDisplaySurface) == -1 )
			throw AdapterException("Failed to swap the buffers/refresh the display: %s", SDL_GetError());
	}

	void Display::clear()
	{
		FGEAL_CHECK_INIT();
		if ( SDL_FillRect(self.sdlDisplaySurface, null, 0) == -1 )
			throw AdapterException("Error while trying to clear displau: SDL_FillRect error! %s", SDL_GetError());
	}

	unsigned Display::getWidth()
	{
		FGEAL_CHECK_INIT();
		return self.sdlDisplaySurface->w;
	}

	unsigned Display::getHeight()
	{
		FGEAL_CHECK_INIT();
		return self.sdlDisplaySurface->h;
	}

	void Display::setSize(unsigned width, unsigned height)
	{
		FGEAL_CHECK_INIT();
		self.sdlDisplaySurface = SDL_SetVideoMode(width, height,
												  self.sdlDisplaySurface->format->BitsPerPixel,
												  self.sdlDisplaySurface->flags);
		if(self.sdlDisplaySurface == null)
			throw AdapterException("Could not resize display! Error: %s", SDL_GetError());
	}

	string Display::getTitle() const
	{
		FGEAL_CHECK_INIT();
		char* tmp;
		SDL_WM_GetCaption(&tmp, null);
		return string(tmp);
	}

	void Display::setTitle(const string& title)
	{
		FGEAL_CHECK_INIT();
		SDL_WM_SetCaption(title.c_str(), title.c_str());
	}

	void Display::setIcon(const std::string& iconFilename)
	{
		FGEAL_CHECK_INIT();
		core::reportAbsentImplementation("Setting the display's icon after creating a display (any one) is not supported on this adapter.");
	}

	bool Display::isFullscreen()
	{
		FGEAL_CHECK_INIT();
		return self.sdlDisplaySurface->flags & SDL_FULLSCREEN;
	}

	void Display::setFullscreen(bool choice)
	{
		if(isFullscreen() == choice)  // the call to isFullscreen already calls checkInit
			return;  // if same mode, nothing needs to be done

		self.sdlDisplaySurface = SDL_SetVideoMode(self.sdlDisplaySurface->w, self.sdlDisplaySurface->h,
												  self.sdlDisplaySurface->format->BitsPerPixel,
												  self.sdlDisplaySurface->flags ^ SDL_FULLSCREEN);
		if(self.sdlDisplaySurface == null)
			throw AdapterException("Could not change display mode! Error: %s", SDL_GetError());
	}

	void Display::setPosition(const Point& pos)
	{
		FGEAL_CHECK_INIT();
		setupPositionEnvVars(Options::POSITION_DEFINED, pos);  // needs to reset video mode to make it appear on the new position
		self.sdlDisplaySurface = SDL_SetVideoMode(self.sdlDisplaySurface->w, self.sdlDisplaySurface->h,
												  self.sdlDisplaySurface->format->BitsPerPixel,
												  self.sdlDisplaySurface->flags);
		if(self.sdlDisplaySurface == null)
			throw AdapterException("Could not set display position! Error: %s", SDL_GetError());
	}

	void Display::setPositionOnCenter()
	{
		FGEAL_CHECK_INIT();
		setupPositionEnvVars(Options::POSITION_CENTERED);  // needs to reset video mode to make it appear on the new position
		self.sdlDisplaySurface = SDL_SetVideoMode(self.sdlDisplaySurface->w, self.sdlDisplaySurface->h,
												  self.sdlDisplaySurface->format->BitsPerPixel,
												  self.sdlDisplaySurface->flags);
		if(self.sdlDisplaySurface == null)
			throw AdapterException("Could not set display position! Error: %s", SDL_GetError());
	}

	void Display::setMouseCursorVisible(bool enable)
	{
		FGEAL_CHECK_INIT();
		SDL_ShowCursor(enable? SDL_ENABLE : SDL_DISABLE);
	}

	// ----------------------------------------------------------------------------------------------------------------------------------------------------
	void setupPositionEnvVars(Display::Options::Positioning positioning, const Point& pos)
	{
		// represents the SDL_VIDEO_CENTERED and SDL_VIDEO_WINDOW_POS environment variables; altering this arrays affects the variables even after putenv.
		static char cStrEnvVarSdlVideoCentered[32], cStrEnvVarSdlVideoWindowPos[128];

		if(positioning == Display::Options::POSITION_CENTERED)
		{
			// sets SDL_VIDEO_CENTERED as "center"
			sprintf(cStrEnvVarSdlVideoCentered, "SDL_VIDEO_CENTERED=center");
			SDL_putenv(cStrEnvVarSdlVideoCentered);

			// resets SDL_VIDEO_WINDOW_POS
			strcpy(cStrEnvVarSdlVideoWindowPos, "");
		}

		else if(positioning == Display::Options::POSITION_DEFINED)
		{
			// resets SDL_VIDEO_CENTERED
			strcpy(cStrEnvVarSdlVideoCentered, "");

			// sets SDL_VIDEO_WINDOW_POS coordinates
			sprintf(cStrEnvVarSdlVideoWindowPos, "SDL_VIDEO_WINDOW_POS=%d,%d", (int) pos.x, (int) pos.y);
			SDL_putenv(cStrEnvVarSdlVideoWindowPos);
		}

		else if(positioning == Display::Options::POSITION_UNDEFINED)
		{
			// resets SDL_VIDEO_CENTERED and SDL_VIDEO_WINDOW_POS
			strcpy(cStrEnvVarSdlVideoCentered, "");
			strcpy(cStrEnvVarSdlVideoWindowPos, "");
		}
	}
}
