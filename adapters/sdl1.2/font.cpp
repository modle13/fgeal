/*
 * font.cpp
 *
 *  Created on: 24/10/2016
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2016  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/font.hpp"

#include "fgeal/exceptions.hpp"

#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>

#define useAA self.isAntialiased

using std::string;

namespace fgeal
{
	static SDL_Color toSDLColor(const Color& c)
	{
		SDL_Color sdlc;
		sdlc.r = c.r;
		sdlc.g = c.g;
		sdlc.b = c.b;
		return sdlc;
	}

	void setSurfaceAlpha (SDL_Surface *surface, Uint8 alpha)
	{
	    SDL_PixelFormat* fmt = surface->format;

	    // If surface has no alpha channel, just set the surface alpha.
	    if( fmt->Amask == 0 ) {
	        SDL_SetAlpha( surface, SDL_SRCALPHA, alpha );
	    }
	    // Else change the alpha of each pixel.
	    else {
	        unsigned bpp = fmt->BytesPerPixel;
	        // Scaling factor to clamp alpha to [0, alpha].
	        float scale = alpha / 255.0f;

	        SDL_LockSurface(surface);

	        for (int y = 0; y < surface->h; ++y)
	        for (int x = 0; x < surface->w; ++x) {
	            // Get a pointer to the current pixel.
	            Uint32* pixel_ptr = (Uint32 *)(
	                    (Uint8 *)surface->pixels
	                    + y * surface->pitch
	                    + x * bpp
	                    );

	            // Get the old pixel components.
	            Uint8 r, g, b, a;
	            SDL_GetRGBA( *pixel_ptr, fmt, &r, &g, &b, &a );

	            // Set the pixel with the new alpha.
	            *pixel_ptr = SDL_MapRGBA( fmt, r, g, b, scale * a );
	        }

	        SDL_UnlockSurface(surface);
	    }
	}

	// ===========================================================================================================

	Font::Font(const string& filename, int size, bool antialiasing)
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();

		self.sdlttfFont = TTF_OpenFont(filename.c_str(), size);

		if(self.sdlttfFont == null)
			throw AdapterException("Font %s could not be loaded: %s", filename.c_str(), TTF_GetError());

		self.isAntialiased = antialiasing;
		self.cachedSurface = null;
	}

	Font::~Font()
	{
		FGEAL_CHECK_INIT();
		TTF_CloseFont(self.sdlttfFont);
		if(self.cachedSurface != null) SDL_FreeSurface(self.cachedSurface);
		delete &self;
	}

	void Font::drawText(const string& text, float x, float y, Color color)
	{
		FGEAL_CHECK_INIT();

		// "cache miss", need to re-render ttf
		if(text != self.cachedText or color != self.cachedColor)
		{
			SDL_Surface* renderedTextSurface = (useAA? TTF_RenderText_Blended : TTF_RenderText_Solid)(self.sdlttfFont, text.c_str(), toSDLColor(color));

			if(renderedTextSurface == null)
				throw AdapterException("Failed to render text from font: %s \n Failed when trying to draw \"%s\"", TTF_GetError() ,text.c_str());

			// force alpha on text since SDL_ttf ignores SDL_Color's 'a' field.
			if(color.a != 255)
				setSurfaceAlpha(renderedTextSurface, color.a);

			// cleanup if there is a old cached surface
			if(self.cachedSurface != null)
				SDL_FreeSurface(self.cachedSurface);

			// keep this as cache
			self.cachedSurface = renderedTextSurface;
			self.cachedText = text;
			self.cachedColor = color;
		}

		SDL_Rect dstrect = {(Sint16) x, (Sint16) y};

		int blitStatus =
		SDL_BlitSurface(self.cachedSurface, null, fgeal::drawTargetSurface, &dstrect);

		if(blitStatus != 0)
			throw AdapterException("Failed to draw text: %s \n Failed when trying to draw \"%s\"", SDL_GetError() ,text.c_str());
	}

	float Font::getHeight() const
	{
		FGEAL_CHECK_INIT();
		return TTF_FontHeight(self.sdlttfFont);
	}

	float Font::getTextWidth(const std::string& text) const
	{
		FGEAL_CHECK_INIT();
		int width, queryStatus =
		TTF_SizeText(self.sdlttfFont, text.c_str(), &width, null);

		if(queryStatus != 0)
			throw AdapterException("Font width could not be obtained: %s", TTF_GetError());

		return width;
	}
}
