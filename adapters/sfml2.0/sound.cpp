/*
 * sound.cpp
 *
 *  Created on: 30/01/2017
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2017  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/sound.hpp"

#include "fgeal/exceptions.hpp"

using std::string;

namespace fgeal
{
	Sound::Sound(const string& filename)
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();
		sf::SoundBuffer* soundBuffer = new sf::SoundBuffer();
		if(not soundBuffer->loadFromFile(filename))
		{
			delete soundBuffer; soundBuffer = null;
			throw AdapterException("Could not load audio file \"%s\".", filename.c_str());
		}
		self.sfmlSound.setBuffer(*soundBuffer);
	}

	Sound::~Sound()
	{
		FGEAL_CHECK_INIT();
		delete self.sfmlSound.getBuffer();
		delete &self;
	}

	void Sound::play()
	{
		FGEAL_CHECK_INIT();
		self.sfmlSound.setLoop(false);
		self.sfmlSound.play();
	}

	void Sound::loop()
	{
		FGEAL_CHECK_INIT();
		self.sfmlSound.setLoop(true);
		self.sfmlSound.play();
	}

	void Sound::stop()
	{
		FGEAL_CHECK_INIT();
		self.sfmlSound.stop();
	}

	void Sound::pause()
	{
		FGEAL_CHECK_INIT();
		self.sfmlSound.pause();
	}

	void Sound::resume()
	{
		FGEAL_CHECK_INIT();
		//xxx not sure if resumes from pause
		self.sfmlSound.play();
	}

	bool Sound::isPlaying()
	{
		FGEAL_CHECK_INIT();
		return self.sfmlSound.getStatus() == sf::Sound::Playing;
	}

	bool Sound::isPaused()
	{
		FGEAL_CHECK_INIT();
		return self.sfmlSound.getStatus() == sf::Sound::Paused;
	}

	void Sound::setVolume(float value)
	{
		FGEAL_CHECK_INIT();
		self.sfmlSound.setVolume(value*100);
	}

	float Sound::getVolume()
	{
		FGEAL_CHECK_INIT();
		return self.sfmlSound.getVolume()*0.01;
	}

	void Sound::setPlaybackSpeed(float factor, bool hintDynamicChange)
	{
		FGEAL_CHECK_INIT();
		self.sfmlSound.setPitch(factor);
	}

	float Sound::getPlaybackSpeed()
	{
		FGEAL_CHECK_INIT();
		return self.sfmlSound.getPitch();
	}

	float Sound::getDuration()
	{
		FGEAL_CHECK_INIT();
		return self.sfmlSound.getBuffer()->getDuration().asSeconds();
	}

	// ##################################################################################################################

	SoundStream::SoundStream(const string& filename)
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();
		if(not self.sfmlSoundStream.openFromFile(filename))
			throw AdapterException("Could not load audio file \"%s\". (stream mode)", filename.c_str());
	}

	SoundStream::~SoundStream()
	{
		FGEAL_CHECK_INIT();
		delete &self;
	}


	void SoundStream::play()
	{
		FGEAL_CHECK_INIT();
		self.sfmlSoundStream.setLoop(false);
		self.sfmlSoundStream.play();
	}

	void SoundStream::loop()
	{
		FGEAL_CHECK_INIT();
		self.sfmlSoundStream.setLoop(true);
		self.sfmlSoundStream.play();
	}

	void SoundStream::stop()
	{
		FGEAL_CHECK_INIT();
		self.sfmlSoundStream.stop();
	}

	void SoundStream::pause()
	{
		FGEAL_CHECK_INIT();
		self.sfmlSoundStream.pause();
	}

	void SoundStream::resume()
	{
		FGEAL_CHECK_INIT();
		//xxx not sure if resumes from pause
		self.sfmlSoundStream.play();
	}

	bool SoundStream::isPlaying()
	{
		FGEAL_CHECK_INIT();
		return self.sfmlSoundStream.getStatus() == sf::Sound::Playing;
	}

	bool SoundStream::isPaused()
	{
		FGEAL_CHECK_INIT();
		return self.sfmlSoundStream.getStatus() == sf::Sound::Paused;
	}

	void SoundStream::setVolume(float value)
	{
		FGEAL_CHECK_INIT();
		self.sfmlSoundStream.setVolume(value*100);
	}

	float SoundStream::getVolume()
	{
		FGEAL_CHECK_INIT();
		return self.sfmlSoundStream.getVolume()*0.01;
	}

	void SoundStream::setPlaybackSpeed(float factor, bool)
	{
		FGEAL_CHECK_INIT();
		self.sfmlSoundStream.setPitch(factor);
	}

	float SoundStream::getPlaybackSpeed()
	{
		FGEAL_CHECK_INIT();
		return self.sfmlSoundStream.getPitch();
	}

	bool SoundStream::isStreaming()
	{
		FGEAL_CHECK_INIT();
		return true;
	}

	float SoundStream::getDuration()
	{
		FGEAL_CHECK_INIT();
		// we can do this because we know that sfmlSoundStream is a sf::Music instance
		return self.sfmlSoundStream.getDuration().asSeconds();
	}

	// ##################################################################################################################

	Music::Music(const string& filename)
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();
		if(not self.sfmlMusic.openFromFile(filename))
			throw AdapterException("Could not load music file \"%s\".", filename.c_str());
	}

	Music::~Music()
	{
		FGEAL_CHECK_INIT();
		delete &self;
	}

	void Music::play()
	{
		FGEAL_CHECK_INIT();
		self.sfmlMusic.setLoop(false);
		self.sfmlMusic.play();
	}

	void Music::loop()
	{
		FGEAL_CHECK_INIT();
		self.sfmlMusic.setLoop(true);
		self.sfmlMusic.play();
	}

	void Music::stop()
	{
		FGEAL_CHECK_INIT();
		self.sfmlMusic.stop();
	}

	void Music::pause()
	{
		FGEAL_CHECK_INIT();
		self.sfmlMusic.pause();
	}

	void Music::resume()
	{
		FGEAL_CHECK_INIT();
		self.sfmlMusic.play();
	}

	bool Music::isPlaying()
	{
		FGEAL_CHECK_INIT();
		return self.sfmlMusic.getStatus() == sf::Music::Playing;
	}

	bool Music::isPaused()
	{
		FGEAL_CHECK_INIT();
		return self.sfmlMusic.getStatus() == sf::Music::Paused;
	}

	void Music::setVolume(float value)
	{
		FGEAL_CHECK_INIT();
		self.sfmlMusic.setVolume(value*100);
	}

	float Music::getVolume()
	{
		FGEAL_CHECK_INIT();
		return self.sfmlMusic.getVolume()*0.01;
	}

	// todo SFML 2.0 supports calculating music length.
}
