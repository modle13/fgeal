/*
 * implementation.hpp (originally impl.hpp)
 *
 *  Created on: 30/01/2017
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2017  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef FGEAL_ADAPTERS_SFML2_0_IMPL_HPP_
#define FGEAL_ADAPTERS_SFML2_0_IMPL_HPP_

#include "fgeal/display.hpp"
#include "fgeal/image.hpp"
#include "fgeal/event.hpp"
#include "fgeal/font.hpp"
#include "fgeal/sound.hpp"

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

#include <string>

namespace fgeal
{
	extern sf::Clock uptimeClock;

	/**
	 * A pointer to the sf::Sprite that is supposed to be the "symbolical owner" of the sf::Texture on sf::RenderTarget.
	 * That, of course, only if the draw render target is an Image (a RenderTexture, actually). If draw render target is
	 * a Display (a RenderWindow, actually), then this pointer is null. */
	extern sf::Sprite* drawRenderTargetSprite;

	/// The draw render target
	extern sf::RenderTarget* drawRenderTarget;

	struct Display::implementation
	{
		sf::RenderWindow sfmlRenderWindow;
		std::string currentTitle;
		sf::Uint32 style;
	};

	struct Image::implementation
	{
		sf::Sprite sfmlSprite;
	};

	struct Event::implementation
	{
		sf::Event sfmlEvent;
	};

	struct EventQueue::implementation
	{
		sf::Event cachedSfmlEvent;
		bool cached;
	};

	struct Font::implementation
	{
		sf::Text sfmlText;
	};

	struct Sound::implementation
	{
		sf::Sound sfmlSound;
	};

	struct SoundStream::implementation
	{
		sf::Music sfmlSoundStream;
	};

	struct Music::implementation
	{
		sf::Music sfmlMusic;
	};

	namespace KeyboardKeyMapping
	{
		/// Returns the fgeal-equivalent key enum of the given SFML 2.x keycode.
		Keyboard::Key toGenericKey(sf::Keyboard::Key sfmlKeycode);

		/// Returns the SFML 2.x keycode of the given fgeal-equivalent key enum.
		sf::Keyboard::Key toUnderlyingLibraryKey(Keyboard::Key key);
	}

	namespace MouseButtonMapping
	{
		/// Returns the fgeal-equivalent mouse button enum of the given SFML 2.x mouse button number.
		Mouse::Button toGenericMouseButton(sf::Mouse::Button sfmlButtonNumber);

		/// Returns the SFML 2.x mouse button number of the fgeal-equivalent mouse button enum.
		sf::Mouse::Button toUnderlyingLibraryMouseButton(Mouse::Button button);
	}

	namespace JoystickManagement
	{
		/** Contains a list of axes (sf::Joystick::Axis enum) that exist on each joystick.
		 *  Implicity (by its size() amount), holds the number of connected joysticks and the number of axes of each joystick. */
		extern std::vector< std::vector<sf::Joystick::Axis> > existingAxes;
	}
}

#endif /* FGEAL_ADAPTERS_SFML2_0_IMPL_HPP_ */
