/*
 * event.cpp
 *
 *  Created on: 30/01/2017
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2017  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/event.hpp"

#define window Display::instance->self.sfmlRenderWindow

namespace fgeal
{
	Event::Event()
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();
	}

	Event::~Event()
	{
		FGEAL_CHECK_INIT();
		delete &self;
	}

	Event::Type Event::getEventType()
	{
		FGEAL_CHECK_INIT();
		switch(self.sfmlEvent.type)
		{
			case sf::Event::Closed:					return Event::TYPE_DISPLAY_CLOSURE;
			case sf::Event::KeyPressed: 			return Event::TYPE_KEY_PRESS;
			case sf::Event::KeyReleased:			return Event::TYPE_KEY_RELEASE;
			case sf::Event::MouseButtonPressed: 	return Event::TYPE_MOUSE_BUTTON_PRESS;
			case sf::Event::MouseButtonReleased:	return Event::TYPE_MOUSE_BUTTON_RELEASE;
			case sf::Event::MouseMoved:				return Event::TYPE_MOUSE_MOTION;
			case sf::Event::MouseWheelMoved:		return Event::TYPE_MOUSE_WHEEL_MOTION;
			case sf::Event::JoystickButtonPressed:	return Event::TYPE_JOYSTICK_BUTTON_PRESS;
			case sf::Event::JoystickButtonReleased: return Event::TYPE_JOYSTICK_BUTTON_RELEASE;
			case sf::Event::JoystickMoved:			return Event::TYPE_JOYSTICK_AXIS_MOTION;

			default:								return Event::TYPE_UNKNOWN;
		}
	}

	Keyboard::Key Event::getEventKeyCode()
	{
		FGEAL_CHECK_INIT();
		return KeyboardKeyMapping::toGenericKey(self.sfmlEvent.key.code);
	}

	Mouse::Button Event::getEventMouseButton()
	{
		FGEAL_CHECK_INIT();
		return MouseButtonMapping::toGenericMouseButton(self.sfmlEvent.mouseButton.button);
	}

	int Event::getEventMouseX()
	{
		FGEAL_CHECK_INIT();
		return (self.sfmlEvent.type == sf::Event::MouseMoved? self.sfmlEvent.mouseMove.x : self.sfmlEvent.mouseButton.x);
	}

	int Event::getEventMouseY()
	{
		FGEAL_CHECK_INIT();
		return (self.sfmlEvent.type == sf::Event::MouseMoved? self.sfmlEvent.mouseMove.y : self.sfmlEvent.mouseButton.y);
	}

	int Event::getEventMouseWheelMotionAmount()
	{
		FGEAL_CHECK_INIT();
		return self.sfmlEvent.mouseWheel.delta;
	}

	int Event::getEventJoystickIndex()
	{
		FGEAL_CHECK_INIT();
		return self.sfmlEvent.type == sf::Event::JoystickButtonPressed
				or self.sfmlEvent.type == sf::Event::JoystickButtonReleased? self.sfmlEvent.joystickButton.joystickId
                          : self.sfmlEvent.type == sf::Event::JoystickMoved? self.sfmlEvent.joystickMove.joystickId
                                                                            : -1;
	}

	int Event::getEventJoystickButtonIndex()
	{
		FGEAL_CHECK_INIT();
		return self.sfmlEvent.joystickButton.button;
	}

	int Event::getEventJoystickAxisIndex()
	{
		FGEAL_CHECK_INIT();
		for(unsigned a = 0; a < JoystickManagement::existingAxes[self.sfmlEvent.joystickMove.joystickId].size(); a++)
			if(JoystickManagement::existingAxes[self.sfmlEvent.joystickMove.joystickId][a] == self.sfmlEvent.joystickMove.axis)
				return a;

		return -1;
	}

	float Event::getEventJoystickAxisPosition()
	{
		FGEAL_CHECK_INIT();
		return self.sfmlEvent.joystickMove.position;
	}

	int Event::getEventJoystickSecondAxisIndex()
	{
		FGEAL_CHECK_INIT();
		return -1;
	}

	float Event::getEventJoystickSecondAxisPosition()
	{
		FGEAL_CHECK_INIT();
		return 0;
	}

	//******************* EVENTQUEUE

	EventQueue::EventQueue()
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();
		self.cached = false;
	}

	EventQueue::~EventQueue()
	{
		FGEAL_CHECK_INIT();
		delete &self;
	}

	bool EventQueue::isEmpty()
	{
		// hasEvents() already calls checkInit()
		return not hasEvents();
	}

	bool EventQueue::hasEvents()
	{
		FGEAL_CHECK_INIT();

		if(self.cached)
			return true;

		self.cached = true;
		if(window.pollEvent(self.cachedSfmlEvent) == true)
			return true;

		self.cached = false;
		return false;
	}

	Event* EventQueue::getNextEvent()
	{
		FGEAL_CHECK_INIT();
		Event* ev = new Event();
		if(getNextEvent(ev) == true)
			return ev;
		else
		{
			delete ev;
			return null;
		}
	}

	bool EventQueue::getNextEvent(Event* containerEvent)
	{
		FGEAL_CHECK_INIT();
		if(self.cached)
		{
			containerEvent->self.sfmlEvent = self.cachedSfmlEvent;
			self.cached = false;
			return true;
		}
		else return window.pollEvent(containerEvent->self.sfmlEvent);
	}

	bool EventQueue::skipNextEvent()
	{
		if(self.cached)
		{
			self.cached = false;
			return true;
		}
		else
		{
			sf::Event tmp;
			return window.pollEvent(tmp);
		}
	}

	void EventQueue::waitNextEvent(Event* containerEvent)
	{
		FGEAL_CHECK_INIT();
		if(not self.cached)
		{
			self.cached = true;
			window.waitEvent(self.cachedSfmlEvent);
		}

		if(containerEvent != null)
		{
			containerEvent->self.sfmlEvent = self.cachedSfmlEvent;
			self.cached = false;
		}
	}

	//XXX SFML 2.0 EventQueue::flushEvents() implementation is experimental and untested
	void EventQueue::flushEvents()
	{
		FGEAL_CHECK_INIT();

		if(self.cached)
			self.cached = false;

		// this is not safe and may loop indefinitely if events keep comming faster than we can dispose
		for(sf::Event ev; window.pollEvent(ev)==true; /*just continue*/);
	}

	//fixme SFML 2.0 EventQueue::setEventBypassingEnabled() not implemented
	void EventQueue::setEventBypassingEnabled(bool bypass)
	{
		FGEAL_CHECK_INIT();
		core::reportAbsentImplementation("EventQueue::setEventBypassingEnabled is not implemented in this adapter.");
	}
}
