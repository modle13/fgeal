/*
 * input.cpp
 *
 *  Created on: 30/01/2017
*
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2017  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/input.hpp"

#define FORWARD_MAPPING(a, b)  case a: return b;
#define BACKWARD_MAPPING(a, b) case b: return a;
#define window Display::instance->self.sfmlRenderWindow

namespace fgeal
{
	using std::string;
	using std::vector;

	// =======================================================================================
	// Keyboard

	namespace KeyboardKeyMapping
	{
		#define SFML_2_0_KEY_MAPPINGS(mapping)\
		mapping(sf::Keyboard::A, Keyboard::KEY_A)\
		mapping(sf::Keyboard::B, Keyboard::KEY_B)\
		mapping(sf::Keyboard::C, Keyboard::KEY_C)\
		mapping(sf::Keyboard::D, Keyboard::KEY_D)\
		mapping(sf::Keyboard::E, Keyboard::KEY_E)\
		mapping(sf::Keyboard::F, Keyboard::KEY_F)\
		mapping(sf::Keyboard::G, Keyboard::KEY_G)\
		mapping(sf::Keyboard::H, Keyboard::KEY_H)\
		mapping(sf::Keyboard::I, Keyboard::KEY_I)\
		mapping(sf::Keyboard::J, Keyboard::KEY_J)\
		mapping(sf::Keyboard::K, Keyboard::KEY_K)\
		mapping(sf::Keyboard::L, Keyboard::KEY_L)\
		mapping(sf::Keyboard::M, Keyboard::KEY_M)\
		mapping(sf::Keyboard::N, Keyboard::KEY_N)\
		mapping(sf::Keyboard::O, Keyboard::KEY_O)\
		mapping(sf::Keyboard::P, Keyboard::KEY_P)\
		mapping(sf::Keyboard::Q, Keyboard::KEY_Q)\
		mapping(sf::Keyboard::R, Keyboard::KEY_R)\
		mapping(sf::Keyboard::S, Keyboard::KEY_S)\
		mapping(sf::Keyboard::T, Keyboard::KEY_T)\
		mapping(sf::Keyboard::U, Keyboard::KEY_U)\
		mapping(sf::Keyboard::V, Keyboard::KEY_V)\
		mapping(sf::Keyboard::W, Keyboard::KEY_W)\
		mapping(sf::Keyboard::X, Keyboard::KEY_X)\
		mapping(sf::Keyboard::Y, Keyboard::KEY_Y)\
		mapping(sf::Keyboard::Z, Keyboard::KEY_Z)\
		\
		mapping(sf::Keyboard::Num0, Keyboard::KEY_0)\
		mapping(sf::Keyboard::Num1, Keyboard::KEY_1)\
		mapping(sf::Keyboard::Num2, Keyboard::KEY_2)\
		mapping(sf::Keyboard::Num3, Keyboard::KEY_3)\
		mapping(sf::Keyboard::Num4, Keyboard::KEY_4)\
		mapping(sf::Keyboard::Num5, Keyboard::KEY_5)\
		mapping(sf::Keyboard::Num6, Keyboard::KEY_6)\
		mapping(sf::Keyboard::Num7, Keyboard::KEY_7)\
		mapping(sf::Keyboard::Num8, Keyboard::KEY_8)\
		mapping(sf::Keyboard::Num9, Keyboard::KEY_9)\
		\
		mapping(sf::Keyboard::F1, Keyboard::KEY_F1)\
		mapping(sf::Keyboard::F2, Keyboard::KEY_F2)\
		mapping(sf::Keyboard::F3, Keyboard::KEY_F3)\
		mapping(sf::Keyboard::F4, Keyboard::KEY_F4)\
		mapping(sf::Keyboard::F5, Keyboard::KEY_F5)\
		mapping(sf::Keyboard::F6, Keyboard::KEY_F6)\
		mapping(sf::Keyboard::F7, Keyboard::KEY_F7)\
		mapping(sf::Keyboard::F8, Keyboard::KEY_F8)\
		mapping(sf::Keyboard::F9, Keyboard::KEY_F9)\
		mapping(sf::Keyboard::F10, Keyboard::KEY_F10)\
		mapping(sf::Keyboard::F11, Keyboard::KEY_F11)\
		mapping(sf::Keyboard::F12, Keyboard::KEY_F12)\
		\
		mapping(sf::Keyboard::Up,    Keyboard::KEY_ARROW_UP)\
		mapping(sf::Keyboard::Down,  Keyboard::KEY_ARROW_DOWN)\
		mapping(sf::Keyboard::Left,  Keyboard::KEY_ARROW_LEFT)\
		mapping(sf::Keyboard::Right, Keyboard::KEY_ARROW_RIGHT)\
		\
		mapping(sf::Keyboard::Return,    Keyboard::KEY_ENTER)\
		mapping(sf::Keyboard::Space,     Keyboard::KEY_SPACE)\
		mapping(sf::Keyboard::Escape,    Keyboard::KEY_ESCAPE)\
		mapping(sf::Keyboard::LControl,  Keyboard::KEY_LEFT_CONTROL)\
		mapping(sf::Keyboard::RControl,  Keyboard::KEY_RIGHT_CONTROL)\
		mapping(sf::Keyboard::LShift,    Keyboard::KEY_LEFT_SHIFT)\
		mapping(sf::Keyboard::RShift,    Keyboard::KEY_RIGHT_SHIFT)\
		mapping(sf::Keyboard::LAlt,      Keyboard::KEY_LEFT_ALT)\
		mapping(sf::Keyboard::RAlt,      Keyboard::KEY_RIGHT_ALT)\
		mapping(sf::Keyboard::LSystem,   Keyboard::KEY_LEFT_SUPER)\
		mapping(sf::Keyboard::RSystem,   Keyboard::KEY_RIGHT_SUPER)\
		mapping(sf::Keyboard::Menu,      Keyboard::KEY_MENU)\
		mapping(sf::Keyboard::Tab,       Keyboard::KEY_TAB)\
		mapping(sf::Keyboard::BackSpace, Keyboard::KEY_BACKSPACE)\
		\
		mapping(sf::Keyboard::Dash,      Keyboard::KEY_MINUS)\
		mapping(sf::Keyboard::Equal,     Keyboard::KEY_EQUALS)\
		mapping(sf::Keyboard::LBracket,  Keyboard::KEY_LEFT_BRACKET)\
		mapping(sf::Keyboard::RBracket,  Keyboard::KEY_RIGHT_BRACKET)\
		mapping(sf::Keyboard::SemiColon, Keyboard::KEY_SEMICOLON)\
		mapping(sf::Keyboard::Comma,     Keyboard::KEY_COMMA)\
		mapping(sf::Keyboard::Period,    Keyboard::KEY_PERIOD)\
		mapping(sf::Keyboard::Slash,     Keyboard::KEY_SLASH)\
		mapping(sf::Keyboard::BackSlash, Keyboard::KEY_BACKSLASH)\
		mapping(sf::Keyboard::Quote,     Keyboard::KEY_QUOTE)\
		mapping(sf::Keyboard::Tilde,     Keyboard::KEY_TILDE)\
		\
		mapping(sf::Keyboard::Insert,   Keyboard::KEY_INSERT)\
		mapping(sf::Keyboard::Delete,   Keyboard::KEY_DELETE)\
		mapping(sf::Keyboard::Home,     Keyboard::KEY_HOME)\
		mapping(sf::Keyboard::End,      Keyboard::KEY_END)\
		mapping(sf::Keyboard::PageUp,   Keyboard::KEY_PAGE_UP)\
		mapping(sf::Keyboard::PageDown, Keyboard::KEY_PAGE_DOWN)\
		\
		mapping(sf::Keyboard::Numpad0, Keyboard::KEY_NUMPAD_0)\
		mapping(sf::Keyboard::Numpad1, Keyboard::KEY_NUMPAD_1)\
		mapping(sf::Keyboard::Numpad2, Keyboard::KEY_NUMPAD_2)\
		mapping(sf::Keyboard::Numpad3, Keyboard::KEY_NUMPAD_3)\
		mapping(sf::Keyboard::Numpad4, Keyboard::KEY_NUMPAD_4)\
		mapping(sf::Keyboard::Numpad5, Keyboard::KEY_NUMPAD_5)\
		mapping(sf::Keyboard::Numpad6, Keyboard::KEY_NUMPAD_6)\
		mapping(sf::Keyboard::Numpad7, Keyboard::KEY_NUMPAD_7)\
		mapping(sf::Keyboard::Numpad8, Keyboard::KEY_NUMPAD_8)\
		mapping(sf::Keyboard::Numpad9, Keyboard::KEY_NUMPAD_9)\
		\
/*      fixme SFML 2.0 cant reference extra numpad keys directly. Instead, it passes a normal non-numpad version of the key.
  		mapping(sf::Keyboard::???, Keyboard::KEY_NUMPAD_ADDITION)\
		mapping(sf::Keyboard::???, Keyboard::KEY_NUMPAD_SUBTRACTION)\
		mapping(sf::Keyboard::???, Keyboard::KEY_NUMPAD_MULTIPLICATION)\
		mapping(sf::Keyboard::???, Keyboard::KEY_NUMPAD_DIVISION)\
		mapping(sf::Keyboard::???, Keyboard::KEY_NUMPAD_DECIMAL)\
		mapping(sf::Keyboard::???, Keyboard::KEY_NUMPAD_ENTER)\ */
		//end

		// Returns the fgeal-equivalent key enum of the given SFML 2.0 keycode.
		Keyboard::Key toGenericKey(sf::Keyboard::Key sfmlKeycode)
		{
			switch(sfmlKeycode)
			{
				SFML_2_0_KEY_MAPPINGS(FORWARD_MAPPING);
				default: return Keyboard::KEY_UNKNOWN;
			}
		}

		// Returns the SFML 2.0 keycode of the given fgeal-equivalent key enum.
		sf::Keyboard::Key toUnderlyingLibraryKey(Keyboard::Key key)
		{
			switch(key)
			{
				SFML_2_0_KEY_MAPPINGS(BACKWARD_MAPPING);
				default: return sf::Keyboard::KeyCount;
			}
		}
	}

	// API functions

	bool Keyboard::isKeyPressed(Keyboard::Key key)
	{
		FGEAL_CHECK_INIT();
		return sf::Keyboard::isKeyPressed(KeyboardKeyMapping::toUnderlyingLibraryKey(key));
	}

	// =======================================================================================
	// Mouse

	namespace MouseButtonMapping
	{
		#define SFML_2_0_MOUSE_BUTTON_MAPPINGS(mapping)\
		mapping(sf::Mouse::Left,   Mouse::BUTTON_LEFT)\
		mapping(sf::Mouse::Middle, Mouse::BUTTON_MIDDLE)\
		mapping(sf::Mouse::Right,  Mouse::BUTTON_RIGHT)\
		//end

		// Returns the fgeal-equivalent mouse button enum of the given SFML 2.0 mouse button number.
		Mouse::Button toGenericMouseButton(sf::Mouse::Button sfmlMouseButton)
		{
			switch(sfmlMouseButton)
			{
				SFML_2_0_MOUSE_BUTTON_MAPPINGS(FORWARD_MAPPING);
				default: return Mouse::BUTTON_UNKNOWN;
			}
		}

		// Returns the SFML 2.0 mouse button number of the fgeal-equivalent mouse button enum.
		sf::Mouse::Button toUnderlyingLibraryMouseButton(Mouse::Button button)
		{
			switch(button)
			{
				SFML_2_0_MOUSE_BUTTON_MAPPINGS(BACKWARD_MAPPING);
				default: return sf::Mouse::ButtonCount;
			}
		}
	}

	bool Mouse::isButtonPressed(Mouse::Button btn)
	{
		FGEAL_CHECK_INIT();
		return sf::Mouse::isButtonPressed(MouseButtonMapping::toUnderlyingLibraryMouseButton(btn));
	}

	Point Mouse::getPosition()
	{
		FGEAL_CHECK_INIT();
		Point pt = {(float) sf::Mouse::getPosition(window).x, (float) sf::Mouse::getPosition(window).y};
		return pt;
	}

	void Mouse::setPosition(const Point& position)
	{
		FGEAL_CHECK_INIT();
		Mouse::setPosition(position.x, position.y);
	}

	int Mouse::getPositionX()
	{
		FGEAL_CHECK_INIT();
		return sf::Mouse::getPosition(window).x;
	}

	int Mouse::getPositionY()
	{
		FGEAL_CHECK_INIT();
		return sf::Mouse::getPosition(window).y;
	}

	void Mouse::setPosition(int x, int y)
	{
		FGEAL_CHECK_INIT();
		sf::Mouse::setPosition(sf::Vector2i(x, y));
	}

	// =======================================================================================
	// Joystick

	vector< vector<sf::Joystick::Axis> > JoystickManagement::existingAxes;

	void Joystick::configureAll()
	{
		sf::Joystick::update();
		for(unsigned j = 0; j < sf::Joystick::Count; j++)
		if(sf::Joystick::isConnected(j))
		{
			// increments the amount of joysticks implicitly
			JoystickManagement::existingAxes.push_back(vector<sf::Joystick::Axis>());

			// for each "possible" axis
			for(unsigned a = 0; a < sf::Joystick::AxisCount; a++)
				if(sf::Joystick::hasAxis(j, static_cast<sf::Joystick::Axis>(a)))
					JoystickManagement::existingAxes[j].push_back(static_cast<sf::Joystick::Axis>(a));
		}
		else return;  // xxx is it the correct behavior? can SFML behave in a way that there are "holes" in the joysticks list? ex: 0,1,_,_,4,_,6,7,8
	}

	void Joystick::releaseAll()
	{
		JoystickManagement::existingAxes.clear();
	}

	unsigned Joystick::getCount()
	{
		FGEAL_CHECK_INIT();
		return JoystickManagement::existingAxes.size();  // implicitly has the number of joysticks
	}

	//xxx SFML 2.x adapter: when version is less than 2.2, sf::Joystick::getIdentification is not available
	string Joystick::getJoystickName(unsigned joystickIndex)
	{
		// for versions older than SFML 2.2, sf::Joystick::getIdentification() is not available
		#if SFML_VERSION_MAJOR == 2 and SFML_VERSION_MINOR >= 2
			return sf::Joystick::getIdentification(joystickIndex).name;
		#else
			core::reportAbsentImplementation("Getting joystick name is not supported on this adapter when using SFML versions older than 2.2.");
			return string();
		#endif
	}

	unsigned Joystick::getButtonCount(unsigned joystickIndex)
	{
		return sf::Joystick::getButtonCount(joystickIndex);
	}

	unsigned Joystick::getAxisCount(unsigned joystickIndex)
	{
		return JoystickManagement::existingAxes[joystickIndex].size();  // implicitly has the number of axis
	}

	bool Joystick::isButtonPressed(unsigned joystickIndex, unsigned buttonIndex)
	{
		return sf::Joystick::isButtonPressed(joystickIndex, buttonIndex);
	}

	float Joystick::getAxisPosition(unsigned joystickIndex, unsigned axisIndex)
	{
		return sf::Joystick::getAxisPosition(joystickIndex, JoystickManagement::existingAxes[joystickIndex][axisIndex]);
	}
}
