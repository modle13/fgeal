/*
 * font.cpp
 *
 *  Created on: 30/01/2017
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2017  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/font.hpp"

#include "fgeal/exceptions.hpp"

using std::string;

// fallback for SFML older than 2.4.
// from SFML 2.4 and upwards, setFillColor is the preferred way to set text color.
#if SFML_VERSION_MAJOR == 2 and SFML_VERSION_MINOR < 4
	#define setFillColor setColor
#endif

namespace fgeal
{
	Font::Font(const string& filename, int size, bool antialiasing)
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();
		sf::Font* font = new sf::Font();
		if(not font->loadFromFile(filename))
		{
			delete font; font = null;
			throw AdapterException("Font \"%s\" could not be loaded.", filename.c_str());
		}
		self.sfmlText.setFont(*font);
		self.sfmlText.setString("");
		self.sfmlText.setCharacterSize(size);

		//XXX SFML2.0 adapter ignoring font antialiasing parameter
	}

	Font::~Font()
	{
		delete self.sfmlText.getFont();
		delete &self;
	}

	void Font::drawText(const string& text, float x, float y, Color color)
	{
		FGEAL_CHECK_INIT();
		self.sfmlText.setString(text);
		self.sfmlText.setPosition(x, y);
		self.sfmlText.setFillColor(sf::Color(color.r, color.g, color.b, color.a));
		fgeal::drawRenderTarget->draw(self.sfmlText);
	}

	float Font::getHeight() const
	{
		FGEAL_CHECK_INIT();
		self.sfmlText.setString('|');
//		return self.sfmlText.getLocalBounds().height;  // bad results
//		return self.sfmlText.getCharacterSize();  // not bad, but unsatisfying results
		return self.sfmlText.getFont()->getLineSpacing(self.sfmlText.getCharacterSize());  // acceptable, but not perfect
	}

	float Font::getTextWidth(const std::string& text) const
	{
		FGEAL_CHECK_INIT();
		self.sfmlText.setString(text);
		return self.sfmlText.getLocalBounds().width;
	}
}
