/*
 * image.cpp
 *
 *  Created on: 24/10/2016
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2016  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/image.hpp"

#include "fgeal/exceptions.hpp"

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#ifndef SDL_PIXELFORMAT_RGBA32
	#define SDL_PIXELFORMAT_RGBA32 SDL_PIXELFORMAT_RGBA8888
#endif

// Useful macros to improve readability and reduce typing
#define displayRenderer Display::instance->self.sdlRenderer
#define toSDLRect(x, y, w, h) {static_cast<Sint16>(x), static_cast<Sint16>(y), static_cast<Uint16>(w), static_cast<Uint16>(h)}
#define toSDLRect2(x, y)      {static_cast<Sint16>(x), static_cast<Sint16>(y)}
#define toSDLPoint(x, y)      {static_cast<int>(x),    static_cast<int>(y)}

using std::string;

namespace fgeal
{
	static SDL_RendererFlip toSdlRendererFlipFlag(const Image::FlipMode mode)
	{
		switch (mode) {
			default:case Image::FLIP_NONE:  return SDL_FLIP_NONE;
			case Image::FLIP_HORIZONTAL:    return SDL_FLIP_HORIZONTAL;
			case Image::FLIP_VERTICAL:      return SDL_FLIP_VERTICAL;
		}
	}

	// =====================================================================================================================================================

	Image::Image(const string& filename)
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();
		self.sdlTexture = IMG_LoadTexture(displayRenderer, filename.c_str());
		if (self.sdlTexture == null)
			throw AdapterException("Could not load image \"%s\": %s", filename.c_str(), IMG_GetError());

		SDL_QueryTexture(self.sdlTexture, null, null, &self.w, &self.h);
	}

	Image::Image(int w, int h)
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();
		self.sdlTexture = SDL_CreateTexture(displayRenderer, SDL_PIXELFORMAT_RGBA32, SDL_TEXTUREACCESS_TARGET, w, h);
		if (self.sdlTexture == null)
			throw AdapterException("Could not load image with dimensions w=%d h=%d : %s", w, h, IMG_GetError());

		// clear the surfarce with transparent black
		SDL_SetRenderTarget(displayRenderer, self.sdlTexture);
		SDL_SetTextureBlendMode(self.sdlTexture, SDL_BLENDMODE_BLEND);
		SDL_SetRenderDrawColor(displayRenderer, 0, 0, 0, SDL_ALPHA_TRANSPARENT);
		SDL_RenderClear(displayRenderer);
		SDL_SetRenderTarget(displayRenderer, null);
		self.w = w;
		self.h = h;
	}

	Image::~Image()
	{
		FGEAL_CHECK_INIT();
		SDL_DestroyTexture(self.sdlTexture);
		delete &self;
	}

	int Image::getWidth()
	{
		FGEAL_CHECK_INIT();
		return self.w;
	}

	int Image::getHeight()
	{
		FGEAL_CHECK_INIT();
		return self.h;
	}

	void Image::draw(float x, float y)
	{
		FGEAL_CHECK_INIT();
		//draws all source region
		SDL_Rect dstrect = toSDLRect(x, y, self.w, self.h);
		SDL_RenderCopy(displayRenderer, self.sdlTexture, null, &dstrect);
	}

	void Image::drawRegion(float x, float y, float fromX, float fromY, float fromWidth, float fromHeight)
	{
		FGEAL_CHECK_INIT();
		SDL_Rect dstrect = toSDLRect(x, y, fromWidth, fromHeight);
		SDL_Rect srcrect = toSDLRect(fromX, fromY, fromWidth, fromHeight);  // draws selected region
		SDL_RenderCopy(displayRenderer, self.sdlTexture, &srcrect, &dstrect);
	}

	void Image::drawFlipped(float x, float y, const FlipMode flipmode)
	{
		FGEAL_CHECK_INIT();
		//draws all source region
		SDL_Rect dstrect = toSDLRect(x, y, self.w, self.h);
		SDL_RenderCopyEx(displayRenderer, self.sdlTexture, null, &dstrect, 0, null, toSdlRendererFlipFlag(flipmode));
	}

	void Image::drawFlippedRegion(float x, float y, const FlipMode flipmode, float fromX, float fromY, float fromWidth, float fromHeight)
	{
		FGEAL_CHECK_INIT();
		SDL_Rect dstrect = toSDLRect(x, y, fromWidth, fromHeight);
		SDL_Rect srcrect = toSDLRect(fromX, fromY, fromWidth, fromHeight);  // draws selected region
		SDL_RenderCopyEx(displayRenderer, self.sdlTexture, &srcrect, &dstrect, 0, null, toSdlRendererFlipFlag(flipmode));
	}

	void Image::drawScaled(float x, float y, float xScale, float yScale, const FlipMode flipmode)
	{
		FGEAL_CHECK_INIT();
		//draws all source region
		SDL_Rect dstrect = toSDLRect(x, y, (self.w * xScale), (self.h * yScale));
		SDL_RenderCopyEx(displayRenderer, self.sdlTexture, null, &dstrect, 0, null, toSdlRendererFlipFlag(flipmode));
	}

	void Image::drawScaledRegion(float x, float y, float xScale, float yScale, const FlipMode flipmode, float fromX, float fromY, float fromWidth, float fromHeight)
	{
		FGEAL_CHECK_INIT();
		SDL_Rect dstrect = toSDLRect(x, y, (fromWidth * xScale), (fromHeight * yScale));
		SDL_Rect srcrect = toSDLRect(fromX, fromY, fromWidth, fromHeight);  // draws selected region
		SDL_RenderCopyEx(displayRenderer, self.sdlTexture, &srcrect, &dstrect, 0, null, toSdlRendererFlipFlag(flipmode));
	}

	void Image::drawRotated(float x, float y, float angle, float centerX, float centerY, const FlipMode flipmode)
	{
		FGEAL_CHECK_INIT();
		SDL_Point center = toSDLPoint(centerX, centerY);
		SDL_Rect dstrect = toSDLRect2((x - centerX), (y - centerY));
		SDL_QueryTexture(self.sdlTexture, null, null, &(dstrect.w), &(dstrect.h));  //@suppress("Invalid arguments")
		SDL_RenderCopyEx(displayRenderer, self.sdlTexture, null, &dstrect, rad2deg(-angle), &center, toSdlRendererFlipFlag(flipmode));
	}

	void Image::drawRotatedRegion(float x, float y, float angle, float centerX, float centerY, const FlipMode flipmode, float fromX, float fromY, float fromWidth, float fromHeight)
	{
		FGEAL_CHECK_INIT();
		SDL_Point center = toSDLPoint(centerX, centerY);
		SDL_Rect dstrect = toSDLRect((x - centerX), (y - centerY), fromWidth, fromHeight);
		SDL_Rect srcrect = toSDLRect(fromX, fromY, fromWidth, fromHeight);  // draws selected region
		SDL_RenderCopyEx(displayRenderer, self.sdlTexture, &srcrect, &dstrect, rad2deg(-angle), &center, toSdlRendererFlipFlag(flipmode));
	}

	void Image::drawScaledRotated(float x, float y, float xScale, float yScale, float angle, float centerX, float centerY, const FlipMode flipmode)
	{
		FGEAL_CHECK_INIT();
		SDL_Point center = toSDLPoint(xScale*centerX, yScale*centerY);
		//draws all source region
		SDL_Rect dstrect = toSDLRect((x - xScale*centerX), (y - yScale*centerY), (self.w * xScale), (self.h * yScale));
		SDL_RenderCopyEx(displayRenderer, self.sdlTexture, null, &dstrect, rad2deg(-angle), &center, toSdlRendererFlipFlag(flipmode));
	}

	void Image::drawScaledRotatedRegion(float x, float y, float xScale, float yScale, float angle, float centerX, float centerY, const FlipMode flipmode, float fromX, float fromY, float fromWidth, float fromHeight)
	{
		FGEAL_CHECK_INIT();
		SDL_Point center = toSDLPoint(xScale*centerX, yScale*centerY);
		SDL_Rect dstrect = toSDLRect((x - xScale*centerX), (y - yScale*centerY), (fromWidth * xScale), (fromHeight * yScale));
		SDL_Rect srcrect = toSDLRect(fromX, fromY, fromWidth, fromHeight);  // draws selected region
		SDL_RenderCopyEx(displayRenderer, self.sdlTexture, &srcrect, &dstrect, rad2deg(-angle), &center, toSdlRendererFlipFlag(flipmode));
	}

	void Image::blit(Image& img, float x, float y, float xScale, float yScale, float angle, float centerX, float centerY, const FlipMode flipmode, float fromX, float fromY, float fromWidth, float fromHeight)
	{
		FGEAL_CHECK_INIT();

		SDL_BlendMode previousMode;
		SDL_GetTextureBlendMode(img.self.sdlTexture, &previousMode);
		SDL_SetTextureBlendMode(img.self.sdlTexture, SDL_BLENDMODE_BLEND);

		// set img as target
		SDL_SetRenderTarget(displayRenderer, img.self.sdlTexture);

		// draw
		this->delegateDraw(x, y, xScale, yScale, angle, centerX, centerY, flipmode, fromX, fromY, fromWidth, fromHeight);

		// restores Display::instance target
		SDL_SetRenderTarget(displayRenderer, null);
		SDL_SetTextureBlendMode(img.self.sdlTexture, previousMode);
	}
}
