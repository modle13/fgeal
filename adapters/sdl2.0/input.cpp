/*
 * input.cpp
 *
 *  Created on: 25/01/2017
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2017  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/input.hpp"

#define FORWARD_MAPPING(a, b)  case a: return b;
#define BACKWARD_MAPPING(a, b) case b: return a;

namespace fgeal
{
	using std::vector;
	using std::string;
	using JoystickManagement::AuxData;

	// =======================================================================================
	// Keyboard

	namespace KeyboardKeyMapping
	{
		#define SDL_2_0_KEY_MAPPINGS(mapping) \
		mapping(SDLK_a, Keyboard::KEY_A)\
		mapping(SDLK_b, Keyboard::KEY_B)\
		mapping(SDLK_c, Keyboard::KEY_C)\
		mapping(SDLK_d, Keyboard::KEY_D)\
		mapping(SDLK_e, Keyboard::KEY_E)\
		mapping(SDLK_f, Keyboard::KEY_F)\
		mapping(SDLK_g, Keyboard::KEY_G)\
		mapping(SDLK_h, Keyboard::KEY_H)\
		mapping(SDLK_i, Keyboard::KEY_I)\
		mapping(SDLK_j, Keyboard::KEY_J)\
		mapping(SDLK_k, Keyboard::KEY_K)\
		mapping(SDLK_l, Keyboard::KEY_L)\
		mapping(SDLK_m, Keyboard::KEY_M)\
		mapping(SDLK_n, Keyboard::KEY_N)\
		mapping(SDLK_o, Keyboard::KEY_O)\
		mapping(SDLK_p, Keyboard::KEY_P)\
		mapping(SDLK_q, Keyboard::KEY_Q)\
		mapping(SDLK_r, Keyboard::KEY_R)\
		mapping(SDLK_s, Keyboard::KEY_S)\
		mapping(SDLK_t, Keyboard::KEY_T)\
		mapping(SDLK_u, Keyboard::KEY_U)\
		mapping(SDLK_v, Keyboard::KEY_V)\
		mapping(SDLK_w, Keyboard::KEY_W)\
		mapping(SDLK_x, Keyboard::KEY_X)\
		mapping(SDLK_y, Keyboard::KEY_Y)\
		mapping(SDLK_z, Keyboard::KEY_Z)\
		\
		mapping(SDLK_0, Keyboard::KEY_0)\
		mapping(SDLK_1, Keyboard::KEY_1)\
		mapping(SDLK_2, Keyboard::KEY_2)\
		mapping(SDLK_3, Keyboard::KEY_3)\
		mapping(SDLK_4, Keyboard::KEY_4)\
		mapping(SDLK_5, Keyboard::KEY_5)\
		mapping(SDLK_6, Keyboard::KEY_6)\
		mapping(SDLK_7, Keyboard::KEY_7)\
		mapping(SDLK_8, Keyboard::KEY_8)\
		mapping(SDLK_9, Keyboard::KEY_9)\
		\
		mapping(SDLK_F1,  Keyboard::KEY_F1)\
		mapping(SDLK_F2,  Keyboard::KEY_F2)\
		mapping(SDLK_F3,  Keyboard::KEY_F3)\
		mapping(SDLK_F4,  Keyboard::KEY_F4)\
		mapping(SDLK_F5,  Keyboard::KEY_F5)\
		mapping(SDLK_F6,  Keyboard::KEY_F6)\
		mapping(SDLK_F7,  Keyboard::KEY_F7)\
		mapping(SDLK_F8,  Keyboard::KEY_F8)\
		mapping(SDLK_F9,  Keyboard::KEY_F9)\
		mapping(SDLK_F10, Keyboard::KEY_F10)\
		mapping(SDLK_F11, Keyboard::KEY_F11)\
		mapping(SDLK_F12, Keyboard::KEY_F12)\
		\
		mapping(SDLK_UP,    Keyboard::KEY_ARROW_UP)\
		mapping(SDLK_DOWN,  Keyboard::KEY_ARROW_DOWN)\
		mapping(SDLK_LEFT,  Keyboard::KEY_ARROW_LEFT)\
		mapping(SDLK_RIGHT, Keyboard::KEY_ARROW_RIGHT)\
		\
		mapping(SDLK_RETURN, Keyboard::KEY_ENTER)\
		mapping(SDLK_SPACE,  Keyboard::KEY_SPACE)\
		mapping(SDLK_ESCAPE, Keyboard::KEY_ESCAPE)\
		mapping(SDLK_LCTRL,     Keyboard::KEY_LEFT_CONTROL)\
		mapping(SDLK_RCTRL,     Keyboard::KEY_RIGHT_CONTROL)\
		mapping(SDLK_LSHIFT,    Keyboard::KEY_LEFT_SHIFT)\
		mapping(SDLK_RSHIFT,    Keyboard::KEY_RIGHT_SHIFT)\
		mapping(SDLK_LALT,      Keyboard::KEY_LEFT_ALT)\
		mapping(SDLK_RALT,      Keyboard::KEY_RIGHT_ALT)\
		mapping(SDLK_LGUI,    Keyboard::KEY_LEFT_SUPER)\
		mapping(SDLK_RGUI,    Keyboard::KEY_RIGHT_SUPER)\
		mapping(SDLK_MENU,      Keyboard::KEY_MENU)\
		mapping(SDLK_TAB,       Keyboard::KEY_TAB)\
		mapping(SDLK_BACKSPACE, Keyboard::KEY_BACKSPACE)\
		\
		mapping(SDLK_MINUS,        Keyboard::KEY_MINUS)\
		mapping(SDLK_EQUALS,       Keyboard::KEY_EQUALS)\
		mapping(SDLK_LEFTBRACKET,  Keyboard::KEY_LEFT_BRACKET)\
		mapping(SDLK_RIGHTBRACKET, Keyboard::KEY_RIGHT_BRACKET)\
		mapping(SDLK_SEMICOLON,    Keyboard::KEY_SEMICOLON)\
		mapping(SDLK_COMMA,        Keyboard::KEY_COMMA)\
		mapping(SDLK_PERIOD,       Keyboard::KEY_PERIOD)\
		mapping(SDLK_SLASH,        Keyboard::KEY_SLASH)\
		mapping(SDLK_BACKSLASH,    Keyboard::KEY_BACKSLASH)\
		mapping(SDLK_QUOTE,        Keyboard::KEY_QUOTE)\
		mapping(SDLK_BACKQUOTE,    Keyboard::KEY_TILDE)\
		\
		mapping(SDLK_INSERT,   Keyboard::KEY_INSERT)\
		mapping(SDLK_DELETE,   Keyboard::KEY_DELETE)\
		mapping(SDLK_HOME,     Keyboard::KEY_HOME)\
		mapping(SDLK_END,      Keyboard::KEY_END)\
		mapping(SDLK_PAGEUP,   Keyboard::KEY_PAGE_UP)\
		mapping(SDLK_PAGEDOWN, Keyboard::KEY_PAGE_DOWN)\
		\
		mapping(SDLK_KP_0, Keyboard::KEY_NUMPAD_0)\
		mapping(SDLK_KP_1, Keyboard::KEY_NUMPAD_1)\
		mapping(SDLK_KP_2, Keyboard::KEY_NUMPAD_2)\
		mapping(SDLK_KP_3, Keyboard::KEY_NUMPAD_3)\
		mapping(SDLK_KP_4, Keyboard::KEY_NUMPAD_4)\
		mapping(SDLK_KP_5, Keyboard::KEY_NUMPAD_5)\
		mapping(SDLK_KP_6, Keyboard::KEY_NUMPAD_6)\
		mapping(SDLK_KP_7, Keyboard::KEY_NUMPAD_7)\
		mapping(SDLK_KP_8, Keyboard::KEY_NUMPAD_8)\
		mapping(SDLK_KP_9, Keyboard::KEY_NUMPAD_9)\
		\
/*		mapping(SDLK_KP_PLUS,     Keyboard::KEY_NUMPAD_ADDITION)\
		mapping(SDLK_KP_MINUS,    Keyboard::KEY_NUMPAD_SUBTRACTION)\
		mapping(SDLK_KP_MULTIPLY, Keyboard::KEY_NUMPAD_MULTIPLICATION)\
		mapping(SDLK_KP_DIVIDE,   Keyboard::KEY_NUMPAD_DIVISION)\
		mapping(SDLK_KP_PERIOD,   Keyboard::KEY_NUMPAD_DECIMAL)\
		mapping(SDLK_KP_ENTER,    Keyboard::KEY_NUMPAD_ENTER)\ */
		//end

		// Returns the fgeal-equivalent key enum of the given SDL 2.0 keycode.
		Keyboard::Key toGenericKey(int sdlKeyCode)
		{
			switch(sdlKeyCode)
			{
				SDL_2_0_KEY_MAPPINGS(FORWARD_MAPPING);
				default: return Keyboard::KEY_UNKNOWN;
			}
		}

		// Returns the SDL 2.0 keycode of the given fgeal-equivalent key enum.
		int toUnderlyingLibraryKey(Keyboard::Key key)
		{
			switch(key)
			{
				SDL_2_0_KEY_MAPPINGS(BACKWARD_MAPPING);
				default: return 0;
			}
		}
	}

	// API functions

	bool Keyboard::isKeyPressed(Keyboard::Key key)
	{
		FGEAL_CHECK_INIT();
		return (bool) SDL_GetKeyboardState(null)[SDL_GetScancodeFromKey(KeyboardKeyMapping::toUnderlyingLibraryKey(key))];
	}

	// =======================================================================================
	// Mouse

	namespace MouseButtonMapping
	{
		#define SDL_2_0_MOUSE_BUTTON_MAPPINGS(mapping)\
		mapping(SDL_BUTTON_LEFT,   Mouse::BUTTON_LEFT)\
		mapping(SDL_BUTTON_MIDDLE, Mouse::BUTTON_MIDDLE)\
		mapping(SDL_BUTTON_RIGHT,  Mouse::BUTTON_RIGHT)\
		//end

		// Returns the fgeal-equivalent mouse button enum of the given SDL 2.0 mouse button number.
		Mouse::Button toGenericMouseButton(int sdlMouseButtonNumber)
		{
			switch(sdlMouseButtonNumber)
			{
				SDL_2_0_MOUSE_BUTTON_MAPPINGS(FORWARD_MAPPING);
				default: return Mouse::BUTTON_UNKNOWN;
			}
		}

		// Returns the SDL 2.0 mouse button number of the fgeal-equivalent mouse button enum.
		int toUnderlyingLibraryMouseButton(Mouse::Button button)
		{
			switch(button)
			{
				SDL_2_0_MOUSE_BUTTON_MAPPINGS(BACKWARD_MAPPING);
				default: return 0;
			}
		}
	}


	// API functions

	bool Mouse::isButtonPressed(Mouse::Button btn)
	{
		FGEAL_CHECK_INIT();
		return SDL_GetMouseState(null, null) & SDL_BUTTON(MouseButtonMapping::toUnderlyingLibraryMouseButton(btn));
	}

	Point Mouse::getPosition()
	{
		FGEAL_CHECK_INIT();
		static int x, y;
		SDL_GetMouseState(&x, &y);
		Point pt = {(float) x, (float) y};
		return pt;
	}

	void Mouse::setPosition(const Point& position)
	{
		FGEAL_CHECK_INIT();
		Mouse::setPosition(position.x, position.y);
	}

	int Mouse::getPositionX()
	{
		FGEAL_CHECK_INIT();
		static int x;
		SDL_GetMouseState(&x, null);
		return x;
	}

	int Mouse::getPositionY()
	{
		FGEAL_CHECK_INIT();
		static int y;
		SDL_GetMouseState(null, &y);
		return y;
	}

	void Mouse::setPosition(int x, int y)
	{
		FGEAL_CHECK_INIT();
		SDL_WarpMouseInWindow(Display::instance->self.sdlWindow, x, y);
	}


	// =======================================================================================
	// Joystick

	vector<JoystickManagement::AuxData> JoystickManagement::auxData;

	SDL_Joystick* JoystickManagement::getSdlJoystickByID(SDL_JoystickID id)
	{
		for(unsigned j = 0; j < JoystickManagement::auxData.size(); j++)
		{
			AuxData& aux = JoystickManagement::auxData[j];
			if(SDL_JoystickInstanceID(aux.sdlJoystick) == id)
				return aux.sdlJoystick;
		}
		return NULL;
	}

	float JoystickManagement::getAxisValueFromHatPov(float currentValue, bool isPovY)
	{
		if(isPovY)  // pov_y
		{
			if(currentValue == SDL_HAT_UP or currentValue == SDL_HAT_LEFTUP or currentValue == SDL_HAT_RIGHTUP)
				return -1;
			else if(currentValue == SDL_HAT_DOWN or currentValue == SDL_HAT_LEFTDOWN or currentValue == SDL_HAT_RIGHTDOWN)
				return 1;
			else
				return 0;
		}
		else  // pov_x
		{
			if(currentValue == SDL_HAT_LEFT or currentValue == SDL_HAT_LEFTUP or currentValue == SDL_HAT_LEFTDOWN)
				return -1;
			else if(currentValue == SDL_HAT_RIGHT or currentValue == SDL_HAT_RIGHTUP or currentValue == SDL_HAT_RIGHTDOWN)
				return 1;
			else
				return 0;
		}
	}

	void JoystickManagement::recordJoystickEventHatPovStateIfNeeded(SDL_Event* sdlEvent)
	{
		if(sdlEvent->type == SDL_JOYHATMOTION)
			JoystickManagement::auxData[sdlEvent->jhat.which].lastHatsStates[sdlEvent->jhat.hat] = sdlEvent->jhat.value;
	}

	void Joystick::configureAll()
	{
		for(int j = 0; j < SDL_NumJoysticks(); j++)
		{
			SDL_Joystick* sdlJoystick = SDL_JoystickOpen(j);
			if(sdlJoystick != null)
			{
				JoystickManagement::auxData.push_back(AuxData());
				AuxData& aux = JoystickManagement::auxData[j];  // or JoystickManagement::auxData.back()

				aux.sdlJoystick = sdlJoystick;
				for(int h = 0; h < SDL_JoystickNumHats(sdlJoystick); h++)
					aux.lastHatsStates.push_back(SDL_HAT_CENTERED);  // save state of each hat
			}
			else {} // todo create a proper exception when a joystick could not be opened (or a message on cout/cerr).
		}
	}

	void Joystick::releaseAll()
	{
		for(unsigned j = 0; j < JoystickManagement::auxData.size(); j++)
		{
			SDL_JoystickClose(JoystickManagement::auxData[j].sdlJoystick);
//			delete JoystickManagement::auxData[j].sdlJoystick;  // todo check if we need to delete this or the SDL_JoystickClose() function does it already.
		}
		JoystickManagement::auxData.clear();
	}

	unsigned Joystick::getCount()
	{
		FGEAL_CHECK_INIT();
		return SDL_NumJoysticks();
	}

	string Joystick::getJoystickName(unsigned joyIndex)
	{
		FGEAL_CHECK_INIT();
		return SDL_JoystickName(JoystickManagement::auxData[joyIndex].sdlJoystick);
	}

	/** Returns the number of buttons on this joystick. */
	unsigned Joystick::getButtonCount(unsigned joyIndex)
	{
		FGEAL_CHECK_INIT();
		return SDL_JoystickNumButtons(JoystickManagement::auxData[joyIndex].sdlJoystick);
	}

	/** Returns the number of axis on this joystick. */
	unsigned Joystick::getAxisCount(unsigned joyIndex)
	{
		FGEAL_CHECK_INIT();
		SDL_Joystick* const sdlJoyPtr = JoystickManagement::auxData[joyIndex].sdlJoystick;
		return SDL_JoystickNumAxes(sdlJoyPtr) + 2*SDL_JoystickNumHats(sdlJoyPtr) + 2*SDL_JoystickNumBalls(sdlJoyPtr);
	}

	/** Returns true if the given button (by index) is being pressed on this joystick. */
	bool Joystick::isButtonPressed(unsigned joyIndex, unsigned buttonIndex)
	{
		FGEAL_CHECK_INIT();
		return SDL_JoystickGetButton(JoystickManagement::auxData[joyIndex].sdlJoystick, buttonIndex) > 0;
	}

	/** Returns the current position of the given axis (by index) on this joystick. */
	float Joystick::getAxisPosition(unsigned joyIndex, unsigned axisIndex)
	{
		FGEAL_CHECK_INIT();
		SDL_Joystick* const sdlJoyPtr = JoystickManagement::auxData[joyIndex].sdlJoystick;
		const unsigned axesCount = SDL_JoystickNumAxes(sdlJoyPtr),
					   hatsCount = SDL_JoystickNumHats(sdlJoyPtr),
					   ballCount = SDL_JoystickNumBalls(sdlJoyPtr);

		if(axisIndex < axesCount)
			return SDL_JoystickGetAxis(sdlJoyPtr, axisIndex)/32768.0f;  // converting range [-32768 to 32767] to [-1.0 to 1.0]

		else if(axisIndex < axesCount + 2*hatsCount)
		{
			const unsigned hat = (axisIndex - axesCount)/2,
					       pov = (axisIndex - axesCount)%2;

			return JoystickManagement::getAxisValueFromHatPov(SDL_JoystickGetHat(sdlJoyPtr, hat), (pov == 1));
		}
		else if(axisIndex < axesCount + 2*hatsCount + 2*ballCount)
		{
			const unsigned ball = (axisIndex - axesCount - 2*hatsCount)/2,
					       dir  = (axisIndex - axesCount - 2*hatsCount)%2;
			int dx, dy;
			SDL_JoystickGetBall(sdlJoyPtr, ball, &dx, &dy);
			return dir == 0? dx : dy;
		}
		else
			return 0;
	}
}
