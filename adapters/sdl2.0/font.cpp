/*
 * font.cpp
 *
 *  Created on: 24/10/2016
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2016  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/font.hpp"

#include "fgeal/exceptions.hpp"

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#define useAA self.isAntialiased

using std::string;

namespace fgeal
{
	static SDL_Color toSDLColor(const Color& c)
	{
		SDL_Color sdlc;
		sdlc.r = c.r;
		sdlc.g = c.g;
		sdlc.b = c.b;
		return sdlc;
	}

	Font::Font(const string& filename, int size, bool antialiasing)
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();

		self.sdlttfFont = TTF_OpenFont(filename.c_str(), size);

		if(self.sdlttfFont == null)
			throw AdapterException("Font %s could not be loaded: %s", filename.c_str(), TTF_GetError());

		self.isAntialiased = antialiasing;
		self.cachedTexture = null;
		self.cachedTextureWidth = 0;
		self.cachedTextureHeight = 0;
	}

	Font::~Font()
	{
		FGEAL_CHECK_INIT();
		TTF_CloseFont(self.sdlttfFont);
		if(self.cachedTexture != null) SDL_DestroyTexture(self.cachedTexture);
		delete &self;
	}

	void Font::drawText(const string& text, float x, float y, Color color)
	{
		FGEAL_CHECK_INIT();

		// "cache miss", need to re-render ttf
		if(text != self.cachedText or color != self.cachedColor)
		{
			SDL_Surface* renderedTextSurface = (useAA? TTF_RenderText_Blended : TTF_RenderText_Solid)(self.sdlttfFont, text.c_str(), toSDLColor(color));

			if(renderedTextSurface == null)
				throw AdapterException("Failed to render text from font: %s \n Failed when trying to draw \"%s\"", TTF_GetError() ,text.c_str());

			SDL_Texture* cachedTexture = SDL_CreateTextureFromSurface(Display::instance->self.sdlRenderer, renderedTextSurface);

			if(cachedTexture == null)
				throw AdapterException("Failed to create texture from text surface: %s \n Failed when trying to draw \"%s\"", SDL_GetError(), text.c_str());

			// force alpha on text since SDL_ttf ignores SDL_Color's 'a' field.
			if(color.a != 255)
				SDL_SetTextureAlphaMod(cachedTexture, color.a);

			// cleanup if there is a old cached texture
			if(self.cachedTexture != null)
				SDL_DestroyTexture(self.cachedTexture);

			// keep this as cache
			self.cachedTexture = cachedTexture;
			self.cachedText = text;
			self.cachedColor = color;
			self.cachedTextureWidth = renderedTextSurface->w;
			self.cachedTextureHeight = renderedTextSurface->h;

			// free temporary surface
			SDL_FreeSurface(renderedTextSurface);
		}

		SDL_Rect dstrect = {(Sint16) x, (Sint16) y, self.cachedTextureWidth, self.cachedTextureHeight};

		int renderStatus =
		SDL_RenderCopy(Display::instance->self.sdlRenderer, self.cachedTexture, null, &dstrect);

		if(renderStatus != 0)
			throw AdapterException("Failed to draw text texture: %s \n Failed when trying to draw \"%s\"", SDL_GetError(), text.c_str());
	}

	float Font::getHeight() const
	{
		FGEAL_CHECK_INIT();
		return TTF_FontHeight(self.sdlttfFont);
	}

	float Font::getTextWidth(const std::string& text) const
	{
		FGEAL_CHECK_INIT();
		int width, queryStatus =
		TTF_SizeText(self.sdlttfFont, text.c_str(), &width, null);

		if(queryStatus != 0)
			throw AdapterException("Font width could not be obtained: %s", TTF_GetError());

		return width;
	}
}

