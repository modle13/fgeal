/*
 * event.cpp
 *
 *  Created on: 24/10/2016
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2016  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/event.hpp"

#include "fgeal/exceptions.hpp"

#include <SDL2/SDL.h>

#if SDL_MAJOR_VERSION == 2 and SDL_MINOR_VERSION == 0 and SDL_PATCHLEVEL < 4
	#define SDL_JoystickFromInstanceID JoystickManagement::getSdlJoystickByID
#endif

namespace fgeal
{
	static int ignoreAllSdlEventFilter(void *data, const SDL_Event *event) { return 0; }

	Event::Event()
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();
		self.sdlEvent = new SDL_Event();
	}

	Event::~Event()
	{
		FGEAL_CHECK_INIT();
		delete self.sdlEvent;
		delete &self;
	}

	Event::Type Event::getEventType()
	{
		FGEAL_CHECK_INIT();
		switch(self.sdlEvent->type)
		{
			case SDL_QUIT:              return Event::TYPE_DISPLAY_CLOSURE;
			case SDL_KEYDOWN:           return Event::TYPE_KEY_PRESS;
			case SDL_KEYUP:             return Event::TYPE_KEY_RELEASE;
			case SDL_MOUSEBUTTONDOWN:   return Event::TYPE_MOUSE_BUTTON_PRESS;
			case SDL_MOUSEBUTTONUP:     return Event::TYPE_MOUSE_BUTTON_RELEASE;
			case SDL_MOUSEMOTION:       return Event::TYPE_MOUSE_MOTION;
			case SDL_MOUSEWHEEL:		return Event::TYPE_MOUSE_WHEEL_MOTION;
			case SDL_JOYBUTTONDOWN:		return Event::TYPE_JOYSTICK_BUTTON_PRESS;
			case SDL_JOYBUTTONUP:		return Event::TYPE_JOYSTICK_BUTTON_RELEASE;
			case SDL_JOYBALLMOTION:  // xxx mapping joystick ball as axis is controverse
			case SDL_JOYHATMOTION:   // xxx mapping joystick hat as axis controverse
			case SDL_JOYAXISMOTION:     return Event::TYPE_JOYSTICK_AXIS_MOTION;

			default: return Event::TYPE_UNKNOWN;
		}
	}

	Keyboard::Key Event::getEventKeyCode()
	{
		FGEAL_CHECK_INIT();
		return KeyboardKeyMapping::toGenericKey(self.sdlEvent->key.keysym.sym);
	}

	Mouse::Button Event::getEventMouseButton()
	{
		FGEAL_CHECK_INIT();
		return MouseButtonMapping::toGenericMouseButton(self.sdlEvent->button.button);
	}

	int Event::getEventMouseX()
	{
		FGEAL_CHECK_INIT();
		if(self.sdlEvent->type == SDL_MOUSEMOTION) return self.sdlEvent->motion.x;
		else return self.sdlEvent->button.x;
	}

	int Event::getEventMouseY()
	{
		FGEAL_CHECK_INIT();
		if(self.sdlEvent->type == SDL_MOUSEMOTION) return self.sdlEvent->motion.y;
		else return self.sdlEvent->button.y;
	}

	int Event::getEventMouseWheelMotionAmount()
	{
		FGEAL_CHECK_INIT();
		return self.sdlEvent->wheel.y;
	}

	int Event::getEventJoystickIndex()
	{
		FGEAL_CHECK_INIT();
		const SDL_JoystickID id = (
			(self.sdlEvent->type == SDL_JOYBUTTONDOWN
			 or self.sdlEvent->type == SDL_JOYBUTTONUP)    ? self.sdlEvent->jbutton.which
			:   self.sdlEvent->type == SDL_JOYAXISMOTION   ? self.sdlEvent->jaxis.which
			:   self.sdlEvent->type == SDL_JOYHATMOTION    ? self.sdlEvent->jhat.which
			/*  self.sdlEvent->type == SDL_JOYBALLMOTION */: self.sdlEvent->jball.which);

		for(unsigned j = 0; j < JoystickManagement::auxData.size(); j++)
			if(SDL_JoystickInstanceID(JoystickManagement::auxData[j].sdlJoystick) == id)
				return j;

		return -1;
	}

	int Event::getEventJoystickButtonIndex()
	{
		FGEAL_CHECK_INIT();
		return self.sdlEvent->jbutton.button;
	}

	// xxx this axis mapping seems overly complex and controverse
	int Event::getEventJoystickAxisIndex()
	{
		FGEAL_CHECK_INIT();
		if(self.sdlEvent->type == SDL_JOYAXISMOTION)
			return self.sdlEvent->jaxis.axis;

		else if(self.sdlEvent->type == SDL_JOYHATMOTION)
			// hats indexes comes after all axis
			return SDL_JoystickNumAxes(SDL_JoystickFromInstanceID(self.sdlEvent->jhat.which))
				+  self.sdlEvent->jhat.hat*2 + (self.hatPovMotion == POV_Y? 1 : 0);  // offset by 1 if the hat motion is on Y-axis

		else if(self.sdlEvent->type == SDL_JOYBALLMOTION)
		{
			// balls indexes comes after all axis and hats
			return SDL_JoystickNumAxes(SDL_JoystickFromInstanceID(self.sdlEvent->jball.which))
				+  SDL_JoystickNumHats(SDL_JoystickFromInstanceID(self.sdlEvent->jball.which))*2
				+  self.sdlEvent->jball.ball + (self.sdlEvent->jball.yrel != 0? 1 : 0);  // offset by 1 if the ball motion is on Y-axis
		}
		else return -1;
	}

	int Event::getEventJoystickSecondAxisIndex()
	{
		FGEAL_CHECK_INIT();
		// is it a hat motion event and does the hat motion occur on both axis?
		if(self.sdlEvent->type == SDL_JOYHATMOTION and self.hatPovMotion == BOTH_AXIS)
			return SDL_JoystickNumAxes(SDL_JoystickFromInstanceID(self.sdlEvent->jhat.which))
				 + self.sdlEvent->jhat.hat*2 + 1; // hats indexes comes after all axis

		// is it a ball motion event and does the ball motion occur on both axis?
		else if(self.sdlEvent->type == SDL_JOYBALLMOTION and self.sdlEvent->jball.xrel != 0 and self.sdlEvent->jball.yrel != 0)
			return SDL_JoystickNumAxes(SDL_JoystickFromInstanceID(self.sdlEvent->jball.which))
				 + SDL_JoystickNumHats(SDL_JoystickFromInstanceID(self.sdlEvent->jball.which))*2
				 + self.sdlEvent->jball.ball*2 + 1;  // balls indexes comes after all axis and hats

		else  // case not jball nor jhat, return -1 as there is no secondary axis motion
			return -1;
	}

	float Event::getEventJoystickAxisPosition()
	{
		FGEAL_CHECK_INIT();
		if(self.sdlEvent->type == SDL_JOYAXISMOTION)
			return ((float) self.sdlEvent->jaxis.value)/32768.0f;  // converting range [-32768 to 32767] to [-1.0 to 1.0]

		else if(self.sdlEvent->type == SDL_JOYHATMOTION)
		{
			if(self.hatPovMotion == POV_Y)
				return self.hatPovY;
			else  // either POV X changed or both X and Y changed. either way, return X position
				return self.hatPovX;
		}

		else if(self.sdlEvent->type == SDL_JOYBALLMOTION)
		{
			if(self.sdlEvent->jball.xrel == 0 and self.sdlEvent->jball.yrel != 0)
				return self.sdlEvent->jball.yrel;
			else  // either ball's X changed or both X and Y changed. either way, return X position
				return self.sdlEvent->jball.xrel;
		}
		else return 0;
	}

	float Event::getEventJoystickSecondAxisPosition()
	{
		FGEAL_CHECK_INIT();
		// is it a hat motion event and does the hat motion occur on both axis?
		if(self.sdlEvent->type == SDL_JOYHATMOTION and self.hatPovMotion == BOTH_AXIS)
			return self.hatPovY;  // return Y position (because the non-second version will return X position)

		// is it a ball motion event and does the ball motion occur on both axis?
		else if(self.sdlEvent->type == SDL_JOYBALLMOTION and self.sdlEvent->jball.xrel != 0 and self.sdlEvent->jball.yrel != 0)
			return self.sdlEvent->jball.yrel;  // return Y position (because the non-second version will return X position)

		else return 0;
	}

	void Event::implementation::fillJoystickEventHatFieldsIfNeeded()
	{
		// skip if it's not a joystick hat motion event or it's already recorded
		if(sdlEvent->type != SDL_JOYHATMOTION or isHatPovStateUpdated)
			return;

		// discover index from jhat.which's ID
		int tmp = -1;
		for(unsigned j = 0; tmp == -1 and j < JoystickManagement::auxData.size(); j++)
			if(SDL_JoystickInstanceID(JoystickManagement::auxData[j].sdlJoystick) == sdlEvent->jhat.which)
				tmp = j;

		// should never happen, but...
		if(tmp == -1)
			return;

		const unsigned joyIndex = tmp;

		// lets use references to attempt to write shorter expressions
		const Uint8& previousValue = JoystickManagement::auxData[joyIndex].lastHatsStates[sdlEvent->jhat.hat];
		const Uint8& currentValue = sdlEvent->jhat.value;

		// identify motion type
		if(currentValue == previousValue)
			hatPovMotion = NONE;
		else switch(previousValue)
		{
			default:
			case SDL_HAT_CENTERED:
				if(currentValue == SDL_HAT_UP or currentValue == SDL_HAT_DOWN)
					hatPovMotion = POV_Y;
				if(currentValue == SDL_HAT_LEFT or currentValue == SDL_HAT_RIGHT)
					hatPovMotion = POV_X;
				else
					hatPovMotion = BOTH_AXIS;
				break;

			case SDL_HAT_UP:
				if(currentValue == SDL_HAT_CENTERED or currentValue == SDL_HAT_DOWN)
					hatPovMotion = POV_Y;
				if(currentValue == SDL_HAT_LEFTUP or currentValue == SDL_HAT_RIGHTUP)
					hatPovMotion = POV_X;
				else
					hatPovMotion = BOTH_AXIS;
				break;

			case SDL_HAT_DOWN:
				if(currentValue == SDL_HAT_UP or currentValue == SDL_HAT_CENTERED)
					hatPovMotion = POV_Y;
				if(currentValue == SDL_HAT_LEFTDOWN or currentValue == SDL_HAT_RIGHTDOWN)
					hatPovMotion = POV_X;
				else
					hatPovMotion = BOTH_AXIS;
				break;

			case SDL_HAT_LEFT:
				if(currentValue == SDL_HAT_LEFTUP or currentValue == SDL_HAT_LEFTDOWN)
					hatPovMotion = POV_Y;
				if(currentValue == SDL_HAT_CENTERED or currentValue == SDL_HAT_RIGHT)
					hatPovMotion = POV_X;
				else
					hatPovMotion = BOTH_AXIS;
				break;

			case SDL_HAT_RIGHT:
				if(currentValue == SDL_HAT_RIGHTUP or currentValue == SDL_HAT_RIGHTDOWN)
					hatPovMotion = POV_Y;
				if(currentValue == SDL_HAT_LEFT or currentValue == SDL_HAT_CENTERED)
					hatPovMotion = POV_X;
				else
					hatPovMotion = BOTH_AXIS;
				break;

			case SDL_HAT_LEFTUP:
				if(currentValue == SDL_HAT_LEFT or currentValue == SDL_HAT_LEFTDOWN)
					hatPovMotion = POV_Y;
				if(currentValue == SDL_HAT_UP or currentValue == SDL_HAT_RIGHTUP)
					hatPovMotion = POV_X;
				else
					hatPovMotion = BOTH_AXIS;
				break;

			case SDL_HAT_RIGHTUP:
				if(currentValue == SDL_HAT_RIGHT or currentValue == SDL_HAT_RIGHTDOWN)
					hatPovMotion = POV_Y;
				if(currentValue == SDL_HAT_LEFTUP or currentValue == SDL_HAT_UP)
					hatPovMotion = POV_X;
				else
					hatPovMotion = BOTH_AXIS;
				break;

			case SDL_HAT_LEFTDOWN:
				if(currentValue == SDL_HAT_LEFT or currentValue == SDL_HAT_LEFTUP)
					hatPovMotion = POV_Y;
				if(currentValue == SDL_HAT_DOWN or currentValue == SDL_HAT_RIGHTDOWN)
					hatPovMotion = POV_X;
				else
					hatPovMotion = BOTH_AXIS;
				break;

			case SDL_HAT_RIGHTDOWN:
				if(currentValue == SDL_HAT_RIGHT or currentValue == SDL_HAT_RIGHTUP)
					hatPovMotion = POV_Y;
				if(currentValue == SDL_HAT_LEFTDOWN or currentValue == SDL_HAT_DOWN)
					hatPovMotion = POV_X;
				else
					hatPovMotion = BOTH_AXIS;
				break;
		}

		// set current positions X and Y positions
		hatPovX = JoystickManagement::getAxisValueFromHatPov(currentValue, false);
		hatPovY = JoystickManagement::getAxisValueFromHatPov(currentValue, true);

		isHatPovStateUpdated = true;  // remember to do all of this only once
	}

	//******************* EVENTQUEUE

	EventQueue::EventQueue()
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();
	}

	EventQueue::~EventQueue()
	{
		FGEAL_CHECK_INIT();
		delete &self;
	}

	bool EventQueue::isEmpty()
	{
		FGEAL_CHECK_INIT();
		return SDL_PollEvent(null) == 0;
	}

	bool EventQueue::hasEvents()
	{
		FGEAL_CHECK_INIT();
		return SDL_PollEvent(null) == 1;
	}

	Event* EventQueue::getNextEvent()
	{
		FGEAL_CHECK_INIT();
		SDL_Event sdlEvent;
		if(SDL_PollEvent(&sdlEvent) == 1)
		{
			Event* event = new Event();
			*(event->self.sdlEvent) = sdlEvent;
			event->self.fillJoystickEventHatFieldsIfNeeded();
			JoystickManagement::recordJoystickEventHatPovStateIfNeeded(&sdlEvent);
			return event;
		}
		else return null;
	}

	bool EventQueue::getNextEvent(Event* container)
	{
		FGEAL_CHECK_INIT();
		bool status = (SDL_PollEvent(container->self.sdlEvent) == 1);
		container->self.fillJoystickEventHatFieldsIfNeeded();
		JoystickManagement::recordJoystickEventHatPovStateIfNeeded(container->self.sdlEvent);
		return status;
	}

	bool EventQueue::skipNextEvent()
	{
		FGEAL_CHECK_INIT();
		SDL_Event tmpEvent;
		bool status = (SDL_PollEvent(&tmpEvent) == 1);
		JoystickManagement::recordJoystickEventHatPovStateIfNeeded(&tmpEvent);
		return status;
	}

	void EventQueue::waitNextEvent(Event* container)
	{
		FGEAL_CHECK_INIT();
		if(SDL_WaitEvent(container == null ? null : container->self.sdlEvent) == 0)
			throw AdapterException("Error while waiting for event. %s", SDL_GetError());
		if(container != null)
		{
			container->self.fillJoystickEventHatFieldsIfNeeded();
			JoystickManagement::recordJoystickEventHatPovStateIfNeeded(container->self.sdlEvent);
		}
	}

	void EventQueue::flushEvents()
	{
		FGEAL_CHECK_INIT();
		SDL_FlushEvents(SDL_FIRSTEVENT, SDL_LASTEVENT);
	}

	//XXX SDL 2.0 EventQueue::setEventBypassingEnabled() implementation is experimental and untested
	void EventQueue::setEventBypassingEnabled(bool bypass)
	{
		FGEAL_CHECK_INIT();
		if(bypass)
		{
			SDL_SetEventFilter((SDL_EventFilter) ignoreAllSdlEventFilter, null);
			flushEvents();
		}
		else
		{
			flushEvents();
			SDL_SetEventFilter(null, null);
		}
	}
}
