/*
 * input.cpp
 *
 *  Created on: 25/01/2017
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2017  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/input.hpp"

#define FORWARD_MAPPING(a, b)  case a: return b;
#define BACKWARD_MAPPING(a, b) case b: return a;

#undef DELETE

using std::vector;
using std::string;

namespace fgeal
{
	// =======================================================================================
	// Keyboard

	namespace KeyboardKeyMapping
	{
		#define ALLEGRO_5_0_KEY_MAPPINGS(mapping)\
		mapping(ALLEGRO_KEY_A,  Keyboard::KEY_A)\
		mapping(ALLEGRO_KEY_B,  Keyboard::KEY_B)\
		mapping(ALLEGRO_KEY_C,  Keyboard::KEY_C)\
		mapping(ALLEGRO_KEY_D,  Keyboard::KEY_D)\
		mapping(ALLEGRO_KEY_E,  Keyboard::KEY_E)\
		mapping(ALLEGRO_KEY_F,  Keyboard::KEY_F)\
		mapping(ALLEGRO_KEY_G,  Keyboard::KEY_G)\
		mapping(ALLEGRO_KEY_H,  Keyboard::KEY_H)\
		mapping(ALLEGRO_KEY_I,  Keyboard::KEY_I)\
		mapping(ALLEGRO_KEY_J,  Keyboard::KEY_J)\
		mapping(ALLEGRO_KEY_K,  Keyboard::KEY_K)\
		mapping(ALLEGRO_KEY_L,  Keyboard::KEY_L)\
		mapping(ALLEGRO_KEY_M,  Keyboard::KEY_M)\
		mapping(ALLEGRO_KEY_N,  Keyboard::KEY_N)\
		mapping(ALLEGRO_KEY_O,  Keyboard::KEY_O)\
		mapping(ALLEGRO_KEY_P,  Keyboard::KEY_P)\
		mapping(ALLEGRO_KEY_Q,  Keyboard::KEY_Q)\
		mapping(ALLEGRO_KEY_R,  Keyboard::KEY_R)\
		mapping(ALLEGRO_KEY_S,  Keyboard::KEY_S)\
		mapping(ALLEGRO_KEY_T,  Keyboard::KEY_T)\
		mapping(ALLEGRO_KEY_U,  Keyboard::KEY_U)\
		mapping(ALLEGRO_KEY_V,  Keyboard::KEY_V)\
		mapping(ALLEGRO_KEY_W,  Keyboard::KEY_W)\
		mapping(ALLEGRO_KEY_X,  Keyboard::KEY_X)\
		mapping(ALLEGRO_KEY_Y,  Keyboard::KEY_Y)\
		mapping(ALLEGRO_KEY_Z,  Keyboard::KEY_Z)\
		\
		mapping(ALLEGRO_KEY_0,  Keyboard::KEY_0)\
		mapping(ALLEGRO_KEY_1,  Keyboard::KEY_1)\
		mapping(ALLEGRO_KEY_2,  Keyboard::KEY_2)\
		mapping(ALLEGRO_KEY_3,  Keyboard::KEY_3)\
		mapping(ALLEGRO_KEY_4,  Keyboard::KEY_4)\
		mapping(ALLEGRO_KEY_5,  Keyboard::KEY_5)\
		mapping(ALLEGRO_KEY_6,  Keyboard::KEY_6)\
		mapping(ALLEGRO_KEY_7,  Keyboard::KEY_7)\
		mapping(ALLEGRO_KEY_8,  Keyboard::KEY_8)\
		mapping(ALLEGRO_KEY_9,  Keyboard::KEY_9)\
		\
		mapping(ALLEGRO_KEY_F1,  Keyboard::KEY_F1)\
		mapping(ALLEGRO_KEY_F2,  Keyboard::KEY_F2)\
		mapping(ALLEGRO_KEY_F3,  Keyboard::KEY_F3)\
		mapping(ALLEGRO_KEY_F4,  Keyboard::KEY_F4)\
		mapping(ALLEGRO_KEY_F5,  Keyboard::KEY_F5)\
		mapping(ALLEGRO_KEY_F6,  Keyboard::KEY_F6)\
		mapping(ALLEGRO_KEY_F7,  Keyboard::KEY_F7)\
		mapping(ALLEGRO_KEY_F8,  Keyboard::KEY_F8)\
		mapping(ALLEGRO_KEY_F9,  Keyboard::KEY_F9)\
		mapping(ALLEGRO_KEY_F10, Keyboard::KEY_F10)\
		mapping(ALLEGRO_KEY_F11, Keyboard::KEY_F11)\
		mapping(ALLEGRO_KEY_F12, Keyboard::KEY_F12)\
		\
		mapping(ALLEGRO_KEY_UP,    Keyboard::KEY_ARROW_UP)\
		mapping(ALLEGRO_KEY_DOWN,  Keyboard::KEY_ARROW_DOWN)\
		mapping(ALLEGRO_KEY_LEFT,  Keyboard::KEY_ARROW_LEFT)\
		mapping(ALLEGRO_KEY_RIGHT, Keyboard::KEY_ARROW_RIGHT)\
		\
		mapping(ALLEGRO_KEY_ENTER,      Keyboard::KEY_ENTER)\
		mapping(ALLEGRO_KEY_SPACE,      Keyboard::KEY_SPACE)\
		mapping(ALLEGRO_KEY_ESCAPE,     Keyboard::KEY_ESCAPE)\
		mapping(ALLEGRO_KEY_LCTRL,      Keyboard::KEY_LEFT_CONTROL)\
		mapping(ALLEGRO_KEY_RCTRL,      Keyboard::KEY_RIGHT_CONTROL)\
		mapping(ALLEGRO_KEY_LSHIFT,      Keyboard::KEY_LEFT_SHIFT)\
		mapping(ALLEGRO_KEY_RSHIFT,      Keyboard::KEY_RIGHT_SHIFT)\
		mapping(ALLEGRO_KEY_ALT,        Keyboard::KEY_LEFT_ALT)\
		mapping(ALLEGRO_KEY_ALTGR,      Keyboard::KEY_RIGHT_ALT)\
		mapping(ALLEGRO_KEY_LWIN,       Keyboard::KEY_LEFT_SUPER)\
		mapping(ALLEGRO_KEY_RWIN,       Keyboard::KEY_RIGHT_SUPER)\
		mapping(ALLEGRO_KEY_MENU,       Keyboard::KEY_MENU)\
		mapping(ALLEGRO_KEY_TAB,        Keyboard::KEY_TAB)\
		mapping(ALLEGRO_KEY_BACKSPACE,  Keyboard::KEY_BACKSPACE)\
		mapping(ALLEGRO_KEY_MINUS,      Keyboard::KEY_MINUS)\
		mapping(ALLEGRO_KEY_EQUALS,     Keyboard::KEY_EQUALS)\
		mapping(ALLEGRO_KEY_OPENBRACE,  Keyboard::KEY_LEFT_BRACKET)\
		mapping(ALLEGRO_KEY_CLOSEBRACE, Keyboard::KEY_RIGHT_BRACKET)\
		mapping(ALLEGRO_KEY_SEMICOLON,  Keyboard::KEY_SEMICOLON)\
		mapping(ALLEGRO_KEY_COMMA,      Keyboard::KEY_COMMA)\
		mapping(ALLEGRO_KEY_FULLSTOP,   Keyboard::KEY_PERIOD)\
		mapping(ALLEGRO_KEY_SLASH,      Keyboard::KEY_SLASH)\
		mapping(ALLEGRO_KEY_BACKSLASH,  Keyboard::KEY_BACKSLASH)\
		mapping(ALLEGRO_KEY_QUOTE,      Keyboard::KEY_QUOTE)\
		mapping(ALLEGRO_KEY_TILDE,      Keyboard::KEY_TILDE)\
		\
		mapping(ALLEGRO_KEY_INSERT, Keyboard::KEY_INSERT)\
		mapping(ALLEGRO_KEY_DELETE, Keyboard::KEY_DELETE)\
		mapping(ALLEGRO_KEY_HOME,   Keyboard::KEY_HOME)\
		mapping(ALLEGRO_KEY_PGUP,   Keyboard::KEY_PAGE_UP)\
		mapping(ALLEGRO_KEY_PGDN,   Keyboard::KEY_PAGE_DOWN)\
		\
		mapping(ALLEGRO_KEY_PAD_0,  Keyboard::KEY_NUMPAD_0)\
		mapping(ALLEGRO_KEY_PAD_1,  Keyboard::KEY_NUMPAD_1)\
		mapping(ALLEGRO_KEY_PAD_2,  Keyboard::KEY_NUMPAD_2)\
		mapping(ALLEGRO_KEY_PAD_3,  Keyboard::KEY_NUMPAD_3)\
		mapping(ALLEGRO_KEY_PAD_4,  Keyboard::KEY_NUMPAD_4)\
		mapping(ALLEGRO_KEY_PAD_5,  Keyboard::KEY_NUMPAD_5)\
		mapping(ALLEGRO_KEY_PAD_6,  Keyboard::KEY_NUMPAD_6)\
		mapping(ALLEGRO_KEY_PAD_7,  Keyboard::KEY_NUMPAD_7)\
		mapping(ALLEGRO_KEY_PAD_8,  Keyboard::KEY_NUMPAD_8)\
		mapping(ALLEGRO_KEY_PAD_9,  Keyboard::KEY_NUMPAD_9)\
		\
/*		mapping(ALLEGRO_KEY_PAD_PLUS,     Keyboard::KEY_NUMPAD_ADDITION)\
		mapping(ALLEGRO_KEY_PAD_MINUS,    Keyboard::KEY_NUMPAD_SUBTRACTION)\
		mapping(ALLEGRO_KEY_PAD_ASTERISK, Keyboard::KEY_NUMPAD_MULTIPLICATION)\
		mapping(ALLEGRO_KEY_PAD_SLASH,    Keyboard::KEY_NUMPAD_DIVISION)\
		mapping(ALLEGRO_KEY_PAD_DELETE,   Keyboard::KEY_NUMPAD_DECIMAL)\
		mapping(ALLEGRO_KEY_PAD_ENTER,    Keyboard::KEY_NUMPAD_ENTER)\ */
		//end

		// Returns the fgeal-equivalent key enum of the given allegro keycode.
		Keyboard::Key toGenericKey(int allegroKeycode)
		{
			switch(allegroKeycode)
			{
				ALLEGRO_5_0_KEY_MAPPINGS(FORWARD_MAPPING);
				default: return Keyboard::KEY_UNKNOWN;
			}
		}

		// Returns the Allegro 5.0 keycode of the given fgeal-equivalent key enum.
		int toUnderlyingLibraryKey(Keyboard::Key key)
		{
			switch(key)
			{
				ALLEGRO_5_0_KEY_MAPPINGS(BACKWARD_MAPPING);
				default: return ALLEGRO_KEY_MAX;
			}
		}
	}

	// API functions

	bool Keyboard::isKeyPressed(Keyboard::Key key)
	{
		FGEAL_CHECK_INIT();
		static ALLEGRO_KEYBOARD_STATE state;
		al_get_keyboard_state(&state);
		return al_key_down(&state, KeyboardKeyMapping::toUnderlyingLibraryKey(key));
	}

	// =======================================================================================
	// Mouse

	namespace MouseButtonMapping
	{
		#define ALLEGRO_5_0_MOUSE_BUTTON_MAPPINGS(mapping)\
		mapping(1,  Mouse::BUTTON_LEFT)\
		mapping(2,  Mouse::BUTTON_RIGHT)\
		mapping(3,  Mouse::BUTTON_MIDDLE)\
		//end

		// Returns the fgeal-equivalent mouse button enum of the given allegro mouse button number.
		Mouse::Button toGenericMouseButton(int allegroButtonNumber)
		{
			switch(allegroButtonNumber)
			{
				ALLEGRO_5_0_MOUSE_BUTTON_MAPPINGS(FORWARD_MAPPING);
				default: return Mouse::BUTTON_UNKNOWN;
			}
		}

		// Returns the Allegro 5.0 mouse button number of the fgeal-equivalent mouse button enum.
		int toUnderlyingLibraryMouseButton(Mouse::Button button)
		{
			switch(button)
			{
				ALLEGRO_5_0_MOUSE_BUTTON_MAPPINGS(BACKWARD_MAPPING);
				default: return 0;
			}
		}
	}

	// API functions

	bool Mouse::isButtonPressed(Mouse::Button btn)
	{
		FGEAL_CHECK_INIT();
		static ALLEGRO_MOUSE_STATE state;
		al_get_mouse_state(&state);
		return al_mouse_button_down(&state, MouseButtonMapping::toUnderlyingLibraryMouseButton(btn));
	}

	Point Mouse::getPosition()
	{
		FGEAL_CHECK_INIT();
		static ALLEGRO_MOUSE_STATE state;
		al_get_mouse_state(&state);
		Point pt = {(float) state.x, (float) state.y};
		return pt;
	}

	void Mouse::setPosition(const Point& position)
	{
		FGEAL_CHECK_INIT();
		Mouse::setPosition(position.x, position.y);
	}

	int Mouse::getPositionX()
	{
		FGEAL_CHECK_INIT();
		static ALLEGRO_MOUSE_STATE state;
		al_get_mouse_state(&state);
		return state.x;
	}

	int Mouse::getPositionY()
	{
		FGEAL_CHECK_INIT();
		static ALLEGRO_MOUSE_STATE state;
		al_get_mouse_state(&state);
		return state.y;
	}

	void Mouse::setPosition(int x, int y)
	{
		FGEAL_CHECK_INIT();
		al_set_mouse_xy(Display::instance->self.allegroDisplay, x, y);
	}

	// =======================================================================================
	// Joystick

	vector<ALLEGRO_JOYSTICK*> JoystickManagement::allegroJoystickPtrs;

	void Joystick::configureAll()
	{
		for(int j = 0; j < al_get_num_joysticks(); j++)
			JoystickManagement::allegroJoystickPtrs.push_back(al_get_joystick(j));
	}

	void Joystick::releaseAll()
	{
		JoystickManagement::allegroJoystickPtrs.clear();
	}

	unsigned Joystick::getCount()
	{
		FGEAL_CHECK_INIT();
		return al_get_num_joysticks();
	}

	string Joystick::getJoystickName(unsigned joyIndex)
	{
		FGEAL_CHECK_INIT();
		return al_get_joystick_name(JoystickManagement::allegroJoystickPtrs[joyIndex]);
	}

	/** Returns the number of buttons on this joystick. */
	unsigned Joystick::getButtonCount(unsigned joyIndex)
	{
		FGEAL_CHECK_INIT();
		return al_get_joystick_num_buttons(JoystickManagement::allegroJoystickPtrs[joyIndex]);
	}

	/** Returns the number of axis on this joystick. */
	unsigned Joystick::getAxisCount(unsigned joyIndex)
	{
		FGEAL_CHECK_INIT();
		unsigned axesCount = 0;
		for(int s = 0; s < al_get_joystick_num_sticks(JoystickManagement::allegroJoystickPtrs[joyIndex]); s++)
			axesCount += al_get_joystick_num_axes(JoystickManagement::allegroJoystickPtrs[joyIndex], s);
		return axesCount;
	}

	/** Returns true if the given button (by index) is being pressed on this joystick. */
	bool Joystick::isButtonPressed(unsigned joyIndex, unsigned buttonIndex)
	{
		FGEAL_CHECK_INIT();
		ALLEGRO_JOYSTICK_STATE state;
		al_get_joystick_state(JoystickManagement::allegroJoystickPtrs[joyIndex], &state);
		return state.button[buttonIndex] > 0;
	}

	/** Returns the current position of the given axis (by index) on this joystick. */
	float Joystick::getAxisPosition(unsigned joyIndex, unsigned axisIndex)
	{
		FGEAL_CHECK_INIT();
		ALLEGRO_JOYSTICK_STATE state;
		al_get_joystick_state(JoystickManagement::allegroJoystickPtrs[joyIndex], &state);
		return state.stick[axisIndex/2].axis[axisIndex%2];
	}
}
