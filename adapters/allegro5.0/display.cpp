/*
 * display.cpp
 *
 *  Created on: 24/10/2016
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2016  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/display.hpp"

#include "fgeal/exceptions.hpp"

#include <allegro5/allegro.h>

#include <climits>
#include <string>
using std::string;

static const int ALLEGRO_FORCE_VSYNC_OFF = 2;  // as specified by the documentation

namespace fgeal
{
	static Point getCenteredWindowPositionForSize(unsigned width, unsigned height);

	Display::Display(const Options& options)
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();

		if(options.fullscreen)
			al_set_new_display_flags(ALLEGRO_FULLSCREEN);
		else
		{
			int flags = ALLEGRO_WINDOWED;

			if(options.isUserResizeable)
				flags |= ALLEGRO_RESIZABLE;

			al_set_new_display_flags(flags);

			if(options.positioning == Options::POSITION_DEFINED)
				al_set_new_window_position(options.position.x, options.position.y);

			else if(options.positioning == Options::POSITION_CENTERED)
			{
				const Point pos = getCenteredWindowPositionForSize(options.width, options.height);
				al_set_new_window_position(pos.x, pos.y);
			}

			else if(options.positioning == Options::POSITION_UNDEFINED)
				al_set_new_window_position(INT_MAX, INT_MAX);
		}

		al_set_new_display_option(ALLEGRO_VSYNC, ALLEGRO_FORCE_VSYNC_OFF, ALLEGRO_SUGGEST);

		self.allegroDisplay = al_create_display(options.width, options.height);
		if(self.allegroDisplay == null)
			throw AdapterException("Could not create display! Error %d", al_get_errno());

		self.currentTitle = options.title;
		al_set_window_title(self.allegroDisplay, self.currentTitle.c_str());

		self.icon = null;
		if(not options.iconFilename.empty())
			this->setIcon(options.iconFilename);
	}

	Display::~Display()
	{
		FGEAL_CHECK_INIT();
		al_destroy_display(self.allegroDisplay);
		if(self.icon != null) al_destroy_bitmap(self.icon);
		delete &self;
	}

	unsigned Display::getWidth()
	{
		FGEAL_CHECK_INIT();
		return al_get_display_width(self.allegroDisplay);
	}

	unsigned Display::getHeight()
	{
		FGEAL_CHECK_INIT();
		return al_get_display_height(self.allegroDisplay);
	}

	string Display::getTitle() const
	{
		FGEAL_CHECK_INIT();
		return self.currentTitle;
	}

	void Display::setTitle(const string& title)
	{
		FGEAL_CHECK_INIT();
		self.currentTitle = title;
		al_set_window_title(self.allegroDisplay, self.currentTitle.c_str());
	}

	void Display::setIcon(const std::string& iconFilename)
	{
		FGEAL_CHECK_INIT();
		ALLEGRO_BITMAP* allegroBitmap = al_load_bitmap(iconFilename.c_str());

		if(allegroBitmap == null)
			throw AdapterException("Error while trying to set display icon \"%s\". Error %d", iconFilename.c_str(), al_get_errno());

		al_set_display_icon(self.allegroDisplay, allegroBitmap);

		if(self.icon != null)
			al_destroy_bitmap(self.icon);

		self.icon = allegroBitmap;
	}

	void Display::setSize(unsigned width, unsigned height)
	{
		FGEAL_CHECK_INIT();
		al_resize_display(self.allegroDisplay, width, height);
	}

	void Display::refresh()
	{
		FGEAL_CHECK_INIT();
		al_flip_display();
	}

	void Display::clear()
	{
		FGEAL_CHECK_INIT();
		al_clear_to_color(al_map_rgba(0,0,0,255));
	}

	bool Display::isFullscreen()
	{
		FGEAL_CHECK_INIT();
		return al_get_display_flags(self.allegroDisplay) & ALLEGRO_FULLSCREEN;
	}

	void Display::setFullscreen(bool choice)
	{
		FGEAL_CHECK_INIT();
		if(isFullscreen() == choice)  // the call to isFullscreen already calls checkInit
			return;  // if same mode, nothing needs to be done

		// flips both ALLEGRO_WINDOWED and ALLEGRO_FULLSCREEN bit flags
		al_set_new_display_flags((al_get_display_flags(self.allegroDisplay) ^ ALLEGRO_FULLSCREEN) ^ ALLEGRO_WINDOWED);

		// restart display
		al_destroy_display(self.allegroDisplay);
		self.allegroDisplay = al_create_display(al_get_display_width(self.allegroDisplay), al_get_display_height(self.allegroDisplay));

		if(self.allegroDisplay == null)
			throw AdapterException("Could not set display mode! Error %d", al_get_errno());

		al_set_window_title(self.allegroDisplay, self.currentTitle.c_str());

		if(self.icon != null)
			al_set_display_icon(self.allegroDisplay, self.icon);
	}

	void Display::setPosition(const Point& pos)
	{
		FGEAL_CHECK_INIT();
		al_set_window_position(self.allegroDisplay, pos.x, pos.y);
	}

	void Display::setPositionOnCenter()
	{
		FGEAL_CHECK_INIT();
		const Point pos = getCenteredWindowPositionForSize(al_get_display_width(self.allegroDisplay), al_get_display_height(self.allegroDisplay));
		al_set_window_position(self.allegroDisplay, pos.x, pos.y);
	}

	void Display::setMouseCursorVisible(bool enable)
	{
		FGEAL_CHECK_INIT();
		if(enable) al_show_mouse_cursor(self.allegroDisplay);
		else al_hide_mouse_cursor(self.allegroDisplay);
	}

	// -----------------------------------------------------------------------------------
	Point getCenteredWindowPositionForSize(unsigned width, unsigned height)
	{
		ALLEGRO_MONITOR_INFO info;
		al_get_monitor_info(0, &info);
		const Point pos = {
			(float)((info.x2 - info.x1) - width)/2,
			(float)((info.y2 - info.y1) - height)/2
		};
		return pos;
	}
}
