/*
 * font.cpp
 *
 *  Created on: 24/10/2016
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2016  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/font.hpp"

#include "fgeal/exceptions.hpp"

#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>

using std::string;

namespace fgeal
{
	Font::Font(const string& filename, int size, bool antialiasing)
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();

		//I know, pretty odd...
		int flags = (antialiasing? 0 : ALLEGRO_TTF_MONOCHROME);
		self.allegroFont = al_load_font(filename.c_str(), size, flags);

		if(self.allegroFont == null)
			throw AdapterException("Font %s could not be loaded: Error %d", filename.c_str(), al_get_errno());
	}

	Font::~Font()
	{
		FGEAL_CHECK_INIT();
		al_destroy_font(self.allegroFont);
		delete &self;
	}

	void Font::drawText(const string& text, float x, float y, Color color)
	{
		FGEAL_CHECK_INIT();
		al_draw_text(self.allegroFont, al_map_rgba(color.r, color.g, color.b, color.a), x, y, ALLEGRO_ALIGN_LEFT, text.c_str());
	}

	float Font::getHeight() const
	{
		FGEAL_CHECK_INIT();
		return al_get_font_line_height(self.allegroFont);
	}

	float Font::getTextWidth(const std::string& text) const
	{
		FGEAL_CHECK_INIT();
		return al_get_text_width(self.allegroFont, text.c_str());
	}
}
