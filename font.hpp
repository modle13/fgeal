/*
 * font.hpp
 *
 *  Created on: 26/11/2015
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2015  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef FGEAL_FONT_HPP_
#define FGEAL_FONT_HPP_
#include <ciso646>

#include <string>

namespace fgeal
{
	class Font
	{
		struct implementation;
		friend struct implementation;
		implementation& self;

		// All wrapper classes are granted access to each other inner members.
		friend class fgeal::Display;
		friend class fgeal::Graphics;
		friend class fgeal::Image;
		friend class fgeal::Event;
		friend class fgeal::EventQueue;
		//friend class fgeal::Font;  // class itself
		friend class fgeal::Sound;
		friend class fgeal::SoundStream;
		friend class fgeal::Music;
		friend class fgeal::Keyboard;
		friend class fgeal::Mouse;
		friend class fgeal::Joystick;

		public:

		/** Creates a font object from the given font filename and size. The antialiasing hint may be ignored. */
		Font(const std::string& filename, int size=12, bool antialiasing=true);
		~Font();

		/** Draws the given string on the screen at given location with the given color. */
		void drawText(const std::string& text, float x=0, float y=0, Color color=Color::BLACK);

		/** Returns the height of a text drawn at the screen using this font. */
		float getHeight() const;

		/** Returns the expected rendered size of the given string, using this font. */
		float getTextWidth(const std::string& text) const;
	};
}

#endif /* FGEAL_FONT_HPP_ */
