# fgeal

A simple C++ wrapper meant to provide common functionalities between some game libraries through a single API.

This wrapper is a cross-platform abstraction layer to other low-level game libraries, such as [SDL](https://www.libsdl.org/). So, when developing a game using fgeal, a game could, theoretically, be coded once and compiled everywhere the underlying backend library supports. For example, when using a SDL 1.2 backend, it should be possible to build/use fgeal in every platform that SDL 1.2 supports.

It supports creating windows, drawing images (normal, rotated, scaled), drawing text using [TrueType](https://en.wikipedia.org/wiki/TrueType) fonts, drawing a few primitives (line, rectangle, ellipse, etc), playing sounds/music and event handling. There is also some extras, such as a sprite class, a simple state-based game class (based on [Slick2D](http://slick.ninjacave.com/)), etc. 

The code is in an [Eclipse CDT](http://www.eclipse.org/cdt/) project folder structure. It should be simple to import the code with Eclipse's egit ("Import projects from Git") or import it after cloning externally. Alternatively, there are *experimental* linux makefiles to build the library.

The main motivation for its creation was to create a "code once, compile anywhere" approach for some personal projects. After experiencing some frustation with inconsistent availability of game libraries on mainstream linux distros (mainly those with point release development model, like Ubuntu), version incompatibilities and other issues, I came up with these wrapper functions to try to solve these conflicts. When the code grew a little and started to include some wrapper classes as well, this repository was created to garther it all. 

Due to a single API, programs coded with fgeal can be built using different backends that can be chosen according to the underlying library's availability on the intended operating system. Currently the following adapters/backends are available:

- Allegro 5.0 adapter, compatible with Allegro 5.x versions. See [Allegro library](http://liballeg.org/).
- SDL 1.2 adapter, compatible with SDL 1.2.x versions. See [SDL library](https://www.libsdl.org/).
- SDL 2.0 adapter, compatible with SDL 2.0.x versions. See [SDL library](https://www.libsdl.org/).

Additionally, the following **experimental** adapters/backends are available as well:

- SFML 1.6 adapter, compatible with SFML 1.5 and 1.6 versions. See [SFML library](https://www.sfml-dev.org/).
- SFML 2.0 adapter, compatible with SFML 2.x versions. See [SFML library](https://www.sfml-dev.org/).
- Allegro 4.4 adapter, compatible with Allegro 4.2 and 4.4 versions. See [Allegro library](http://liballeg.org/).

In the future, it should support more features and adapters.

It should be noted, however, that although it is intended to be cross-platform, it was **never** tested on MacOSes.

**fgeal** stands for **f**geal **g**ame **e**ngine **a**bstraction **l**ayer (yes, a recursive acronym).

See [BUILDING.md](https://gitlab.com/hydren/fgeal/blob/master/BUILDING.md) for information about building the library or compiling with/against it.
