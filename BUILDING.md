# Building

#### Prerequisites:
* C++ compiler (Linux GCC, MinGW tested)
* `dirent.h` header. There is a Windows implementation on https://github.com/tronkko/dirent. On Linux distros it is usually present.
* And either:
   - Allegro 5 **development** libraries (5.0.x and 5.2.x tested) and the following addon libraries:
      * Allegro 5 Image addon (see https://www.allegro.cc/manual/5/image.html)
      * Allegro 5 Font addon (see https://www.allegro.cc/manual/5/font.html)
      * Allegro 5 TTF addon (optional but recommended) (see https://www.allegro.cc/manual/5/font.html)
      * Allegro 5 Primitives addon (see https://www.allegro.cc/manual/5/primitives.html
      * Allegro 5 Audio addon (see https://www.allegro.cc/manual/5/audio.html)
      * Allegro 5 Audio Codec addon (see https://www.allegro.cc/manual/5/acodec.html)
   - SDL 1.2 **development** libraries and the following addon libraries:
      * SDL_image (see https://www.libsdl.org/projects/SDL_image/release-1.2.html)
      * SDL_ttf (see https://www.libsdl.org/projects/SDL_ttf/release-1.2.html)
      * SDL_mixer (see https://www.libsdl.org/projects/SDL_mixer/release-1.2.html)
      * SDL_gfx (see http://www.ferzkopp.net/wordpress/2016/01/02/sdl_gfx-sdl2_gfx/ or https://sourceforge.net/projects/sdlgfx/)
   - SDL 2.0 **development** libraries and the following addon libraries:
      * SDL2_image (see https://www.libsdl.org/projects/SDL_image)
      * SDL2_ttf (see https://www.libsdl.org/projects/SDL_ttf)
      * SDL2_mixer (see https://www.libsdl.org/projects/SDL_mixer/)
      * SDL2_gfx (see http://www.ferzkopp.net/wordpress/2016/01/02/sdl_gfx-sdl2_gfx/ or https://sourceforge.net/projects/sdl2gfx/)
   - (experimental) SFML **development** libraries (1.5 and 1.6 tested)
   - (experimental) SFML 2 **development** libraries (2.0.x to 2.4.x tested)
   - (experimental) Allegro 4 **development** libraries (4.2.x and 4.4.x tested) and, optionally, the following addon libraries:
      * loadpng (*recommended*) (see http://liballeg.org/stabledocs/en/addons.html#loadpng or https://www.allegro.cc/resource/Libraries/ImageFormats/loadpng)
      * jpgalleg (*recommended*) (see http://liballeg.org/stabledocs/en/addons.html#jpgalleg or https://www.allegro.cc/resource/Libraries/ImageFormats/jpgalleg)
      * logg (*recommended*) (see http://liballeg.org/stabledocs/en/addons.html#logg or http://opensnc.sourceforge.net/logg/) or alogg (see https://www.allegro.cc/resource/Libraries/Audio/alogg)
      * AllegTTF (*recommended*) (see https://www.allegro.cc/resource/Libraries/Text/AllegTTF or http://www.deleveld.dds.nl/allegttf.htm)
      * AllegroMP3 (see https://www.allegro.cc/resource/Libraries/Audio/AllegroMP3 or https://d1cxvcw9gjxu2x.cloudfront.net/attachments/344252 for download)
      * algif (see https://www.allegro.cc/resource/Libraries/ImageFormats/algif or http://algif.sourceforge.net/)


#### Building
Assuming you are on Linux, you can build it using 'make':

* Install `make` if not installed.
* Open a terminal and navigate it to the folder containing *fgeal*. 
* Copy one of the folders inside `project/build/` (the choice depends on what back-end is going to be used) to the root of the `fgeal` folder.
* Enter the copied folder and execute the configure script: `./configure.sh`.
* If the previous script ended ok, build using the makefile: `make` .

> The resulting library, `libfgeal.so` will be built on the same folder.

> The folder containing fgeal (the root folder) **must** be named `fgeal`. Otherwise the makefile will fail.

#### Eclipse project
You can also use eclipse to build the project (tested to work with MinGW and Linux GCC). Here is how:

##### Prerequisites for eclipse:
- Get the Eclipse IDE for C/C++ Developers. Link: https://www.eclipse.org/downloads/packages/
- When using MinGW, make sure the g++ program is on the OS path variable. On Windows you need to add the `/bin` directory to your PATH variable.

##### Instructions:
- Clone the code from [this](https://gitlab.com/hydren/fgeal.git) repository.
- Import the project using the [Import Projects wizard](http://help.eclipse.org/kepler/index.jsp?topic=%2Forg.eclipse.platform.doc.user%2Ftasks%2Ftasks-importproject.htm).
- If you want to clone from Eclipse, select workspace and on the projects pane left-click and select `Import -> Projects from git`. Choose `Clone URI`. On the URI field, paste the repository adress https://gitlab.com/hydren/fgeal.git. Click next and the choose `master` and next. Wait for the download and then select `Import existing projects` and then next and finish.
- Left-click project and select build configuration according to your environment (i.e. `linux-allegro-5.0-release` for *Linux*, `win32-allegro-5.0-release` for *Windows*) and then build.
- Any problems indicates that something probably went wrong with previous steps (missing/wrongly installed libraries)
- If something wents wrong because of some cross-gcc stuff, install the `C/C++ GCC Cross Compiler Support` plugin on Eclipse via `Install new software` option on Eclipse.
- There should be a resulting library `libfgeal.so` (linux-gcc), `libfgeal.dll` (mingw) or `fgeal.dll` (msvc), to be linked against.

#### Using it
To build a program with **fgeal**, one should add `-lfgeal` flag when building/linking with GCC/MinGW. Otherwise build your program together with the fgeal source, using the apropriate linker flags, which depends on the desired backend to use.

#### MSVC compatibility

Compatibility with MSVC is experimental and not fully tested. Currently, there are Eclipse build configurations that builds with MSVC and were tested to work with MSVC2010 (VC10) 32bit.
