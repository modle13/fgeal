/*
 * exceptions.hpp
 *
 *  Created on: 27/10/2016
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2016  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef FGEAL_EXCEPTIONS_HPP_
#define FGEAL_EXCEPTIONS_HPP_
#include <ciso646>

#include <cstdarg>

#include <stdexcept>
#include <string>

#include "core.hpp"

namespace fgeal
{
	class Exception extends public std::exception
	{
		protected:
		std::string message;

		Exception(){}

		public:
		Exception(const char* msg, ...);
		Exception(const std::string& msg) : message(msg) {}
		~Exception() throw() {}

		const char* what() const throw() { return this->message.c_str(); }
	};

	class AdapterException extends public Exception
	{
		public:
		AdapterException(const char* msg, ...);
		AdapterException(const std::string& msg) : Exception(msg) {}
		~AdapterException() throw() {}
	};

	class NotImplementedException extends public Exception
	{
		public:
		NotImplementedException(const char* msg, ...);
		NotImplementedException(const std::string& msg) : Exception(msg) {}
		~NotImplementedException() throw() {}
	};

	class GameException extends public Exception
	{
		public:
		GameException(const char* msg, ...);
		GameException(const std::string& msg) : Exception(msg) {}
		~GameException() throw() {}
	};
}

#endif /* FGEAL_EXCEPTIONS_HPP_ */
